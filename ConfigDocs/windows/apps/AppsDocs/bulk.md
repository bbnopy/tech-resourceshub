# BULK RENAME UTILITY



## Розділ &#127991; Інструменти &#129520;

>&#128161; Безкоштовна утиліта для перейменування файлів для Windows &#128421;. Перейменовуйте кілька файлів одним натисканням кнопки.

- **версія програми &#128230;**: :three: . :four: . :four:
- **розробник &#128422;**: Valley Software
- **офіційний сайт**: [посилання &#128279;](https://www.bulkrenameutility.co.uk/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/bulk-file-rename/9N2LR7HCGDDM?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.bulkrenameutility.co.uk/Download.php)
- &#128229; з Хмарного сховища &#9729;
