# RAINMETER


> :information_source: **Інформація:** Дозволяє відображати на робочому столі скіни, що налаштовуються, від лічильників використання обладнання до повнофункціональних аудіовізуалізаторів. Ви обмежені лише своєю уявою та креативністю.

***
:open_file_folder: **Розділ** :framed_picture: *Графіка*
***

## Про Rainmeter

| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :four: . :five: . :one: :eight: |
| :computer: **розробник** | Kimmo 'Rainy' Pekkola |
| **сайт** | [:link: посилання](https://www.rainmeter.net/) |

## Встановлення

### :desktop_computer: Windows

- :inbox_tray: Завантажити з сайту за [:link: посилання](https://www.rainmeter.net/)
- :inbox_tray: Завантажити з :cloud: Хмарного сховища

