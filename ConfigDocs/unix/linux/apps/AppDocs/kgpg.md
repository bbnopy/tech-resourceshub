# KGPG



## Розділ &#127991; Інструменти &#129520;

>&#128161; Простий інтерфейс до GnuPG, потужного засобу шифрування. Програма &#128230; допоможе вам у керуванні ключами, імпортуванні та експортуванні ключів, перегляді підписів ключів, станів довіри до ключів та даних щодо строку дії ключів.

- **версія програми &#128230;**: :two: :three: . :zero: :eight: . :two:
- **розробник &#128422;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://apps.kde.org/uk/kgpg/)

## Операційни Системи

### Linux &#128039;

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

