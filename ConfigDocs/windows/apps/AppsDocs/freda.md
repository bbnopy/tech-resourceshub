# FREDA EPUB EBOOK READER



## Розділ &#127991; Офіс &#128209;

>&#128161; Програма &#128230; для читання електронних книг (е-книг) на Windows &#128421;.

- **версія програми &#128230;**: :five: . :zero: :five:
- **розробник &#128422;**: Turnipsoft

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store**: за [посиланням &#128279;](https://apps.microsoft.com/store/detail/freda-epub-ebook-reader/9WZDNCRFJ43B)
