# NATRON



## Розділ &#127991; Графіка &#128443;

>&#128161; Це безкоштовна програма для створення композицій на основі вузлів з відкритим вихідним кодом. На нього вплинуло програмне забезпечення для цифрової композиції, таке як Avid Media Illusion, Apple Shake, Blackmagic Fusion, Autodesk Flame і Nuke, від яких запозичено користувацький інтерфейс і багато концепцій.

- **версія програми &#128230;**: :two: . :five: . :zero:
- **розробник &#128422;**: Alexandre Gauthier, Frédéric Devernay
- **офіційний сайт**: [посилання &#128279;](https://natrongithub.github.io/)

## Операційні Системи

### Windows &#128421;

- &#128229; з сайту за [посиланням &#128279;](https://natrongithub.github.io/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з сайту за [посиланням &#128279;](https://natrongithub.github.io/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub fr.natron.Natron`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install natron`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install natron --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install natron --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

