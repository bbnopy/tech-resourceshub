# WHATSAPP



## Розділ &#127991; Інтернет &#127758;

>&#128161; Пропрієтарний месенджер для смартфонів.

- **версія програми &#128230;**:
  - Windows &#128421;: :two: . :two: :three: :three: :eight: . :seven: . :zero:
  - Linux &#128039;: :one: . :six: . :three:
- **розробник &#128422;**: WhatsApp, Inc.
- **офіційний сайт**: [посилання &#128279;](https://web.whatsapp.com)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/whatsapp/9NKSQGP7F2NH)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.whatsapp.com/download/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.github.eneshecan.WhatsAppForLinux`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install whatsapp-for-linux`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

