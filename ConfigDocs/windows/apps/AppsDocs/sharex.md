# SHAREX



## Розділ &#127991; Графіка &#128443;

>&#128161; Це безкоштовне програмне забезпечення з відкритим вихідним кодом для створення скріншотів та скрінкастів для Windows.

- **версія програми &#128230;**: :one: :five: . :zero: . :zero:
- **розробник &#128422;**: ShareX Team
- **офіційний сайт**: [посилання &#128279;](https://getsharex.com/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9NBLGGH4Z1SP?hl=uk-ua&gl=UA)
- &#128189; з **Steam store** за [посиланням &#128279;](https://store.steampowered.com/app/400040/ShareX/)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://getsharex.com/)
- &#128229; з Хмарного сховища &#9729;

