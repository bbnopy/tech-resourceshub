# ONECOMMANDER



## Розділ &#127991; Інструменти &#129520;

>&#128161; Сучасний файловий менеджер для Windows &#128421; 10 і Windows &#128421; 11. Функції включають вкладки, двопанельні браузери, навігацію по стовпчиках, вбудований попередній перегляд, систему тем, кольорові мітки та багато іншого. Він безкоштовний для домашнього використання і не містить реклами.

- **версія програми &#128230;**: :three: . :five: :zero: . :zero: . :zero:
- **розробник &#128422;**: Milos Paripovic
- **офіційний сайт**: [посилання &#128279;](https://onecommander.com/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/onecommander/9NBLGGH4S79B?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://onecommander.com/)
- &#128189; за допомогою терміналу:
  - **Chocolatey**: ```choco install onecomander```
  - **Scoop**:

```powershell
scoop bucket add extras
scoop install onecommander
```
