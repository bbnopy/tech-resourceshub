# HBUILDERX



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; H - перша буква HTML, Builder - конструктор, X - наступна версія HBuilder. Нас також називають HX. HX is lightweight but powerful IDE.

- **версія програми &#128230;**: :three: . :eight: . :one: :two: . :two: :zero: :two: :three: :zero: :eight: :one: :seven:
- **розробник &#128422;**: DCloud
- **офіційний сайт**: [посилання &#128279;](https://www.dcloud.io/hbuilderx.html)
    
## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.dcloud.io/hbuilderx.html)
- &#128229; з Хмарного сховища &#9729;
