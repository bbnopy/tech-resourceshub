# GITKRAKEN



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; GitKraken пропонує набір інструментів, які допомагають розробникам візуалізувати, керувати та співпрацювати з Git-репозиторіями

- **версія програми &#128230;**: :nine: . :one: :zero: . :zero:
- **розробник &#128422;**: Axosoft, LLC DBA GitKraken
- **офіційний сайт**: [посилання &#128279;](https://www.gitkraken.com/)

## Операційні Системи

#### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.gitkraken.com/download)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.gitkraken.com/download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.axosoft.GitKraken`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install gitkraken --classic`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install gitkraken --candidate --classic`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install gitkraken --beta --classic`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install gitkraken --edge --classic`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

