# CAESIUM IMAGE COMPRESSOR


> :information_source: **Інформація:** Компресор зображень. Стискайте фотографії до 90% без видимої втрати якості.

***
:open_file_folder: **Розділ:** :framed_picture: *Графіка*
***

## Про Caesium Image Compressor

| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :two: . :seven: . :one: |
| :computer: **розробник** | SaeraSoft & Matteo Paonessa |
| **сайт** | [:link: посилання](https://saerasoft.com/caesium) |

## Встановлення

### :desktop_computer: Windows

* :inbox_tray: Завантажити з сайту за [:link: посиланням](https://saerasoft.com/caesium#downloads)
* :inbox_tray: Завантаження з :cloud: Хмарного сховища

