# MIXER


> :information_source: **Інформація:** Це безкоштовний і простий у використанні інструмент, який дозволяє текстурувати 3D-активи за допомогою 3D-малювання, ліплення, малювання шарів і процедурних масок.

***
:open_file_folder: **Розділ** :framed_picture: *Графіка*
***

## Про Mixer

| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :two: :zero: :two: :two: . :one: . :one: |
| :computer: **розробник** | Quixel |
| **сайт** | [:link: посилання](https://quixel.com/mixer) |

## Встановлення

### :desktop_computer: Windows

- :inbox_tray: Завантажити з сайту за [:link: посиланням](https://quixel.com/products/mixer)
- :inbox_tray: Завантажити з :cloud: Хмарного сховища

