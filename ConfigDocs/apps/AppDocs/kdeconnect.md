# KDECONNECT



## Розділ &#127991; Інструменти &#129520;

>&#128161; Забезпечує зв'язок між усіма вашими пристроями. Створено для таких людей, як ви.

- **версія програми &#128230;**: :two: :three: . :zero: :eight: . :three:
- **розробник &#128422;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://kdeconnect.kde.org/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9N93MRMSXBF0?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://kdeconnect.kde.org/download.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://kdeconnect.kde.org/download.html)

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
- &#128189; за допомогою терміналу: `sudo dnf install kdeconnectd`
