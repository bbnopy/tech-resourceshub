# SIGNAL DESKTOP



## Розділ &#127991; Інтернет &#127758;

>&#128161; Це багатоплатформова служба зашифрованих миттєвих повідомлень, розроблена Signal Foundation та Signal Messenger LLC.

- **версія програми &#128230;**: :six: . :three: :two: . :zero:
- **розробник &#128422;**: Signal Foundation, Signal Messenger LLC
- **офіційний сайт**: [посилання &#128279;](https://www.signal.org)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.signal.org/download/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.signal.org/download/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.signal.Signal`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install signal-desktop`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install signal-desktop --candidate`
#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

