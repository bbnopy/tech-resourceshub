# ZOC TERMINAL



## Розділ &#127991; Інструменти &#129520;

>&#128161; Популярний комп'ютерний емулятор терміналу та програмний &#128230; клієнт Telnet.

- **версія програми &#128230;**: :eight: . :zero: :six: . :five:
- **розробник &#128422;**: EmTec
- **офіційний сайт**: [посилання &#128279;](https://www.emtec.com/index.html)

## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.emtec.com/download.html)
- &#128229; з Хмарного сховища &#9729;
