# THUNDERBIRD



## Розділ &#127991; Інтернет &#127758;

>&#128161; Безкоштовний поштовий клієнт, новин та RSS з відкритим початковим кодом. Заснований на технології XUL.

- **версія програми &#128230;**: :one: :one: :five: . :three: . :one:
- **розробник &#128422;**: Mozilla Foundation, Mozilla Messaging
- **офіційний сайт**: [посилання &#128279;](https://www.thunderbird.net/uk/)

## Операційні Системи

#### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.thunderbird.net/uk/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.thunderbird.net/uk/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.mozilla.Thunderbird`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install thunderbird`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install thunderbird --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install thunderbird --beta`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

