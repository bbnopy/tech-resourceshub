# BING WALLPAPER


> :information_source: **Інформація:** Це колекція красивих зображень з усього світу, які були розміщені на домашній сторінці Bing. Ви не лише щодня бачитимете нове зображення на своєму робочому столі, але й зможете переглядати зображення та дізнаватися, звідки вони походять.

***
:open_file_folder: **Розділ:** :framed_picture: *Графіка*
***

## Про Bing Wallpaper
 
| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :two: . :one: . :one: . :one: |
| :computer: **розробник** | Microsoft Corporation |
| **сайт** |  [:link: посилання](https://www.microsoft.com/en-us/bing/bing-wallpaper)

## Встановлення

### :desktop_computer: Windows

* :inbox_tray: Завантаження з сайту Microsoft за [:link: посиланням](https://www.microsoft.com/en-us/bing/bing-wallpaper)
* :inbox_tray: Завантаження з :cloud: Хмарного сховища

