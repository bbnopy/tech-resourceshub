# KRITA



## Розділ &#127991; Графіка &#128443;

>&#128161; Растровий графічний редактор з відкритим початковим кодом, написаний на Qt і розроблений переважно для цифрового живопису та анімації.

- **версія програми &#128230;**: :five: . :one: . :five:
- **розробник &#128422;**: Krita Foundation, KDE
- **офіційний сайт**: [посилання &#128279;](https://krita.org)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/krita/9N6X57ZGRW96)
- &#128189; з **Steam store** за [посиланням &#128279;](https://store.steampowered.com/app/280680/Krita/)
- &#128189; з **Epic game store** за [посиланням &#128279;](https://store.epicgames.com/en-US/p/krita)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://krita.org/en/download/krita-desktop/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; з **Steam store** за [посиланням &#128279;](https://store.steampowered.com/app/280680/Krita/)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://krita.org/en/download/krita-desktop/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.kde.krita`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install krita`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install krita --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

