# OPENTOONZ



## Розділ &#127991; Графіка &#128443;

>&#128161; Програмне забезпечення для виробництва 2D анімації.

- **версія програми &#128230;**: :one: . :seven: . :one:
- **розробник &#128422;**: OpenToonz
- **офіційний сайт**: [посилання &#128279;](https://opentoonz.github.io/e/index.html)

## Операційні Системи

### Windows &#128421;

- &#128229; з сайту за [посиланням &#128279;](https://opentoonz.github.io/e/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з :octocat: за [посиланням &#128279;](https://github.com/opentoonz/opentoonz/releases/tag/v1.7.1)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub io.github.OpenToonz`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install opentoonz`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install opentoonz --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

