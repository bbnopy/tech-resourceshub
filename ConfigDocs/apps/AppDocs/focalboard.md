# FOCALBOARD



## Розділ &#127991; Інтернет &#127758;

>&#128161; Управління проектами для технічних команд.

- **версія програми &#128230;**: :seven: . :one: :one: . :three:
- **розробник &#128422;**: Mattermost
- **офіційний сайт**: [посилання &#128279;](https://www.focalboard.com/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9NLN2T0SX9VF?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.focalboard.com/docs/personal-edition/desktop/#windows)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.focalboard.com/docs/personal-edition/desktop/#linux-desktop)

