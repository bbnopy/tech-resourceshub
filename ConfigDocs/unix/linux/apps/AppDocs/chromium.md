# CHROMIUM



## Розділ &#127991; Інтернет &#127758;

>&#128161; До проектів Chromium належать Chromium і ChromiumOS, проекти з відкритим вихідним кодом, що лежать в основі браузера Google Chrome і Google ChromeOS відповідно.

- **версія програми &#128230;**: :one: :two: :two: . :zero: . :six: :two: :six: :one: . :one: :two: :eight:
- **розробник &#128422;**: Google LLC
- **офіційний сайт**: [посилання &#128279;](https://www.chromium.org/chromium-projects/)

## Операційні системи

### Linux &#128039;

-  &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.chromium.Chromium`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: ```sudo snap install chromium```
  - **версія програми &#128230;** latest candidate команда для терміналу: ```sudo snap install chromium --candidate```
  - **версія програми &#128230;** latest beta команда для терміналу: ```sudo snap install chromium --beta```
  - **версія програми &#128230;** latest edge команда для терміналу:```sudo snap install chromium --edge```
