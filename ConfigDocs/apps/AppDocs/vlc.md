# VLC



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; Кросплатформений та вільний плеєр проєкту VideoLAN.

- **версія програми &#128230;**: :three: . :zero: . :one: :eight:
- **розробник &#128422;**: VideoLAN
- **офіційний сайт**: [посилання &#128279;](https://www.videolan.org/vlc/)

## Операційні Системи

#### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/vlc/XPDM1ZW6815MQM)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.videolan.org/vlc/download-windows.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.videolan.org/vlc/#download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.videolan.VLC`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install vlc`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install vlc --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install vlc --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install vlc --edge`

#### fedora WORKSTATION

>&#128161; якщо встановлено &#128189; **RPM Fusion** 

- &#128189; з **Центру програмного &#128230; забезпечення**
- &#128189; за допомогою терміналу: `sudo dnf install vlc`

