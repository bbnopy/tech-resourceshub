# ANYDESK



## Розділ &#127991; Інтернет &#127758;

>&#128161; Потужне програмне &#128230; забезпечення віддаленої допомоги.

- **версія програми &#128230;**:
  - Windows &#128421;: :eight: . :zero: . :three:
  - Linux &#128039;: :six: . :three: . :zero:
- **розробник &#128422;**: AnyDesk Software GmbH
- **офіційний сайт**: [посилання &#128279;](https://anydesk.com/en)

## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://anydesk.com/en/downloads/windows)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://anydesk.com/en/downloads/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.microsoft.Edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

