# FIREFOX



## Розділ &#127991; Інтернет &#127758;

>&#128161; Вільний безкоштовний браузер з відкритим кодом, використовує ядро Quantum.

- **версія програми &#128230;**: :one: :one: :eight: . :zero:
- **розробник &#128422;**: Mozilla Foundation, Mozilla Messaging
- **офіційний сайт**: [посилання &#128279;](https://www.mozilla.org/uk/firefox/new/)

## Операційні Системи

#### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/mozilla-firefox/9NZVDKPMR9RD)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.mozilla.org/uk/firefox/download/thanks/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.mozilla.org/uk/firefox/download/thanks/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.mozilla.firefox`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install firefox`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install firefox --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install firefox --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install firefox --edge`
  - **версія програми &#128230;** esr stable команда для терміналу: `sudo snap install firefox --channel=esr/stable`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

## Налаштування

### Увімкнути або Вимкнути Апаратне Прискорення

>&#128161; Запускаємо Firefox. Нам потрібно отримати доступ до розширених параметрів конфігурації, які можна відкрити, ввівши `about:config` в адресному рядку. Може попередження, що ви збираєтеся отримати доступ до деяких розширених налаштувань >&#128161; конфігурації - просто натисніть "Прийняти ризик і продовжити".

>&#128161; У рядку пошуку параметрів введіть `layers.acceleration.force-enabled`. Коли опція з'явиться, буде написано **"false"**, якщо вона вимкнена, і **"true"**, якщо увімкнена. Щоб увімкнути або вимкнути цей параметр, клацніть крайню праву іконку.

>&#128161; Після увімкнення або вимкнення, перезапустіть браузер Firefox.

### Функція WebRender

>&#128161; Увімкнути WebRender у Firefox можна вручну встановити значення параметра `gfx.webrender.enabled` або `gfx.webrender.all` в **true** на сторінці `about:config` і перезапустити браузер. Щоб вимкнути вручну встановити значення параметра `gfx.webrender.enabled` або `gfx.webrender.all` в **false** на сторінці `about:config` і перезапустити браузер.

>&#128161; Запустіть Firefox з **MOZ_WEBRENDER=1** як змінною оточення 1. Для відключення запустіть Firefox з **MOZ_WEBRENDER=0** в якості змінної оточення 1.

