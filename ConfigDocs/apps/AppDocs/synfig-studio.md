# SYNFIG STUDIO



## Розділ &#127991; Графіка &#128443;

>&#128161; Це безкоштовне програмне забезпечення для 2D-анімації з відкритим вихідним кодом, яке дозволяє створювати анімацію кіношної якості, використовуючи векторні та растрові ілюстрації.

- **версія програми &#128230;**: :one: . :five: . :one: - :six: . fc :three: :nine:
- **розробник &#128422;**: Robert Quattlebaum
- **офіційний сайт**: [посилання &#128279;](https://www.synfig.org/)

## Операційні Системи

### Windows &#128421;

- &#128229; з сайту за [посиланням &#128279;](https://synfig.gumroad.com/l/synfig)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://synfig.gumroad.com/l/synfig)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.synfig.SynfigStudio`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install synfigstudio`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install synfigstudio --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install synfigstudio --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install synfigstudio --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

