# ELISA



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; Музичний програвач, який розроблено спільнотою KDE.

- **версія програми &#128230;**: :two: :three: . :zero: :eight: . :three:
- **розробник &#127970;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://apps.kde.org/uk/elisa/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9PB5MD7ZH8TL?hl=uk-ua&gl=UA)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.kde.elisa`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install elisa`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install elisa --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

