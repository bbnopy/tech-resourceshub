# KMAHJONGG



## Розділ &#127991; Ігри &#127918;

>&#128161; Це захоплива гра на дошці, створена на основі відомої східної гри у маджонґ.

- **версія програми &#128230;**: :zero: . :nine: . :two: :three: :zero: :eight: :two:
- **розробник &#128422;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://apps.kde.org/uk/kmahjongg/)

## Операційни Системи

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.kde.kmahjongg`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install kmahjongg`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install kmahjongg --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

