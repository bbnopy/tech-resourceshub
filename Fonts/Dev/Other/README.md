# My Fonts List

001. Aldrich
002. Algerian
003. Andale Mono

005. Anka/Coder
006. Anonymous Pro
007. APL2741
008. APL333
009. APL385 Unicode
010. Architects Daughter
010. Arial
011. Audiowide
012. Aurulent Sans Mono
013. AverageMono
014. Azeret Mono Thin
015. b612 Regular
016. Bahnschrift
017. basis33
018. Baskerville Old Face
019. Bauhaus 93
020. Bedstead
021. Bell MT
022. Berlin Sans FB
023. Bernard MT
024. Binchotan_Sharp
025. Bitstream Vera Sans
026. Bitstream Vera Sans Mono
027. Bitstream Vera Serif
028. Blackadder ITC
029. Bodoni MT
030. Book Antique
031. Bookman Old Style
032. BPmono
033. Bradley Hand ITC
034. Britannic
035. Brodway
036. Brush Script MT
037. Calibri
038. Californian FB
039. Calisto MT
040. Cambria
041. Cambria Math
042. CamingoCode
043. Candara
044. Cascadia Code
045. Cascadia Mono
046. Castellar
047. Centaur
048. Century
049. Century Gothic
050. Century Schoolbook
051. Chiller
052. Code New Roman
053. Code Red October
054. CodingFontTobi
055. Colonna MT
056. Comfortaa
057. Comic Mono
058. Comic Sans MS
059. Comic Shanns
060. Consola Mono
061. Consolas
062. Constantia
063. Cooper
064. Copperplate Gothic
065. Corbel
066. Cormorant
067. Cormorant Garamond
068. Cormorant Infant
069. Cormorant SC
070. Cormorant Unicase
071. Cormorant Upright
072. CosmicSansNeueMono
073. Courier New
074. Courier Prime
075. Courier Prime Code
076. Cousine
077. CozetteVector
078. CQ Mono
079. Creep
080. Crisp
081. Crystal
082. Curlz MT
083. Cutive Mono
084. D2Coding
085. D2Coding Lignature
086. DEC Terminal Modern
087. DejaVu LGC Sans
088. DejaVu LGC Sans Mono
089. DejaVu LGC Serif
090. DejaVu Math TeX Gyre
091. DejaVu Sans
092. DejaVu Sans Code
093. Dejavu Sans Mono
094. Dejavu Sans Mono - Bront
095. Dejavu Sans Mono for Powerline
096. Dejavu Serif
097. Didact Ghotic
098. Dina ttf 10px
099. DM Mono
100. Drafting* Mono
101. Droid Sans Mono
102. Dubai
103. Ebrima
104. Edlo
105. Edlo Nerd Font
106. Edwardian Script ITC
107. EffectsEighty
108. Elephant
109. Engravers MT
110. Envy Code R
111. Envy Code R VS
112. Eras ITC
113. Espresso
114. Exo 2
115. Fairfax
116. Fairfax Hax HD
117. Fairfax HD
118. Fairfax Serif
119. Fairfax Serif SM
120. Fairfax SM
121. Fairfax SM HD
122. Fantasque Sans Mono
123. Felix Titling
124. Fifteen
125. Fira Code
126. Fira Mono
127. Fixedsys Excelsior 3.01
128. Footlight MT
129. Forte
130. Franklin Gothic
131. Franklin Gothic Book
132. FreeMono
133. FreeSans
134. FreeSerif
135. Freestyle Script
136. French Script MT
137. Gabriola
138. Gadugi
139. Garamond
140. Georgia
141. Gigi
142. Gill Sans
143. Gill Sans MT
144. Gloriah Hallelujah
144. Gloucester MT
145. Go Mono
146. Goudy Old Style
147. Goudy Stout
148. Hack
149. Haettenschweiler
150. Happy Monkey
151. Harlow Solid
152. Harrington
153. Hasklig
154. Hello Alpha
155. Hermit
156. High Tower Text
157. HoloLens MDL2 Assets
158. Hyperfont
159. iA Writer Duo S
160. iA Writer Duo V
161. iA Writer Duospace
162. iA Writer Mono S
163. iA Writer Mono V
164. iA Writer Quattro S
165. iA Writer Quattro V
166. IBM 3270
167. IBM 3270 Semi
168. IBM Plex Mono
169. Impact
170. Imprint MT Shadow
171. Inconsolata
172. Inconsolata Condensed
173. Inconsolata Expanded
174. Inconsolata ExtraCondensed
175. Inconsolata ExtraExpanded
176. Inconsolata SemiCondensed
177. Inconsolata SemiExpanded
178. Inconsolata UltraCondensed
179. Inconsolata UltraExpanded
180. Informal Roman
181. Ink Free
182. Input Mono
183. Input Mono Compressed
184. Input Mono Condensed
185. Input Mono Narrow
186. Input Sans
187. Input Sans Compressed
188. Input Sans Condensed
189. Input Sans Narrow
190. Input Serif
191. Input Serif Compressed
192. Input Serif Condensed
193. Input Serif Narrow
194. Iosevka
195. Izayoi Monospaced
196. Javanese Text
197. JetBrains Mono
198. JetBrains Mono NL
199. Jokerman
200. Juice ITC
201. JuliaMono
202. Jura
203. KaiTi
204. Kernel Panic NBP
205. Kristen ITC
206. Kunstler Script
207. Latin Modern Mono
208. Latin Modern Mono Caps
209. Latin Modern Mono Light
210. Latin Modern Mono Light Cond
211. Latin Modern Mono Prop
212. Latin Modern Mono Prop Light
213. Latin Modern Mono Slanted
214. League Mono
215. Leelawadee
216. Leelawadee UI
217. Lekton
218. Liberation Mono
219. Lilex
220. Linux Libertine
221. Linux Libertine Capitals
222. Linux Libertine Display
223. Linux Libertine Display Capitals
224. Linux Libertine Initials
225. Linux Libertine Slanted
226. Lotion
227. Lucida Bright
228. Lucida Calligraphy
229. Lucida Console
230. Lucida Fax
231. Lucida Handwrighting
232. Lucida Sans
233. Lucida Sans Typewriter
234. Lucida Sans Unicode
235. Luculent
236. Luxi Mono
237. M+ 1m
238. M+ 2m
239. Magneto
240. Maiandra GD
241. Malgun Gothic
242. Matura MT Script Capitals
243. Menlo
244. Mensch
245. Meslo LG L
246. Meslo LG L DZ
247. Meslo LG M
248. Meslo LG M DZ
249. Meslo LG S
250. Meslo LG S DZ
251. Microsoft Himalaya
252. Microfsoft JhengHei
253. Microsoft JhengHei UI
254. Microsoft New Tai Lue
255. Microsoft PhagsPa
256. Microsoft Sans Serif
257. Microsoft Tai Le
258. Microsoft Uighur
259. Microsoft YaHei
260. Microsoft YaHei UI
261. Microsoft Yi Baiti
262. MingLiU_HKSCS-ExtB
263. MingLiU-ExtB
264. Misc Fixed 6x13
265. Mistral
266. Modern No. 20
267. Monaco
268. Mongolian Baiti
269. monofur
270. Monoid
271. mononoki
272. Monospace
273. Monotype Corsiva
274. Montserrat
275. Montserrat Alternates
276. Moon 2.0
277. MS Gothic
278. MS Outlook
279. MS PGothic
280. MS Reference Sans Serif
281. MS UI Gothic
282. MV Boli
283. Myanmar Text
284. NanumGothicCoding
285. New Heterodox Mono
286. Niagara Engraved
287. Niagara Solid
288. Nimbus Mono
289. Nirmala UI
290. NK57 Monospace
291. NotCurierSans
292. Noto Mono
293. Noto Sans
294. Nouveau IBM
295. Nouveau IBM Stretch
296. NovaMono
297. NSimSun
298. OCR A
299. Office Code Pro
300. Office Code Pro D
301. Old Enflish Text MT
302. Onyx
303. Orbitron
304. Overpass Mono
305. Oxygen Mono
306. Palace Script MT
307. Palatino Linotype
308. Panic Sans
309. Papyrus
310. Parchment
311. Perfect DOS VGA 437
312. Perfect DOS VGA 437 Win
313. Perpetua
314. Perpetua Titling MT
315. PixelCarnageMonoTT
316. Play
317. Playbil
318. PMingLiU-ExtB
319. Pointfree
320. Poiret One
321. Poor Richard
322. Press Start 2P
323. Print Char 21
324. Pristina
325. ProFontWindows
326. ProggyCleanTT
327. ProggyCleanTT CE
328. ProggyCleanTTSZ
329. ProggyCleanTTSZBP
330. ProggySmallTT
331. ProggySquareTT
332. ProggySquareTTSZ
333. ProggyTinyTT
334. ProggyTinyTTSZ
335. PT Mono
336. Quinze
337. Rage
338. Ravie
339. Red Hat Display
340. Red Hat Display VF
341. Red Hat Mono
342. Red Hat Mono VF
343. Red Hat Text
344. Red Hat Text VF
345. Roboto Flex
346. Roboto Mono
347. Rockwell
348. saxMono
349. Script MT
350. Segoe Fluent Icons
351. Segoe MDL2 Assets
352. Segoe Print
353. Segoe Script
354. Segoe UI
355. Segoe UI Emoji
356. Segoe UI Historic
357. Segoe UI Mono W01
358. Segoe UI Symbol
359. Segoe UI Variable
360. Selectric
361. SF Mono
362. Share Tech Mono
363. Showcard Gothic
364. SimSun
365. SimSun-ExtB
366. Sitka
367. Smooth Pet
368. Snap ITC
369. Sometype Mono
370. Source Code Pro
371. Source Sans Pro
372. Space Mono
373. Spleen
374. Stencil
375. Sudo
376. SVI Basic Manual
377. Sylfaen
378. Symbol
379. Tahoma
380. Teletoon Lowercase
381. Teletoon Lowercase V2
382. Tempus Sans ITC
383. Terminus
384. TeX Gyre Cursor
385. The Sans Mono-
386. Times New Roman
387. Tiny
388. Topaz-8
389. Trebuchet MS
390. Tw Cen MT
391. Ubuntu Mono
392. Ubuntu Mono - Bont
393. Unifont
394. Unifont CSUR
395. Unifont Sample
396. Unifont Upper
397. Verdana
398. Verily Serif Mono
399. Victor Mono
400. Viner Hand ITC
401. Vivaldi
402. Vladimir Script
403. VT323
404. Webdings
405. Wide Latin
406. Wingdings
407. Wingdings 2
408. Wingdings 3
409. Yu Gothic
410. Yu Gothic UI


000. Envy Code B ???
001. Triskweline???
