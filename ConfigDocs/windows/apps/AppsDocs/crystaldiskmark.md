# CRYSTALDISKMARK



## Розділ &#127991; Інструменти &#129520;


>&#128161; Це просте програмне &#128230; забезпечення для тестування дисків.

- **версія програми &#128230;**: :eight: . :zero: . :four:
- **розробник &#128422;**: Crystal Dew World
- **офіційний сайт**: [посилання &#128279;](https://crystalmark.info/en/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/crystaldiskmark/9NBLGGH4Z6F2?hl=uk-ua&gl=UA)
- &#128189; з **Microsoft Store** Shizuku Edition за [посиланням &#128279;](https://apps.microsoft.com/detail/crystaldiskmark-shizuku-edition/9NBLGGH536CC?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://crystalmark.info/en/software/crystaldiskmark/)
- &#128229; з Хмарного сховища &#9729;
