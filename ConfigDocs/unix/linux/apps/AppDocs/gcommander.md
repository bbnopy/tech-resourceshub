# GNOME COMMANDER



## Розділ &#127991; Інструменти &#129520;

>&#128161; Вільний двохпанельний файловий менеджер, що працює під GNU/Linux у середовищі GNOME. Створений у традиціях таких файлових менеджерів, як Norton Commander та Midnight Commander.

- **версія програми &#128230;**: :one: . :one: :six: . :one:
- **розробник &#128422;**: Piotr Eljasiak, Assaf Gordon, Magnus Stålnacke, Marcus Bjurman
- **офіційний сайт**: [посилання &#128279;](https://gcmd.github.io/)

## Операційні Cистеми

### Linux &#128039;

- &#128229; з офіційного сайту [посилання &#128279;](https://gcmd.github.io/download.html)
- &#128229; з **Gnome download server** [посилання &#128279;](https://download.gnome.org/sources/gnome-commander/)

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
