# AUDACITY



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; Вільний багатоплатформовий редактор звукових файлів, орієнтований на роботу з декількома доріжками. Дозволяє виконувати такі функції, як редагування звукових файлів, запис, о цифрування звуку.

- **версія програми &#128230;**: :three: . :three: . :three:
- **розробник &#128422;**: MuseGroup &amp; The Audacity Open Source Community
- **офіційний сайт**: [посилання &#128279;](https://www.audacityteam.org)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/audacity/XP8K0J757HHRDW)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.audacityteam.org/download/windows/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.audacityteam.org/download/linux/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.audacityteam.Audacity`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install audacity`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install audacity --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

