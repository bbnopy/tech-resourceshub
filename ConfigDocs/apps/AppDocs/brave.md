# BRAVE BROWSER



## Розділ &#127991; Інтернет &#127758;

>&#128161; Інтернет-браузер із відкритим вихідним кодом, розроблений компанією Brave Software Inc. на основі браузера Chromium.

- **версія програми &#128230;**: :one: . :five: :eight: . :one: :three: :five:
- **розробник &#128422;**: Brave Software
- **офіційний сайт**: [посилання &#128279;](https://brave.com)

## Операційні Системи

#### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/brave-browser/XP8C9QZMS2PC1T)
- &#128189; з **Epic games store** за [посиланням &#128279;](https://store.epicgames.com/en-US/p/brave)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://brave.com/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.videolan.org/vlc/#download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.brave.Browser`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install brave`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install brave --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install brave --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install brave --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
- &#128189; за допомогою терміналу:

- **Release Channel**

```bash
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo htpps://brave-browser-rpm-release.s3.brave.com/brave-browser.repo
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf install brave-browser
```

- **Beta Channel**

```bash
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo https://brave-bbrowser-rpm-beta.s3.brave.com/brave-browser-beta.repo
sudo rpm --import https://brave-browser-rpm-beta.s3.brave.com/brave-core-nightly.asc
sudo dnf install brave-browser-beta
```

- **Nightly Channel**

```bash
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo https://brave-bbrowser-rpm-beta.s3.brave.com/brave-browser-nightly.repo
sudo rpm --import https://brave-browser-rpm-beta.s3.brave.com/brave-core-nightly.asc
sudo dnf install brave-browser-nightly
```

