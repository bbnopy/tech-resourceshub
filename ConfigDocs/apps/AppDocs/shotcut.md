# SHOTCUT



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; це безкоштовний кросплатформний відеоредактор з відкритим вихідним кодом.

- **версія програми &#128230;**: :two: :three: . :one: :one:
- **розробник &#128422;**: Meltytech, LLC
- **офіційний сайт**: [посилання &#128279;](https://shotcut.org/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9PLNFFL3P6LR?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://shotcut.org/download/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://shotcut.org/download/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.shotcut.Shotcut`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install shotcut --classic`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install shotcut --candidate --classic`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install shotcut --beta --classic`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install shotcut --edge --classic`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

