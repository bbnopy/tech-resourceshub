# DOCKER



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Надає набір інструментів розробки, сервісів, надійного контенту та автоматизації, які можна використовувати як окремо, так і разом, щоб пришвидшити створення безпечних додатків.

- **версія програми &#128230;**: :four: . :two: :four: . :zero:
- **розробник &#128422;**: Docker, Inc.
- **офіційний сайт**: [посилання &#128279;](https://www.docker.com/)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://hub.docker.com/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

>&#128161; Docker Desktop для Linux запускає віртуальну машину (VM), тому створює і використовує власний контекст докера desktop-linux під час запуску.
>Це означає, що образи і контейнери, розгорнуті на Linux Docker Engine (до встановлення), недоступні у Docker Desktop для Linux.

#### Підтримувані платформи

| Платформа |    x86_64/amd64    |
|:---------:|:------------------:|
|  Ubuntu   | :white_check_mark: |
|  Debian   | :white_check_mark: |
|  Fedora   | :white_check_mark: |

- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install docker`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install docker --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install docker --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install docker --edge`
  - **версія програми &#128230;** core :one: :eight: stable команда для терміналу: `sudo snap install docker --channel=core18/stable`

#### fedora WORKSTATION

##### Встановлення за допомогою rpm-репозиторію

Перед першим встановленням Docker Engine на новий хост-машину, вам потрібно налаштувати репозиторій Docker. Після цього ви зможете встановлювати та оновлювати Docker з репозиторію.

###### Налаштування сховища

Встановіть пакунок `dnf-plugins-core` (який містить команди для керування вашими сховищами DNF) та налаштуйте сховище.

```bash
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
```

Install Docker Engine

```bash
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

