# GNU IMAGE MANIPULATION PROGRAM( GIMP )



## Розділ &#127991; Графіка &#128443;

>&#128161; Растровий графічний редактор, із деякою підтримкою векторної графіки.

- **версія програми &#128230;**: :two: . :one: :zero: . :three: :four:
- **розробник &#128422;**: GIMP Foundation
- **офіційний сайт**: [посилання &#128279;](https://www.gimp.org)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/blender/9PP3C07GTVRH)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.gimp.org/downloads/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.gimp.GIMP`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install gimp`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install gimp --edge`
  - **версія програми &#128230;** preview edge команда для терміналу: `sudo snap install gimp --channel=preview/edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

