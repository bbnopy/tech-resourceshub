# VMWARE WORKSTATION PLAYER



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Це платформа для запуску однієї віртуальної машини на ПК з Windows або Linux для надання керованих корпоративних робочих столів.

- **версія програми &#128230;**: :one: :seven:
- **розробник &#128422;**: VMware, Inc.
- **офіційний сайт**: [посилання &#128279;](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)

## Операційні Системи

#### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)
