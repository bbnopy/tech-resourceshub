# CONTRIBUTING TO TECH-RESOURCESHUB


>**:information_source: Інформація:** Опис значення символів, які використовуються в репозиторії.

|      |                 |                     |               |           |                         |                                               |
|:----:|-----------------|---------------------|---------------|-----------|-------------------------|-----------------------------------------------|
|:memo:|Юнікод ім'я(`Memo`)|Юнікод число(`U+1F4DD`)|HTML(`&#128221;`)|CSS(`\1F4DD`)|Шорткод(`:memo:`, `:pencil:`)|символ для позначення документації у гіт коміті|
|:desktop_computer:|Юнікод ім'я(`Desktop Computer`)|Юнікод число(`U+1F5A5`, `U+FE0F`)|HTML(`&#128421;`)|CSS(`\1F5A5`)|Шорткод(`:desktop_computer:`, `:desktop:`)|символ для позначення операційної системи Windows|
|:penguin:|Юнікод ім'я(`Penguin`)|Юнікод число(`U+1F427`)|HTML(`&#128039;`)|CSS(`\1F427`)|Шорткод(`:penguin:`)|символ для позначення операційної системи Linux|
|:package:|Юнікод ім'я(`Package`)|Юнікод число(`U+1F4A1`)|HTML(`&#128230;`)|CSS(`\1F4A1`)|Шорткод(`:package:`)|символ позначає програму або пакунок|
|:information_source:|Юнікод ім'я(`Information Source`)|Юнікод число(`U+2139`)|HTML(`&#8505;`)|CSS(`\2139`)|Шорткод(`:information:`, `:information_source:`)|символ позначає інформацію|
|:exclamation:|Юнікод ім'я(`Heavy Exclamation Mark Symbol`)|Юнікод число(`U+2757`)|HTML(`&#10071;`)|CSS(`\2757`)|Шорткод(`:exclamation_mark:`, `:exclamation:`, `:heavy_exclamation_mark:`)|символ позначає важливу інформацію або треба бути уважним|
|:computer:|Юнікод ім'я(`Personal Computer`)|Юнікод число(`U+1F4BB`)|HTML(`&#128187;`)|CSS(`\1F4BB`)|Шорткод(`:laptop:`, `:computer:`)|символ розробкика програми або застосунка|
|:cloud:|Юнікод ім'я(`Cloud`)|Юнікод число(`U+2601`, ` U+FE0F`)|HTML(`&#9729;`)|CSS(`\2601`)|Шорткод(`:cloud:`)|символ хмарного сховища|
|:octocat:| | | | |Шорткод(`:octocat:`)|символ позначає гітхаб|
|:gear:|Юнікод ім'я(`Gear`)|Юнікод число(`U+2699`, `U+FE0F`)|HTML(`&#9881;`)|CSS(`\2699`)|Шорткод(`:gear:`)|символ налаштуваннь|
|:link:|Юнікод ім'я(`Link Symbol`)|Юнікод число(`U+1F517`)|HTML(`&#128279;`)|CSS(`\1F517`)|Шорткод(`:link:`)|символ посилання|
|:inbox_tray:|Юнікод ім'я(`Inbox Tray`)|Юнікод число(`U+1F4E5`)|HTML(`&#128229;`)|CSS(`\1F4E5`)|Шорткод(`:inbox_tray:`)|символ завантаження або встановлення|
|:open_file_folder:|Юнікод ім'я(`Open File Folder`)|Юнікод число(`U+1F4C2`)|HTML(`&#128194;`)|CSS(`\1F4C2`)|Шорткод(`:open_file_folder:`)|символ для позначення розділу|
|:card_index_dividers:|Юнікод ім'я(`Card Index Dividers`)|Юнікод число(`U+1F5C2`)|HTML(`&#128450;`)|CSS(`\1F5C2`)|Шорткод(`:card_index_dividers:`, `:dividers:`)|символ для позначення категорії|
|:frame_photo:|Юнікод ім'я(`Frame with Picture`)|Юнікод число(`U+1F5BC`, `U+FE0F`)|HTML(`&#128444;`)|CSS(`\1F5BC`)|Шорткод(`:framed_picture:`, `:frame_with_picture:`, `:frame_photo:`)|символ для категорії "графіка"|
|:tv:|Юнікод ім'я(`Television`)|Юнікод число(`U+1F4FA`)|HTML(`&#128250;`)|CSS(`\1F4FA`)|Шорткод(`:tv:`, `:television:`)|символ для категорії "звук та відео/multimedia"|
|:video_game:|Юнікод ім'я(`Video Game`)|Юнікод число(`U+1F3AE`)|HTML(`&#127918;`)|CSS(`\1F3AE`)|Шорткод(`:video_game:`)|символ для категорії "ігор/games"|
|:toolbox:|Юнікод ім'я(`Toolbox`)|Юнікод число(`U+1F9F0`)|HTML(`&#129520;`)|CSS(`\1F9F0`)|Шорткод(`:toolbox:`)|символ для категорії "інструментів/utilities"|
|:hammer_and_wrench:|Юнікод ім'я(`Hammer and Wrench`)|Юнікод число(`U+1F6E0`, `U+FE0F`)|HTML(`&#128736;`)|CSS(`\1F6E0`)|Шорткод(`:hammer_and_wrench:`, `:tools:`)|символ для категорії "інструментів розробника/development"|
|:globe_with_meridians:|Юнікод ім'я(`Globe with Meridians`)|Юнікод число(`U+1F310`)|HTML(`&#127760;`)|CSS(`\1F310`)|Шорткод(`:globe_with_meridians:`)|символ для категорії "інтернету/internet"|
|:bookmark_tabs:|Юнікод ім'я(`Bookmark Tabs`)|Юнікод число(`U+1F4D1`)|HTML(`&#128209;`)|CSS(`\1F4D1`)|Шорткод(`:bookmark_tabs:`)|символ для категорії "офісу/office"|
|:card_file_box:|Юнікод ім'я(`Card File Box`)|Юнікод число(`U+1F5C3`, `U+FE0F`)|HTML(`&#128451;`)|CSS(`\1F5C3`)|Шорткод(`:card_file_box:`, `:card_box:`)|символ для категорії "систем/system"|
|:zero:|Юнікод ім'я(`Keycap Digit Zero`)|Юнікод число(`U+0030`, `U+30`, `U+FE0F`, `U+20E3`)|HTML(`&#48;`); CSS(`\0030`)|Шорткод(`:zero:`, `:keycap_digit_zero:`)символ для нуля|
|:one:|Юнікод ім'я(`Keycap Digit One`)|Юнікод число(`U+0031`, `U+31`, `U+FE0F`, `U+20E3`)|HTML(`&#49;`)|CSS(`\0031`)|Шорткод(`:one:`, `:keycap_digit_one:`)|символ для одиниці|
|:two:|Юнікод ім'я(`Keycap Digit Two`)|Юнікод число(`U+0032`, `U+32`, `U+FE0F`, `U+20E3`)|HTML(`&#50;`)|CSS(`\0032`)|Шорткод(`:two:`, `:keycap_digit_two:`)|символ для двійки|
|:three:|Юнікод ім'я(`Keycap Digit Three`)|Юнікод число(`U+0033`, `U+33`, `U+FE0F`, `U+20E3`)|HTML(`&#51;`)|CSS(`\0033`)|Шорткод(`:three:`, `:keycap_digit_three:`)|символ для трійки|
|:four:|Юнікод ім'я(`Keycap Digit Four`)|Юнікод число(`U+0034`, `U+34`, `U+FE0F`. `U+20E3`)|HTML(`&#52;`)|CSS(`\0034`)|Шорткод(`:four:`, `:keycap_digit_four:`)|символ для четвірки|
|:five:|Юнікод ім'я(`Keycap Digit Five`)|Юнікод число(`U+0035`, `U+35`, `U+FE0F`, `U+20E3`)|HTML(`&#53;`)|CSS(`\0035`)|Шорткод(`:five:`, `:keycap_digit_five:`)|символ для п'ятірки|
|:six:|Юнікод ім'я(`Keycap Digit Six`)|Юнікод число(`U+0036`, `U+36`, `U+FE0F`, `U+20E3`)|HTML(`&#54;`)|CSS(`\0036`)|Шорткод(`:six:`, `:keycap_digit_six:`)|символ для шістки|
|:seven:|Юнікод ім'я(`Keycap Digit Seven`)|Юнікод число(`U+0037`, `U+37`, `U+FE0F`, `U+20E3`)|HTML(`&#55;`)|CSS(`\0037`)|Шорткод(`:seven:`, `:keycap_digit_seven:`)|символ для сімки|
|:eight:|Юнікод ім'я(`Keycap Digit Eight`)|Юнікод число(`U+0038`, `U+38`, `U+FE0F`, `U+20E3`)|HTML(`&#56;`)|CSS(`\0038`)|Шорткод(`:eight:`, `:keycap_digit_eight`)|символ для вісімки|
|:nine:|Юнікод ім'я(`Keycap Digit Nine`)|Юнікод число(`U+0039`, `U+39`, `U+FE0F`, `U+20E3`)|HTML(`&#57;`)|CSS(`\0039`)|Шорткод(`:nine:`, `:keycap_digit_nine:`)|символ для дев'ятки|

