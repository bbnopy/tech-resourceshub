# LIBRE OFFICE



## Розділ &#127991; Офіс &#128209;

>&#128161; Вільний та крос-платформовий офісний пакет. LibreOffice працює на операційних системах Microsoft Windows, Gnu/Linux та macOS i є одним з провідних вільних аналогів Microsoft Office.

- **версія програми &#128230;**: :seven: . :six:
- **розробник &#128422;**: The Document Foundation
- **офіційний сайт**: [посилання &#128279;](https://www.libreoffice.org)

## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.libreoffice.org/download/download-libreoffice/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.libreoffice.org/download/download-libreoffice/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.libreoffice.LibreOffice`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install libreoffice`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install libreoffice --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install libreoffice --beta`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

