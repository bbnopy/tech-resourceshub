# RIOT GAMES



## Розділ &#127991; Ігри &#127918;

>&#128161; Незалежний творець комп'ютерних ігор зі штаб-квартирою в місті Санта-Моніка.

- **версія програми &#128230;**: :seven: :three: . :zero: . :three:
- **офіційний сайт**: [посилання &#128279;](https://www.riotgames.com/en)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.riotgames.com/en)
- &#128229; з Хмарного сховища &#9729;

## Продукція

- League of Legends
- Valorant
- Teamfight Tactics
- Legends of Runtera
- LOL: Wild Rift
