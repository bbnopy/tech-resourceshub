# KODI



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; Кросплатформне відкрите програмне &#128230; забезпечення для організації медіацентру.

- **версія програми &#128230;**: :two: :zero: . :two:
- **розробник &#128422;**: XBMC Foundation
- **офіційний сайт**: [посилання &#128279;](https://kodi.tv/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/kodi/9NBLGGH4T892)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://kodi.tv/download/windows/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- гайд по встановленню &#128189; за [посиланням &#128279;](https://kodi.wiki/view/HOW-TO:Install_Kodi_for_Linux?https=1)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub tv.kodi.Kodi`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install mir-kiosk-kodi`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install mir-kiosk-kodi --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

