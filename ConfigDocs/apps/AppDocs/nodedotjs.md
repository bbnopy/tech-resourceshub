# NODE.JS



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; JavaScript–оточення побудоване на JavaScript–рушієві Chrome V8.

- **версія програми &#128230;**:
  - LTS версія: :one: :eight: . :one: :eight: . :zero:
  - Поточна версія: :two: :zero: . :eight: . :zero:
- **розробник &#128422;**: Node.js Developers
- **офіційний сайт**: [посилання &#128279;](https://nodejs.org/uk)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://dev.mysql.com/downloads/workbench/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://nodejs.org/uk)
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** :two: :zero: stable команда для терміналу: `sudo snap install node --classic`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install node --channel=latest/edge --classic`
  - **версія програми &#128230;** :two: :one: stable команда для терміналу: `sudo snap install node --channel=21/stable --classic`
  - **версія програми &#128230;** :one: :nine: stable команда для терміналу: `sudo snap install node --channel=19/stable --classic`
  - **версія програми &#128230;** :one: :eight: stable команда для терміналу: `sudo snap install node --channel=18/stable --classic`
  - **версія програми &#128230;** :one: :seven: stable команда для терміналу: `sudo snap install node --channel=17/stable --classic`
  - **версія програми &#128230;** :one: :six: stable команда для терміналу: `sudo snap install node --channel=16/stable --classic`
  - **версія програми &#128230;** :one: :five: stable команда для терміналу: `sudo snap install node --channel=15/stable --classic`
  - **версія програми &#128230;** :one: :four: stable команда для терміналу: `sudo snap install node --channel=14/stable --classic`
  - **версія програми &#128230;** :one: :three: stable команда для терміналу: `sudo snap install node --channel=13/stable --classic`
  - **версія програми &#128230;** :one: :two: stable команда для терміналу: `sudo snap install node --channel=12/stable --classic`
  - **версія програми &#128230;** :one: :one: stable команда для терміналу: `sudo snap install node --channel=11/stable --classic`
  - **версія програми &#128230;** :one: :zero: stable команда для терміналу: `sudo snap install node --channel=10/stable --classic`
  - **версія програми &#128230;** :nine: stable команда для терміналу: `sudo snap install node --channel=9/stable --classic`
  - **версія програми &#128230;** :eight: stable команда для терміналу: `sudo snap install node --channel=8/stable --classic`
  - **версія програми &#128230;** :six: stable команда для терміналу: `sudo snap install node --channel=6/stable --classic`

#### fedora WORKSTATION

Встановити за допомогою терміналу, <stream> відповідає основній версії Node.js.

```bash
dnf module install nodejs:<stream>
```

Щоб переглянути список доступних потоків

```bash
dnf module list nodejs
```

для встановлення Nodejs 18 потрібна така команда

```bash
dnf module install nodejs:18/common
```
