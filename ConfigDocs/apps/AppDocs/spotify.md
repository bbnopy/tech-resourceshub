# SPOTIFY



## Розділ &#127991; Звук та Відео &#128250;

>&#128161; Стримінговий сервіс потокового аудіо, що дозволяє прослуховувати музичні композиції та подкасти.

- **версія програми &#128230;**: :three: . :three: . :two:
- **розробник &#128422;**: Spotify AB
- **офіційний сайт**: [посилання &#128279;](https://open.spotify.com)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/spotify-%D0%BC%D1%83%D0%B7%D0%B8%D0%BA%D0%B0-%D1%82%D0%B0-%D0%BF%D0%BE%D0%B4%D0%BA%D0%B0%D1%81%D1%82%D0%B8/9NCBCSZSJRSB)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://open.spotify.com)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](http://www.sweethome3d.com/download.jsp)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.spotify.Client`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install spotify`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install spotify --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install spotify --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install spotify --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

