# HEROKU



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Хмарна PaaS-платформа, що підтримує ряд мов програмування.

- **версія програми &#128230;**: :eight: . :zero: . :two:
- **розробник &#128422;**: Salesforce.com
- **офіційний сайт**: [посилання &#128279;](https://www.heroku.com/)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.heroku.com/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install heroku --classic`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install heroku --candidate --classic`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install heroku --beta --classic`
   **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install heroku --edge --classic`

#### Ubuntu/Debian

```bash
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
```

#### Arch Linux

Встановіть heroku-cli 7.60.1-1, який підтримується спільнотою.

```bash
yay -S heroku-cli
```

