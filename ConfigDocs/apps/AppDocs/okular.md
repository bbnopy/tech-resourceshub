# OKULAR



## Розділ &#127991; Офіс &#128209;

>&#128161; Багатоплатформний, швидкий і насичений функціями Okular дозволяє читати PDF-документи, комікси та книги в форматі EPub, переглядати зображення, візуалізувати документи Markdown та багато іншого.

- **версія програми &#128230;**: :two: :three: . :zero: :eight: . :three:
- **розробник &#128422;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://okular.kde.org/)

## Операційни Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9N41MSQ1WNM8?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://okular.kde.org/download/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://okular.kde.org/download/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.kde.okular`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install okular`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install okular --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

