# TELEGRAM DESKTOP



## Розділ &#127991; Інтернет &#127758;

>&#128161; Багатоплатформовий месенджер, що також надає опціональні наскрізні зашифровані чати та відеодзвінки, VoIP, обмін файлами та деякі інші функції.

- **версія програми &#128230;**: :four: . :nine: . :seven:
- **розробник &#128422;**: Telegram Messenger LLp
- **офіційний сайт**: [посилання &#128279;](https://telegram.org)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9NZTWSQNTD0S?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://desktop.telegram.org)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://desktop.telegram.org)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.telegram.desktop`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install telegram-desktop`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install telegram-desktop --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

