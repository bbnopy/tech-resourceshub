PROFONT FOR WINDOWS

This is v2 of profont.fon.
Some text editors (VIM, MultiEdit) wouldn't use v1 because it wasn't
recognized as a monospaced font.
Thanks to Zhichao Hong for fixing that problem.

To get the full original Distribution, other ProFont builds
and more information
go to <http://tobiasjung.name/profont/>


DISCLAIMER
See LICENSE file


Tobias Jung
January 2014
profont@tobiasjung.name
