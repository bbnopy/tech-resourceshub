# ULTIMAKER CURA



## Розділ &#127991; Інструменти &#129520;

>&#128161; Це безкоштовне, просте у використанні програмне забезпечення для 3D-друку.

- **версія програми &#128230;**: :five: . :four: . :zero:
- **розробник &#128422;**: Ultimaker
- **офіційний сайт**: [посилання &#128279;](https://ultimaker.com/software/ultimaker-cura/)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://ultimaker.com/software/ultimaker-cura/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://ultimaker.com/software/ultimaker-cura/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.ultimaker.cura`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install cura-slicer`
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install cura-slicer --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

