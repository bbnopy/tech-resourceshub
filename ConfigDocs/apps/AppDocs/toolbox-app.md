# TOOLBOX APP



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Керуйте своїми IDE у простий спосіб.

- **версія програми &#128230;**: :two: . :zero: . :four: . :one: :seven: :two: :one: :two:
- **розробник &#128422;**: JetBrains
- **офіційний сайт**: [посилання &#128279;](https://www.jetbrains.com/)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.jetbrains.com/toolbox-app/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.jetbrains.com/toolbox-app/)
- розпакуйте файл `sudo tar -xvzf ~/Downloads/jetbrains-toolbox-x.xx.xxxx.tar.gz`
- Запустіть файл

## Додатково

- IntelliJ IDEA Community Edition
- RustRover
- IntelliJ IDEA Ultimate
- Fleet
- GoLand
- PyCharm Professional
- DataSpell
- WebStorm
- Aqua
- CLion
- PhpStorm
- DataGrip
- Rider
- dotTrace
- Space Desktop
- RubyMine
- MPS
- Android Studio
- PyCharm Community
- Gateway

