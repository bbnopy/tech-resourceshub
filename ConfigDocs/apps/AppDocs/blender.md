# BLENDER



## Розділ &#127991; Графіка &#128443;

>&#128161; Пограмний &#128230; пакет для створення тривимірної комп'ютерної графіки, що включає засоби моделювання, анімації, рендерінгу, після-обробки відео.

- **версія програми &#128230;**: :three: . :six: . :four:
- **розробник &#128422;**: Blender Foundation
- **офіційний сайт**: [посилання &#128279;](https://www.blender.org)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/blender/9PP3C07GTVRH)
- &#128189; з **Steam store** за [посиланням &#128279;](https://store.steampowered.com/app/365670/Blender/)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.blender.org/download/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.blender.org/download/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.blender.Blender`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install blender --classic`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install blender --edge --classic`
  - **версія програми &#128230;** :four: . :zero: beta команда для терміналу: `sudo snap install blender --channel=4.0/beta --classic`
  - **версія програми &#128230;** :three: . :six: lts stable команда для терміналу: `sudo snap install blender --channel=3.6lts/stable --classic`
  - **версія програми &#128230;** :three: . :five: stable команда для терміналу: `sudo snap install blender --channel=3.5/stable --classic`
  - **версія програми &#128230;** :three: . :four: stable команда для терміналу: `sudo snap install blender --channel=3.4/stable --classic`
  - **версія програми &#128230;** :three: . :three: lts stable команда для терміналу: `sudo snap install blender --channel=3.3lts/stable --classic`
  - **версія програми &#128230;** :three: . :two: stable команда для терміналу: `sudo snap install blender --channel=3.2/stable --classic`
  - **версія програми &#128230;** :three: . :one: stable команда для терміналу: `sudo snap install blender --channel=3.1/stable --classic`
  - **версія програми &#128230;** :three: . :zero: stable команда для терміналу: `sudo snap install blender --channel=3.0/stable --classic`
  - **версія програми &#128230;** :two: . :nine: :three: lts stable команда для терміналу: `sudo snap install blender --channel=2.93lts/stable --classic`
  - **версія програми &#128230;** :two: . :nine: :two: stable команда для терміналу: `sudo snap install blender --channel=2.92/stable --classic`
  - **версія програми &#128230;** :two: . :nine: :one: stable команда для терміналу: `sudo snap install blender --channel=2.91/stable --classic`
  - **версія програми &#128230;** :two: . :nine: :zero: stable команда для терміналу: `sudo snap install blender --channel=2.90/stable --classic`
  - **версія програми &#128230;** :two: . :eight: :three: lts stable команда для терміналу: `sudo snap install blender --channel=2.83lts/stable --classic`
  - **версія програми &#128230;** :two: . :eight: :two: stable команда для терміналу: `sudo snap install blender --channel=2.82/stable --classic`
  - **версія програми &#128230;** :two: . :eight: :one: stable команда для терміналу: `sudo snap install blender --channel=2.81/stable --classic`
  - **версія програми &#128230;** :two: . :eight: :zero: stable команда для терміналу: `sudo snap install blender --channel=2.80/stable --classic`
  - **версія програми &#128230;** :two: . :nine: stable команда для терміналу: `sudo snap install blender --channel=2.9/stable --classic`
  - **версія програми &#128230;** :two: . :seven: :nine: stable команда для терміналу: `sudo snap install blender --channel=2.79/stable --classic`
  - **версія програми &#128230;** :two: . :eight: stable команда для терміналу: `sudo snap install blender --channel=2.8/stable --classic`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

