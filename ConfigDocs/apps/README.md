# ПРОГРАМИ &#128230;



>&#128161; Інформація стосовно налаштування, встановлення і конфігурація кросплотформені &#128230;.

## Розділ &#127991; графіка &#128443;

- Adobe Express [посилання &#128279;](AppDocs/adobe-express.md)
- Blender [посилання &#128279;](AppDocs/blender.md)
- Darktable [посилання &#128279;](AppDocs/darktable.md)
- GIMP [посилання &#128279;](AppDocs/gimp.md)
- Inkscape [посилання &#128279;](AppDocs/inkscape.md)
- Krita [посилання &#128279;](AppDocs/krita.md)
- Material Maker [посилання &#128279;](AppDocs/material-maker.md)
- Natron [посилання &#128279;](AppDocs/natron.md)
- OpenToonz [посилання &#128279;](AppDocs/opentoonz.md)
- Photopea [посилання &#128279;](AppDocs/photopea.md)
- RawTherapee [посилання &#128279;](AppDocs/rawtherapee.md)
- Scribus [посилання &#128279;](AppDocs/scribus.md)
- SweetHome3D [посилання &#128279;](AppDocs/sweethome.md)
- Synfig Studio [посилання &#128279;](AppDocs/synfig-studio.md)
- xLights [посилання &#128279;](AppDocs/xlights.md)

## Розділ &#127991; звук та відео &#128250;

- Audacity [посилання &#128279;](AppDocs/audacity.md)
- Elisa [посилання &#128279;](AppDocs/elisa.md)
- Handbrake [посилання &#128279;](AppDocs/handbrake.md)
- Jellyfin [посилання &#128279;](AppDocs/jellyfin.md)
- Jitsi [посилання &#128279;](AppDocs/jitsi.md)
- Kodi [посилання &#128279;](AppDocs/kodi.md)
- Navidrome [посилання &#128279;](AppDocs/navidrome.md)
- Shotcut [посилання &#128279;](AppDocs/shotcut.md)
- Spotify [посилання &#128279;](AppDocs/spotify.md)
- VLC [посилання &#128279;](AppDocs/vlc.md)

## Розділ &#127991; ігри &#127918;

- Itch setup [посилання &#128279;](AppDocs/itch.md)
- Minecraft Installer [посилання &#128279;](AppDocs/minecraft.md)
- Steam [посилання &#128279;](AppDocs/steam.md)
- Veloren [посилання &#128279;](AppDocs/veloren.md)

## Розділ &#127991; інструменти &#129520;

- 7-Zip [посилання &#128279;](AppDocs/7-zip.md)
- Alacritty [посилання &#128279;](AppDocs/alacritty.md)
- Bitwarden [посилання &#128279;](AppDocs/bitwarden.md)
- File Converter [посилання &#128279;](AppDocs/file-converter.md)
- Gparted [посилання &#128279;](AppDocs/gparted.md)
- Hyper [посилання &#128279;](AppDocs/hyper.md)
- KDE Connect [посилання &#128279;](AppDocs/kdeconnect.md)
- KeePassXC [посилання &#128279;](AppDocs/keepassxc.md)
- Nextcloud desktop [посилання &#128279;](AppDocs/nextcloud.md)
- Putty [посилання &#128279;](AppDocs/putty.md)
- Slic3r [посилання &#128279;](AppDocs/slic3r.md)
- Termius [посилання &#128279;](AppDocs/termius.md)
- Ultimate Cura [посилання &#128279;](AppDocs/cura.md)
- VeraCrypt [посилання &#128279;](AppDocs/veracrypt.md)
- WezTerm [посилання &#128279;](AppDocs/wezterm.md)
- WinCDEmu [посилання &#128279;](AppDocs/wincdemu.md)

## Розділ &#127991; інструменти розробника &#128296;

- BlueGriffon [посилання &#128279;](AppDocs/bluegriffon.md)
- Brackets [посилання &#128279;](AppDocs/brackets.md)
- BRL-CAD [посилання &#128279;](AppDocs/brl-cad.md)
- Docker [посилання &#128279;](AppDocs/docker.md)
- Emacs [посилання &#128279;](AppDocs/emacs.md)
- Git [посилання &#128279;](AppDocs/git.md)
- GitKraken [посилання &#128279;](AppDocs/gitkraken.md)
- Godot Engine [посилання &#128279;](AppDocs/godot-engine.md)
- Heroku [посилання &#128279;](AppDocs/heroku.md)
- Meld [посилання &#128279;](AppDocs/meld.md)
- Microsoft VSCode [посилання &#128279;](AppDocs/vscode.md)
- MySQL Workbench [посилання &#128279;](AppDocs/mysql-workbench.md)
- Node.js [посилання &#128279;](AppDocs/nodedotjs.md)
- NVIM [посилання &#128279;](AppDocs/nvim.md)
- Poedit [посилання &#128279;](AppDocs/poedit.md)
- PostgreSQL [посилання &#128279;](AppDocs/postgresql.md)
- Python [посилання &#128279;](AppDocs/python.md)
- Toolbox App [посилання &#128279;](AppDocs/toolbox-app.md)
- VirtualBox [посилання &#128279;](AppDocs/virtualbox.md)
- VMware Workstation Player [посилання &#128279;](AppDocs/vmware.md)

## Розділ &#127991; інтернет &#127758;

- AnyDesk [посилання &#128279;](AppDocs/anydesk.md)
- Appflowy [посилання &#128279;](AppDocs/appflowy.md)
- Brave Browser [посилання &#128279;](AppDocs/brave.md)
- DeepL [посилання &#128279;](AppDocs/deepl.md)
- Discord [посилання &#128279;](AppDocs/discord.md)
- Focalboard [посилання &#128279;](AppDocs/focalboard.md)
- Konversation [посилання &#128279;](AppDocs/konversation.md)
- Mattermost [посилання &#128279;](AppDocs/mattermost.md)
- Microsoft Edge Browser [посилання &#128279;](AppDocs/edge.md)
- Mozilla Firefox [посилання &#128279;](AppDocs/firefox.md)
- qBittorrent [посилання &#128279;](AppDocs/qbittorrent.md)
- rustdesk [посилання &#128279;](AppDocs/rustdesk.md)
- Signal desktop [посилання &#128279;](AppDocs/signal.md)
- Telegram desktop [посилання &#128279;](AppDocs/telegram.md)
- Threema [посилання &#128279;](AppDocs/threema.md)
- Thunderbird [посилання &#128279;](AppDocs/thunderbird.md)
- Tor Browser [посилання &#128279;](AppDocs/tor.md)
- WhatsApp [посилання &#128279;](AppDocs/whatsapp.md)

## Розділ &#127991; офіс &#128209;

- Bookstack [посилання &#128279;](AppDocs/bookstack.md)
- Calibre [посилання &#128279;](AppDocs/calibre.md)
- Joplin [посилання &#128279;](AppDocs/joplin.md)
- Libre Office [посилання &#128279;](AppDocs/libre-office.md)
- Logseq [посилання &#128279;](AppDocs/logseq.md)
- Microsoft 365 [посилання &#128279;](AppDocs/microsoft-365.md)
- Microsoft Loop [посилання &#128279;](AppDocs/microsoft-loop.md)
- Okular [посилання &#128279;](AppDocs/okular.md)
- Trilium Notes [посилання &#128279;](AppDocs/trilium-notes.md)
- Zeal [посилання &#128279;](AppDocs/zeal.md)

## Розділ &#127991; система &#128736;

- Bleachbit [посилання &#128279;](AppDocs/bleachbit.md)

