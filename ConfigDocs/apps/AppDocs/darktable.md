# DARKTABLE



## Розділ &#127991; Графіка &#128443;

>&#128161; Програма для редагування фотографій, яка виступає в ролі вільної альтернативи таким продуктам, як Adobe Lightroom і Apple Aperture.

- **версія програми &#128230;**: :four: . :four: . :two:
- **розробник &#128422;**: Johannes Hanika
- **офіційний сайт**: [посилання &#128279;](https://www.darktable.org/)

## Операційні Системи

### Windows &#128421;

- &#128229; з сайту за [посиланням &#128279;](https://www.darktable.org/install/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.darktable.org/install/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.darktable.Darktable`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install darktable`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install darktable --candidate`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install darktable --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

