# PUTTY



## Розділ &#127991; Інструменти &#129520;

>&#128161; Вільно розповсюджуваний клієнт для протоколів SSH, Telnet, rlogin і чистого TCP.

- **версія програми &#128230;**: :zero: . :seven: :eight:
- **розробник &#128422;**: Simon Tatham
- **офіційний сайт**: [посилання &#128279;](https://putty.org)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/putty/XPFNZKSKLBP7RJ)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; з офіційного сайту за [посиланням &#128279;](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub uk.org.greenend.chiark.sgtatham.putty`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

