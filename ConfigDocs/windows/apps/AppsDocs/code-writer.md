# CODE WRITER



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Безкоштовний розширений редактор тексту та коду для Windows &#128421;.

- **версія програми &#128230;**: :four: . :two:
- **розробник &#128422;**: Actipro Software LLC
- **офіційний сайт**: [посилання &#128279;](https://www.actiprosoftware.com/products/apps/codewriter)
    
## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/code-writer/9WZDNCRFHZDT)
