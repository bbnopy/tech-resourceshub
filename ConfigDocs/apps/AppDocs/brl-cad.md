# BRL-CAD



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; САПР для 3D-проєктування.

- **версія програми &#128230;**: :seven: . :three: :eight: . :two:
- **розробник &#128422;**: 	Army Research Laboratory
- **офіційний сайт**: [посилання &#128279;](https://brlcad.org/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9P6FK5SXDJLQ?hl=uk-ua&gl=UA)
- &#128229; з :octocat: за [посиланням &#128279;](https://github.com/BRL-CAD/brlcad/releases/tag/rel-7-38-0)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з :octocat: за [посиланням &#128279;](https://github.com/BRL-CAD/brlcad/releases/tag/rel-7-38-0)

