# SWEET HOME 3D



## Розділ &#127991; Графіка &#128443;

>&#128161; Це безкоштовна програма &#128230; для дизайну інтер’єру, який допоможе вам намалювати план вашого будинку, розставити на ньому меблі та переглянути результати в 3D.

- **версія програми &#128230;**: :seven: . :two:
- **розробник &#128422;**: eTEks
- **офіційний сайт**: [посилання &#128279;](http://www.sweethome3d.com)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/sweet-home-3d/9NBLGGH2SMTQ?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](http://www.sweethome3d.com/download.jsp)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](http://www.sweethome3d.com/download.jsp)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.sweethome3d.Sweethome3d`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install sweethome3d-homedesign`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install sweethome3d-homedesign --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install sweethome3d-homedesign --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install sweethome3d-homedesign --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

