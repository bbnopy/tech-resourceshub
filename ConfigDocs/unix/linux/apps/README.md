# ПРОГРАМИ &#128230;



>&#128161; Програми &#128230; які використовуються виключно у операційних системамах родини Linux &#128039;.

- Застосунки для GNOME [посилання &#128279;](https://apps.gnome.org/en/#development)
- Застосунки для KDE [посилання &#128279;](https://apps.kde.org/uk/)
- KDE Store [посилання &#128279;](https://store.kde.org/browse/)

## Розділ &#127991; графіка &#128443;

- Curtail [посилання &#128279;](AppDocs/curtail.md)
- Gwenview [посилання &#128279;](AppDocs/gwenview.md)
- KolourPaint [посилання &#128279;](AppDocs/kolourpaint.md)

## Розділ &#127991; звук та відео &#128250;

- Dragon Player [посилання &#128279;](AppDocs/dragon.md)
- Kamoso [посилання &#128279;](AppDocs/kamoso.md)
- Rhythmbox [посилання &#128279;](AppDocs/rhythmbox.md)

## Розділ &#127991; ігри &#127918;

- Kmahjongg [посилання &#128279;](AppDocs/kmahjongg.md)
- Kmines [посилання &#128279;](AppDocs/kmines.md)
- Kpatience [посилання &#128279;](AppDocs/kpatience.md)

## Розділ &#127991; інструменти &#129520;

- BASH [посилання &#128279;](AppDocs/bash.md)
- DNFdragora [посилання &#128279;](AppDocs/dnfdragora.md)
- Gnome Commander [посилання &#128279;](AppDocs/gcommander.md)
- Gnome Tweaks [посилання &#128279;](AppDocs/tweaks.md)
- htop [посилання &#128279;](AppDocs/htop.md)
- HydraPaper [посилання &#128279;](AppDocs/hydrapaper.md)
- Kgpg [посилання &#128279;](AppDocs/kgpg.md)
- Krusader [посилання &#128279;](AppDocs/krusader.md)
- KTeaTime [посилання &#128279;](AppDocs/kteateam.md)
- Midnight Commander [посилання &#128279;](AppDocs/mc.md)
- tmux [посилання &#128279;](AppDocs/tmux.md)
- ZSH [посилання &#128279;](AppDocs/zsh.md)

## Розділ &#127991; інтернет &#127758;

- Akregator [посилання &#128279;](AppDocs/akregator.md)
- Chromium [посилання &#128279;](AppDocs/chromium.md)
- Kmail [посилання &#128279;](AppDocs/kmail.md)
- Krdc [посилання &#128279;](AppDocs/krdc.md)
- Krfb [посилання &#128279;](AppDocs/krfb.md)
- Nyxt [посилання &#128279;](AppDocs/nyxt.md)
- Wike [посилання &#128279;](AppDocs/wike.md)

## Розділ &#127991; офіс &#128209;

- Cozy [посилання &#128279;](AppDocs/cozy.md)
- Komikku [посилання &#128279;](AppDocs/komikku.md)

