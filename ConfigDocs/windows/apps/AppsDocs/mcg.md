# MICROSOFT CASUAL GAMES



## Розділ &#127991; Ігри &#127918;

>&#128161; Студія казуальних ігор Microsoft зробила революцію в найпопулярніших жанрах казуальних ігор, запропонувавши свіжий погляд на класичні улюбленці, які радують шанувальників вже БІЛЬШЕ 30 років!

## Продукція

- [Microsoft Solitaire Collection](#-microsoft-solitare-collection)
- Mahjong by Microsoft
- Microsoft Jigsaw
- Microsoft Minesweeper
- Microsoft Sudoku
- Microsoft Ultimate Word Games
- Microsoft Treasure Hunt
- Jewel 2
- Wordament

### Microsoft Solitaire Collection

- **версія програми &#128230;**: :four: . :one: :seven: . :one: :zero: :zero: :two: :zero: . :zero:
- **розробник &#128422;**: Microsoft Casual Games
    
## Операційні Системи

### Windows &#128421;

- &#128189; з **Xbox** за [посиланням &#128279;](https://www.xbox.com/uk-ua/games/store/microsoft-solitaire-collection/9wzdncrfhwd2?rtc=1)

