# EPIC GAMES STORE



## Розділ &#127991; Ігри &#127918;

>&#128161; Онлайн-сервіс цифрового поширення комп'ютерних ігор, розроблений і керований американською компанією Epic Games.

- **версія програми &#128230;**: :one: :five: . :one: :four: . :zero: - :two: :eight: :zero: :eight: :six: :one: :five: :one:
- **розробник &#128422;**: Epic Games
- **офіційний сайт**: [посилання &#128279;](https://store.epicgames.com/en-US/)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/epic-games-store/XP99VR1BPSBQJ2)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://store.epicgames.com/en-US/)
- &#128229; з Хмарного сховища &#9729;
