# FOTOSKETCHER


> :information_source: **Інформація:** Це програма для :desktop_computer: Windows, яка автоматично перетворює ваші цифрові фотографії на мистецтво всього за кілька кліків мишкою.

***
:open_file_folder: **Розділ:** :framed_picture: *Графіка*
***

## Про FotoSketcher

| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :three: . :nine: :five: |
| :computer: **розробник** | David Thoiron |
| **сайт** | [:link: посилання](https://fotosketcher.com/) |

## Встановлення

### :desktop_computer: Windows

- :inbox_tray: Завантажити з сайту за [:link: посиланням](https://fotosketcher.com/download-fotosketcher/)
- :inbox_tray: Завантажити з :cloud: Хмарного сховища

