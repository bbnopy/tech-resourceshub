# HYPER



## Розділ &#127991; Інструменти &#129520;

>&#128161; HTML/JS/CSS термінал.

- **версія програми &#128230;**: :three: . :four: . :one:
- **розробник &#128422;**: vercel
- **офіційний сайт**: [посилання &#128279;](https://hyper.is/)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://hyper.is/#installation)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://hyper.is/#installation)

## РОЗШИРЕННЯ для HYPER

- hypercwd `hyper i hypercwd`
- hyper-search `hyper i hyper-search`
- hyper-pane `hyper i hyper-pane`
- hyperpower `hyper i hyperpower`
- hyper-highlight-active-pane `hyper i hyper-highlight-active-pane`

### Теми для Hyper

- hyper-aura-theme `hyper i hyper-aura-theme`
- hyper-rose-pine `hyper i hyper-rose-pine`
- hyper-hypest `hyper i hyper-hypest`
- hyper-cyberpunk `hyper i hyper-cyberpunk`
- hyper-npm-theme `hyper i hyper-npm-theme`
- hyper-afterglow `hyper i hyper-afterglow`
- xi-hyper `hyper i xi-hyper`, `hyper i hyper-named-css-colors`
- hyperocean `hyper i hyperocean`
- hyper-solarized `hyper i hyper-solarized`
- hyper-subliminal-theme `hyper i hyper-subliminal-theme`
- hyper-night-owl `hyper i hyper-night-owl`
- hyper-pokemon `hyper i hyper-pokemon`
- hyper-oceanic-next `hyper i hyper-oceanic-next`
- hyper-relaxed `hyper i hyper-relaxed`
- an-old-hype `hyper i an-old-hype`
- hyper-whimsy `hyper i hyper-whimsy`
- hyper-electron-highlughter `hyper i hyper-electron-highlighter`
- hyper-website-theme `hyper i hyper-website-theme`
- hyper-firefox-devtools `hyper i hyper-firefox-devtools`
- hyper-mono-christmas `hyper i hyper-mono-christmas`
- shafes-of-purple-hyper `hyper i shades-of-purple-hyper`
- hyper-flat `hyper i hyper-flat`
- hyper-one-dark `hyper i hyper-one-dark`
- hyper-one-light `hyper i hyper-one-light`
- hyper-solarized-light `hyper i hyper-solarized-light`
- hyper-solarized-dark `hyper i hyper-solarized-dark`
- hyper-chesterish `hyper i hyper-chesterish`
- verminal `hyper i verminal`
- hyper-material-theme `hyper i hyper-material-theme`
- hyper-native `hyper i hyper-native`
- hyper-snazzy `hyper i hyper-snazzy`
- hyper-dracula `hyper i hyper-dracula`
- hyper-corubo `hyper i hyper-corubo`

