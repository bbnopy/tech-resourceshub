# GODOT ENGINE



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Це безкоштовний, універсальний, кросплатформний ігровий движок, який полегшує вам створення 2D і 3D ігор.

- **версія програми &#128230;**:
  - LTS &#128230;: :three: . :five: . :three:
  - Latest &#128230;: :four: . :one: . :one:
- **розробник &#128422;**: Співавторство розробників
- **офіційний сайт**: [посилання &#128279;](https://godotengine.org)

## Операційні Cистеми

### Windows &#128421;

- &#128189; за допомогою **Steam store** [посилання &#128279;](https://store.steampowered.com/app/404790/Godot_Engine/)
- &#128189; за допомогою **Epic Games store** [посилання &#128279;](https://store.epicgames.com/en-US/p/godot-engine)
- &#128229; з офіційного сайту LTS версію звичайну або з підтримкою dotNET за [посиланням &#128279;](https://godotengine.org/download/3.x/windows/)
- &#128229; з офіційного сайту останню версію, звичайну або з підтримкою dotNET за [посиланням &#128279;](https://godotengine.org/download/windows/)
- &#128229; стабільну версію або з підтримкою dotNet з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Steam store** [посилання &#128279;](https://store.steampowered.com/app/404790/Godot_Engine/)
- &#128229; з офіційного сайту LTS версію звичайну або з підтримкою dotNET за [посиланням &#128279;](https://godotengine.org/download/3.x/linux/)
- &#128229; з офіційного сайту останню версію, звичайну або з підтримкою dotNET за [посиланням &#128279;](https://godotengine.org/download/linux/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.godotengine.Godot`
  - Godot (C#/.NET) вводимо команду у терміналі: `flatpak install flathub org.godotengine.GodotSharp`
  - Godot 3 вводимо команду у терміналі: `flatpak install flathub org.godotengine.Godot3`

#### fedora WORKSTATIONd

- &#128189; з **Центру програмного &#128230; забезпечення**

