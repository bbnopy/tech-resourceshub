# BASH (THE BOURNE-AGAIN SHELL)



## Розділ &#127991; інструменти &#129520;

>&#128161; Вдосконалена й модернізована варіація командної оболонки Bourne shell.

- **версія програми &#128230;**: :five: . :two: . :one: :five: ( :one: )
- **розробник &#128422;**: GNU project
- **офіційний сайт**: [посилання &#128279;](https://tiswww.case.edu/php/chet/bash/bashtop.html)

## Операційні Cистеми

### Linux &#128039;

>&#128161; Попередньо встановлюється в операційній системі.

## Конфігурація

### OH MY BASH

>&#128161; Платформа з відкритим кодом, керована спільнотою, для керування конфігурацією bash.

- **офіційний сайт**: :octocat: [посилання &#128279;](https://github.com/ohmybash/oh-my-bash)

#### Як встановити

##### via curl

відкриваємо термінал і вводимо команду:

```bash
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
```

##### via wget

відкриваємо термінал і вводимо команду:

```bash
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
```

#### Доступні плагіни

- **ansible** - це Python-залежне програмне забезпечення для керування конфігурацією, де і на керуючому вузлі, і на цільовій машині повинні бути встановлені Python та залежні від нього пакети. Ansible не потребує єдиного керуючого комп'ютера, з якого починається оркестрування.
- **aws** - це програмні доповнення, які розширюють функціональність інтерфейсу командного рядка AWS (CLI) та інших інструментів AWS. Вони можуть допомогти вам автоматизувати завдання, керувати ресурсами та ефективніше взаємодіяти зі службами AWS.
- **bash-preexec** - це скрипт оболонки Bash, який дозволяє виконати команду до виконання запиту. Цей плагін не включено до інсталяції ohmybash за замовчуванням. Однак ви можете встановити його вручну, дотримуючись інструкцій, наданих в репозиторії плагіна на GitHub.
- **bashmarks** - плагін дозволяє створювати і використовувати закладки для каталогів у ваших файлових системах. Ви можете створити нову закладку за допомогою команди `bm -a` і перейти до місця розташування закладки за допомогою команди `bm -g`.
- **battery** - Плагін для акумулятора відображає поточний рівень заряду акумулятора у відсотках та стан заряджання у підказці терміналу.
- **brew** - Плагін дозволяє керувати пакетами brew безпосередньо з командного рядка
- **bu** - Плагін надає простий спосіб створення резервних копій конфігураційних файлів bash
- **chezmoi** - менеджер dot-файлів, який допоможе вам безпечно керувати вашими dot-файлами на декількох машинах. Він підтримує плагіни, подібно до git
- **fasd** - прискорювач продуктивності командного рядка, який пропонує швидкий доступ до файлів і каталогів для оболонок POSIX.
- **gcloud** - простий плагін, який відображає активну конфігурацію gcloud, проект за замовчуванням і поточний проект квот для облікових даних програми за замовчуванням (ADC), якщо такі є
- **git** - плагін надає кілька корисних псевдонімів і функцій для роботи з Git-репозиторіями. Список псевдонимів [посилання &#128279;](docs/git-alias.md)
- **goenv** - інструмент, який дозволяє керувати кількома версіями Go у вашій системі.
- **golang** - плагін, який додає деякі псевдоніми для поширених команд Golang
- **kubectl** - інструмент командного рядка для взаємодії з кластерами Kubernetes.
- **npm** - плагін надає набір псевдонімів для команд npm, які допоможуть вам заощадити час і натискання клавіш при роботі з проектами Node.js.
- **nvm** - фреймворк, який автоматично завантажує nvm (Node Version Manager) при відкритті нового вікна терміналу.
- **progress** - плагін, який забезпечує кращий індикатор прогресу для терміналу. Він замінює стандартний індикатор прогресу на більш інформативний і візуально привабливий.
- **pyenv** - інструмент, який допомагає керувати кількома версіями Python в одній системі. Він дозволяє легко встановлювати, видаляти та перемикатися між різними версіями Python.
- **sdkman** - інструмент, який дозволяє встановлювати та керувати кількома наборами для розробки програмного забезпечення (SDK) на системах на базі Unix
- **sudo** - дозволяє вам легко додавати sudo до поточної або попередньої команди, двічі натиснувши <kbd>ESC</kbd>
- **tmux-autoattach** - плагін, який автоматично приєднує сеанс tmux до вашого сеансу оболонки. Плагін надає два варіанти поведінки: від'єднати і завершити сеанс.
- **vagrant** - набір псевдонімів і функцій, які полегшують роботу з оточенням Vagrant.
- **xterm** - це скрипт, який встановлює заголовок xterm для поточного каталогу і поточної запущеної команди. Він автоматично встановлює ваш заголовок xterm з інформацією про хост і місцезнаходження 
- **zoxide** - це "розумніша" команда cd, яка запам'ятовує каталоги, які ви використовуєте найчастіше, щоб ви могли "переходити" до них лише кількома натисканнями клавіш. Вона працює у всіх основних оболонках і підтримує нечіткий пошук.

##### Як встановити плагін

>&#128161; Після того, як ви визначитеся з плагіном (або декількома), який хочете використовувати з Oh My Bash, вам потрібно буде увімкнути їх у файлі .bashrc. Файл bashrc знаходиться у вашому каталозі $HOME. Відкрийте його за допомогою вашого улюбленого текстового редактора, і ви побачите місце для переліку всіх плагінів, які ви хочете завантажити.

```bashrc
plugins=(
  git
  bashmarks
)
```

Щоб завантажити зміни, які було зроблено у файлі **bashrc**, відкриваємо тремінал і вводимо команду: `source ~/.bashrc`

#### Теми

- 90210

#### Список псевдонімів і функцій для роботи з Git-репозиторіями

| Опис | Псевдонім |
|-----:|:---------:|
| команда git | `g` |
| команда git add | `ga` |
| команда git add --all | `gaa` |
| команда git add --patch | `gapa` |
| команда git add --update | `gau` |
| команда git add --verbose | `gav` |
| команда git add -A; команда git rm $(git ls-files --deleted) 2> /dev/null; команда git commit --no-verify --no-gpg-sign --message "--wip-- [skip ci]" | `gwip` |
| команда git am | `gam` |
| команда git am --abort | `gama` |
| команда git am --continue | `gamc` |
| команда git am --show-current-patch | `gamscp` |
| команда git am --skip | `gams` |
| команда git apply | `gap` |
| команда git apply --3way | `gapt` |
| команда git blame -b -w | `gbl` |
| команда git branch | `gb` |
| команда git branch --delete --force | `gbD` |
| команда git branch -a | `gba` |
| команда git branch -d | `gbd` |
| команда git branch --no-color --merged | команда grep -vE "^([+\*]|\s*($(git_main_branch)|$(git_develop_branch))\s*$)" | команда xargs git branch --delete 2>/dev/null | `gbda` |
| LANG=C команда git branch -vv | grep ": gone\]" | `gbg` |
| LANG=C команда git branch --no-color -vv | grep ": gone\]" | awk '"'"'{print $1}'"'"' | команда xargs git branch -D | `gbgD` |
| LANG=C команда git branch --no-color -vv | grep ": gone\]" | awk '"'"'{print $1}'"'"' | команда xargs git branch -d | `gbgd` |
| команда git branch --move | `gbm` |
| команда git branch --no-merged | `gbnm` |
| команда git branch --remote | `gbr` |
| команда git branch --show-current | `gbsc` |
| команда git branch --set-upstream-to="origin/$(git_current_branch)" | `ggsup` |
| команда git bisect | `gbs` |
| команда git bisect bad | `gbsb` |
| команда git bisect good | `gbsg` |
| команда git bisect new | `gbsn` |
| команда git bisect old | `gbso` |
| команда git bisect reset | `gbsr` |
| команда git bisect start | `gbss` |
| команда git checkout -b | `gcb` |
| команда git checkout "$(git_develop_branch)" | `gcd` |
| команда git checkout "$(git_main_branch)" | `gcm` |
| команда git checkout | `gco` |
| команда git checkout --recurse-submodules | `gcor` |
| команда git cherry-pick | `gcp` |
| команда git cherry-pick --abort | `gcpa` |
| команда git cherry-pick --continue | `gcpc` |
| команда git cherry-pick -s | `gcps` |
| команда git clone --recursive | `gcl` |
| команда git clean -fd | `gclean` |
| команда git commit --verbose --amend | `gc!` |
| команда git commit --verbose | `gc` |
| команда git commit --verbose --all --amend | `gca!` |
| команда git commit --verbose --all | `gca` |
| команда git commit --all --message | `gcam` |
| команда git commit --verbose --all --no-edit --amend | `gcan!` |
| команда git commit --verbose --all --signoff --no-edit --amend | `gcans!` |
| команда git commit --all --signoff | `gcas` |
| команда git commit --all --signoff --message | `gcasm` |
| команда git commit --message | `gcmsg` |
| команда git commit --verbose --no-edit --amend | `gcn!` |
| команда git commit --gpg-sign | `gcs` |
| команда git commit --signoff --message | `gcsm` |
| команда git commit --gpg-sign --signoff | `gcss` |
| команда git commit --gpg-sign --signoff --message | `gcssm` |
| команда git config --list | `gcf` |
| команда git describe --tags `git rev-list --tags --max-count=1` | `gdct` |
| команда git diff | `gd` |
| команда git diff --cached | `gdca` |
| команда git diff --cached --word-diff | `gdcw` |
| команда git diff --staged | `gds` |
| команда git diff --word-diff | `gdw` |
| команда git diff @{upstream} | `gdup` |
| команда git diff-tree --no-commit-id --name-only -r | `gdt` |
| команда git difftool -d | `gdtool` |
| команда git fetch | `gf` |
| команда git fetch --all --prune | `gfa` |
| команда git fetch origin | `gfo` |
| команда git gui citool | `gg` |
| команда git gui citool --amend | `gga` |
| команда git help | `ghh` |
| команда git log --stat | `glg` |
| команда git log --graph | `glgg` |
| команда git log --graph --decorate --all | `glgga` |
| команда git log --graph --max-count=10 | `glgm` |
| команда git log --stat -p | `glgp` |
| команда git log --oneline --decorate | `glo` |
| команда git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset" | `glod` |
| команда git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset" --date=short | `glods` |
| команда git log --oneline --decorate --graph | `glog` |
| команда git log --oneline --decorate --graph --all | `gloga` |
| команда git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" | `glol` |
| команда git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" --all | `glola` |
| команда git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" --stat | `glols` |
| \_git_log_prettily | `glp` |
| команда git ls-files -v | grep "^[[:lower:]]" | `gignored` |
| команда git ls-files \| grep | `gfg` |
| команда git merge | `gm` |
| команда git merge --abort | `gma` |
| команда git merge "origin/$(git_main_branch)" | `gmom` |
| команда git merge --squash | `gms` |
| команда git merge "upstream/$(git_main_branch)" | `gmum` |
| команда git mergetool --no-prompt | `gmt` |
| команда git mergetool --no-prompt --tool=vimdiff | `gmtvim` |
| команда git mergetool --no-prompt'
alias gmtlvim='команда git mergetool --no-prompt --tool=vimdiff | `gmtl` |
| команда git pull origin "$(git_current_branch)" | `ggpull` |
| ggu | `ggpur` |
| команда git pull | `gl` |
| команда git pull upstream "$(git_current_branch)" | `gluc` |
| команда git pull upstream "$(git_main_branch)" | `glum` |
| команда git pull --rebase | `gpr` |
| команда git pull --rebase | `gup` |
| команда git pull --rebase --autostash | `gupa` |
| команда git pull --rebase --autostash --verbose | gupav |
| команда git pull --rebase origin "$(git_main_branch)" | `gupom` |
| команда git pull --rebase=interactive origin "$(git_main_branch)"'
alias gupv='команда git pull --rebase --verbose | `gupomi` |
| команда git push origin "$(git_current_branch)" | `ggpush` |
| команда git push | `gp` |
| команда git push --dry-run | `gpd` |
| команда git push --force | `gpf!` |
| команда git push --force-with-lease | `gpf` |
| команда git push origin --all && команда git push origin --tags | `gpoat` |
| команда git push origin --delete | `gpod` |
| команда git push --set-upstream origin "$(git_current_branch)" | `gpsup` |
| команда git push --set-upstream origin "$(git_current_branch)" --force-with-lease | `gpsupf` |
| команда git push upstream | `gpu` |
| команда git push --verbose | `gpv` |
| команда git rebase | `grb` |
| команда git rebase --abort | `grba` |
| команда git rebase --continue | `grbc` |
| команда git rebase --interactive | `grbi` |
| команда git rebase --onto | `grbo` |
| команда git rebase --skip | `grbs` |
| команда git rebase "$(git_develop_branch)" | `grbd` |
| команда git rebase "$(git_main_branch)" | `grbm` |
| команда git rebase "origin/$(git_main_branch)" | `grbom` |
| команда git remote | `gr` |
| команда git remote add | `gra` |
| команда git remote rename | `grmv` |
| команда git remote remove | `grrm` |
| команда git remote set-url | `grset` |
| команда git remote update | `grup` |
| команда git remote --verbose | `grv` |
| команда git reset --hard && команда git clean --force -dfx | `gpristine` |
| команда git reset | `grh` |
| команда git reset --hard | `grhh` |
| команда git reset --keep | `grhk` |
| команда git reset --soft | `grhs` |
| команда git reset "origin/$(git_current_branch)" --hard | `groh` |
| cd $(команда git rev-parse --show-toplevel || echo ".") | `grt` |
| команда git reset -- | `gru` |
| команда git restore | `grs` |
| команда git restore --source | `grss` |
| команда git restore --staged | `grst` |
| команда git rev-list --max-count=1 --format="%s" HEAD | grep -q "\--wip--" && команда git reset HEAD~1 | `gunwip` |
| команда git revert | `grev` |
| команда git rm | `grm` |
| команда git rm --cached | `grmc` |
| команда git shortlog --summary --numbered | `gcount` |
| команда git show | `gsh` |
| команда git show --pretty=short --show-signature | `gsps` |
| команда git stash save | `gsta` |
| команда git stash apply | `gstaa` |
| команда git stash --all | `gstall` |
| команда git stash clear | `gstc` |
| команда git stash drop | `gstd` |
| команда git stash list | `gstl` |
| команда git stash pop | `gstp` |
| команда git stash show | `gsts` |
| gsta --include-untracked | `gstu` |
| команда git status --short --branch | `gsb` |
| команда git status --short | `gss` |
| команда git status | `gst` |
| команда git submodule init | `gsi` |
| команда git submodule update | `gsu` |
| команда git svn dcommit && команда git push github "$(git_main_branch):svntrunk" | `git-svn-dcommit-push` |
| команда git svn dcommit | `gsd` |
| команда git svn rebase | `gsr` |
| команда git switch | `gsw` |
| команда git switch --create | `gswc` |
| команда git switch "$(git_develop_branch)" | `gswd` |
| команда git switch "$(git_main_branch)" | `gswm` |
| команда git tag --annotate | `gta` |
| команда git tag --sign | `gts` |
| команда git tag \| sort -V | `gtv` |
| команда git update-index --assume-unchanged | `gignore` |
| команда git update-index --no-assume-unchanged | `gunignore` |
| команда git whatchanged -p --abbrev-commit --pretty=medium | `gwch` |
| команда git worktree | `gwt` |
| команда git worktree add | `gwta` |
| команда git worktree list | `gwtls` |
| команда git worktree move | `gwtmv` |
| команда git worktree remove | `gwtrm` |
| \gitk --all --branches | `gk` |
| \gitk --all $(git log --walk-reflogs --pretty=%h) | `gke` |


