# ПРОГРАМИ &#128230;

>&#128161; Програми &#128230; які використовуються виключно операційними системами корпорації Microsoft.

## Розділ &#127991; : графіка &#128443;

- Bing Wallpaper [посилання &#128279;](AppDocs/bing-wallpaper.md)
- Caesium [посилання &#128279;](AppDocs/caesium.md)
- FotoSketcher [посилання &#128279;](AppDocs/fotosketcher.md)
- ImageGlass [посилання &#128279;](AppDocs/imageglass.md)
- League Display [посилання &#128279;](AppDocs/league-display.md)
- Quixel [посилання &#128279;](docs/quixel.md)
- Rainmeter [посилання &#128279;](AppDocs/rainmeter.md)
- ShareX [посилання &#128279;](AppDocs/sharex.md)

## Розділ &#127991; звук та відео &#128250;

- Mp3tag [посилання &#128279;](AppDocs/mp3tag.md)

## Розділ &#127991; ігри &#127918;

- Amazon Games [посилання &#128279;](AppDocs/amazon-games.md)
- Battle.net [посилання &#128279;](AppDocs/battledotnet.md)
- Blitz [посилання &#128279;](AppDocs/blitz.md)
- Bluestacks [посилання &#128279;](AppDocs/bluestacks.md)
- Electronics Arts [посилання &#128279;](AppDocs/electronic-arts.md)
- Epic Games Store [посилання &#128279;](AppDocs/epic.md)
- Good Old Games (GOG) [посилання &#128279;](AppDocs/gog.md)
- Guild Wars 2 [посилання &#128279;](AppDocs/gw2.md)
- Microsoft Casual Games [посилання &#128279;](AppDocs/mcg.md)
- Riot Games [посилання &#128279;](AppDocs/riot-games.md)
- Rokstar games [посилання &#128279;](AppDocs/rockstar-games.md)

## Розділ &#127991; інструменти &#129520;

- Bulk Rename Utility [посилання &#128279;](AppDocs/bulk.md)
- Command Prompt [посилання &#128279;](AppDocs/cmd.md)
- Cpu-Z [посилання &#128279;](AppDocs/cpu-z.md)
- CrystalDiskInfo Shizuku Edition [посилання &#128279;](AppDocs/crystaldiskinfo.md)
- CrystalDiskMark Shizuku Edition [посилання &#128279;](AppDocs/crystaldiskmark.md)
- dotSwitcher [посилання &#128279;](AppDocs/dotswitcher.md)
- MobaXterm [посилання &#128279;](AppDocs/mobaxterm.md)
- OneCommander [посилання &#128279;](AppDocs/onecommander.md)
- Open Hardware Monitor [посилання &#128279;](AppDocs/ohm.md)
- Process Explorer [посилання &#128279;](AppDdocs/pe.md)
- SteelSeries GG [посилання &#128279;](AppDocs/steelseriesgg.md)
- TeraCopy [посилання &#128279;](AppDocs/teracopy.md)
- Victoria [посилання &#128279;](AppDocs/victoria.md)
- Windows Terminal [посилання &#128279;](AppDocs/windows-terminal.md)
- WinSCP [посилання &#128279;](AppDocs/winscp.md)
- Zoc Terminal [посилання &#128279;](AppDocs/zoc-terminal.md)

### Розділ &#127991; інструменти розробника &#128296;

- Code Writer [посилання &#128279;](AppDocs/code-writer.md)
- HBuilderX [посилання &#128279;](AppDocs/hbuilderx.md)
- Sourcetree [посилання &#128279;](AppDocs/sourcetree.md)
- Visual Studio Community 2022 [посилання &#128279;](AppDocs/vscommunity.md)

### Розділ &#127991; інтернет &#127758;

- Google Chrome [посилання &#128279;](AppDocs/chrome.md)
- WebCompanion [посилання &#128279;](AppDocs/webcompanion.md)

### Розділ &#127991; офіс &#128209;

- Adobe Acrobat Reader [посилання &#128279;](AppDocs/acrobat-reader.md)
- Freda ebook reader [посилання &#128279;](AppDocs/freda.md)
