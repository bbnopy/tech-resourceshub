# WEZTERM



## Розділ &#127991; Інструменти &#129520;

>&#128161; це потужний кросплатформний емулятор терміналу та мультиплексор.

- **версія програми &#128230;**: :two: :zero: :two: :three: :zero: :seven: :one: :two: - :zero: :seven: :two: :six: :zero: :one:
- **розробник &#128422;**: wez
- **офіційний сайт**: [посилання &#128279;](https://wezfurlong.org/wezterm/)

## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://wezfurlong.org/wezterm/install/windows.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://wezfurlong.org/wezterm/install/linux.html)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.wezfurlong.wezterm`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

