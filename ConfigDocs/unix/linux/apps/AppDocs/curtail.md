# CURTAIL



## Розділ &#127991; Графіка &#128443;

>&#128161; Оптимізуйте свої зображення за допомогою Curtail, корисного компресора зображень, який підтримує типи файлів PNG, JPEG, WebP і SVG.

- **версія програми &#128230;**: :one: . :seven: . :zero:
- **розробник &#128422;**: Hugo Posnic
- **офіційний сайт**: [посилання &#128279;](https://apps.gnome.org/uk/Curtail/)

## Операційні Cистеми

### Linux &#128039;

- &#128229; з :octocat: [посилання &#128279;](https://github.com/Huluti/Curtail/releases/tag/1.7.0)
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install curtail`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install curtail --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
