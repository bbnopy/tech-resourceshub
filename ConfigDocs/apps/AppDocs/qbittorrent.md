# QBITTORRENT



## Розділ &#127991; Інтернет &#127758;

>&#128161; Вільний торент-клієнт, написаний з використанням тулкіта Qt та вільної бібліотеки libtorrent.

- **версія програми &#128230;**: :four: . :five: . :five:
- **розробник &#128422;**: Christophe Dumez
- **офіційний сайт**: [посилання &#128279;](https://www.qbittorrent.org)

## Операційні Системи

#### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.qbittorrent.org/download)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- інформація по встановленню &#128189; за [посиланням &#128279;](https://www.qbittorrent.org/download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.qbittorrent.qBittorrent`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latesst stable команда для терміналу: `sudo snap install qbittorrent-arnatious`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install qbittorrent-arnatious --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install qbittorrent-arnatious --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install qbittorrent-arnatious --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

