# CRYSTALDISKINFO



## Розділ &#127991; Інструменти &#129520;

>&#128161; Утиліта для роботи з HDD/SSD, яка підтримує частину USB, Intel RAID і NVMe.

- **версія програми &#128230;**: :eight: . :one: :seven: . :one: :four:
- **розробник &#128422;**: Crystal Dew World
- **офіційний сайт**: [посилання &#128279;](https://crystalmark.info/en/)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/crystaldiskinfo/XP8K4RGX25G3GM?hl=uk-ua&gl=UA)
- &#128189; з **Microsoft Store** Shizuku Edition за [посиланням &#128279;](https://apps.microsoft.com/detail/crystaldiskinfo-shizuku-edition/XPFP35NT4K8RWK?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://crystalmark.info/en/software/crystaldiskinfo/)
- &#128229; з Хмарного сховища &#9729;
