# STEAM



## Розділ &#127991; Ігри &#127918;

>&#128161; Сервіс компанії Valve, відомого розробника відеоігор, який надає послуги цифрової дистрибуції, багатокористувацьких ігор і спілкування гравців.

- **версія програми &#128230;**:
  - Windows &#128421;: :one: :six: :eight: :seven: :three: :eight: :six: :nine: :zero: :seven:
  - Linux &#128039;: :one: . :zero: . :zero: . :seven: :eight:
- **розробник &#128422;**: Valve Corporation
- **офіційний сайт**: [посилання &#128279;](https://store.steampowered.com)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://store.steampowered.com/about/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.valvesoftware.Steam`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install steam`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install steam --candidate`
  - **версія програми &#128230;** latest beta команда для терміналу: `sudo snap install steam --beta`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install steam --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

