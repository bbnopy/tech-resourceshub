# BRACKETS



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Текстовий редактор від компанії Adobe, призначений для редагування JavaScript, HTML і CSS.

- **версія програми &#128230;**: :two: . :two: . :one:
- **розробник &#128422;**: Brackets Community, Adobe Systems Incorporated
- **офіційний сайт**: [посилання &#128279;](https://brackets.io)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з :octocat: за [посиланням &#128279;](https://github.com/brackets-cont/brackets/releases)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; з офіційного сайту за [посиланням &#128279;](https://github.com/brackets-cont/brackets/releases)

