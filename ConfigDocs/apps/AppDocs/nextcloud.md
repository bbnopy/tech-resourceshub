# NEXTCLOUD DESKTOP



## Розділ &#127991; Інструменти &#129520;

>&#128161; Це набір клієнт-серверного програмного забезпечення для створення та використання служб файлообміну.

- **версія програми &#128230;**: :three: . :one: :zero: . :zero:
- **розробник &#128422;**: Nextcloud GmbH, Community
- **офіційний сайт**: [посилання &#128279;](https://nextcloud.com)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://nextcloud.com/install/#install-clients)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://nextcloud.com/install/#install-clients)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.nextcloud.desktopclient.nextcloud`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

