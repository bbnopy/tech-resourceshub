# DISCORD



## Розділ &#127991; Інтернет &#127758;

>&#128161; Це платформа обміну миттєвими повідомленнями та цифрового розповсюдження інформації з функціями VoIP.

- **версія програми &#128230;**: :two: :three: :two: :six: :four: :three:
- **розробник &#128422;**: Discord, Inc.
- **офіційний сайт**: [посилання &#128279;](https://discord.com)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/discord/XPDC2RH70K22MN)
- &#128189; з **Epic Games store** за [посиланням &#128279;](https://store.epicgames.com/en-US/p/discord)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://discord.com/download)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://discord.com/download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.discordapp.Discord`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install discord`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install discord --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
- &#128189; за допомогою командного рядка:
  - спочатку додаємо репозиторій: ``sudo dnf install https://download1.rpmfusion.org/nonfree/frdora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm``
  - оновити лист репозиторіїв: ``sudo dnf update``
  - встановлюємо Discord: ``sudo dnf install``
  - може запитати імпорт ключів GPG: відповідаємо **Y**
