# ALACRITTY



## Розділ &#127991; Інструменти &#129520;

>&#128161; Швидкий, крос-платформний емулятор терміналу OpenGL

- **версія програми &#128230;**: :zero: . :one: :two: . :three:
- **розробник &#128422;**: Alacritty
- **офіційний сайт**: [посилання &#128279;](https://alacritty.org/index.html)

## Операційні Cистеми

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://alacritty.org/index.html)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з :octocat: за [посиланням &#128279;](https://github.com/alacritty/alacritty/blob/master/INSTALL.md)

#### fedora WORKSTATION

- &#128189; за допомогою терміналу: `sudo dnf install alacritty`

>&#128161;  Для збірки знадобиться декілька додаткових бібліотек. Ось команда dnf, яка має встановити їх усі

```bash
dnf install cmake freetype-devel fontconfig-devel libxcb-devel libxkbcommon-devel g++
```

