# PYTHON



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Інтерпретована об'єктно-орієнтована мова програмування високого рівня зі строгою динамічною типізацією. Розроблена в 1990 році Гвідо ван Россумом.

- **версія програми &#128230;**:
  - python2: :two: . :seven: . :one: :eight:
  - python3: :three: . :one: :one: . :five:
- **розробник &#128422;**: Guido van Rossum
- **офіційний сайт**: [посилання &#128279;](https://www.python.org)

## Операційні Cистеми

### Windows &#128421;

- &#128229; python2 з офіційного сайту за [посиланням &#128279;](https://www.python.org/downloads/release/python-2718/)
- &#128229; python2 з Хмарного сховища &#9729;
- &#128189; python3 з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/python-311/9NRWMJP3717K?hl=uk-ua&gl=ua)
- &#128229; python3 з офіційного сайту за [посиланням &#128279;](https://www.python.org/downloads/release/python-3113/)
- &#128229; python3 з Хмарного сховища &#9729;

### Linux &#128039;

>&#128161; Зазвичай `python` встановлено в операційній системі

- &#128229; python2 з офіційного сайту за [посиланням &#128279;](https://www.python.org/downloads/release/python-2718/)
- &#128229; python3 з офіційного сайту за [посиланням &#128279;](https://www.python.org/downloads/release/python-3120/)

#### fedora WORKSTATION

- &#128189; за допомогою терміналу: `sudo dnf install python`

