# KONVERSATION



## Розділ &#127991; Інтернет &#127758;

>&#128161; Зручний клієнт Internet Relay Chat (IRC), який надає простий доступ до стандартних IRC-мереж, таких як Libera, де можна знайти IRC-канали KDE.

- **версія програми &#128230;**: :two: :three: . :zero: :eight:
- **розробник &#128422;**: The KDE Community
- **офіційний сайт**: [посилання &#128279;](https://konversation.kde.org/)

## Операційни Системи

### Windows &#128421;

- &#128229; з сайту за [посиланням &#128279;](https://binary-factory.kde.org/job/Konversation_Release_win64/)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.kde.konversation`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install konversation`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install konversation --candidate`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

