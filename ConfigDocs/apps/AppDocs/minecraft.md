# MINECRAFT



## Розділ &#127991; Ігри &#127918;

>&#128161; Відеогра від незалежної студії Mojang 2011 року жанру «пісочниця» у відкритому світі з виглядом від першої/третьої особи.

- **розробник &#128422;**: Mojang Studios
- **офіційний сайт**: [посилання &#128279;](https://www.minecraft.net/en-us)

## Операційні Cистеми

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://www.microsoft.com/uk-ua/p/minecraft-launcher/9PGW18NPBZV5?rtc=1&activetab=pivot:overviewtab)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.minecraft.net/en-us>)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://www.minecraft.net/en-us/download)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.mojang.Minecraft`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

