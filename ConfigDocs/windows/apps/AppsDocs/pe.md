# PROCESS EXPLORER



## Розділ &#127991; Інструменти &#129520;

>&#128161; Провідник процесів показує інформацію про те, які дескриптори та бібліотеки DLL були відкриті або завантажені процесами.

- **версія програми &#128230;**: :one: :seven:. :zero: :five:
- **розробник &#128422;**: Microsoft Corporation
- **офіційний сайт**: [посилання &#128279;](https://learn.microsoft.com/uk-ua/sysinternals/downloads/process-explorer)

## Операційні Системи

### Windows &#128421;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://learn.microsoft.com/uk-ua/sysinternals/downloads/process-explorer)
- &#128229; з Хмарного сховища &#9729;
