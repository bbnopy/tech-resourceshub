-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt

opt.colorcolumn = "150" -- is a comma separated list of screen columns that are highlighted with ColorColumn
opt.encoding = "utf-8" -- Encoding displayed
opt.fileencoding = "utf-8" -- Encoding written to file
-- opt.guifont = 'Monaco:h12' -- This is a list of fonts which will be used for the GUI version of Vim
opt.hidden = true -- When off a buffer is unloaded (including loss of undo information)
opt.hlsearch = false -- When there is a previous search pattern, highlight all its matches
opt.numberwidth = 2 -- Minimal number of columns to use for the line number (default 4)
opt.showcmd = true -- Show (partial) command in the last line of the screen
opt.swapfile = true -- Use a swapfile for the buffer
opt.syntax = "on"
opt.title = true -- When on, the title of the window will be set to the value of 'titlestring' (if it is not empty)
opt.wildmenu = true -- When 'wildmenu' is on, command-line completion operates in an enchanced mode
opt.wrap = true -- When on, lines longer than the width of the window will wrap and displaying continues on the next line
