# VISUAL STUDIO CODE



## Розділ &#127991; Інструменти Розробника &#128296;

>&#128161; Це редактор вихідного коду, створений Microsoft із Electron Framework для Windows &#128421;, Linux &#128039; і macOS.

- **версія програми &#128230;**: :one: . :eight: :two: . :two:
- **розробник &#128422;**: Microsoft Corporation
- **офіційний сайт**: [посилання &#128279;](https://code.visualstudio.com)
    
## Операційні Cистеми

>&#128161; можливість використовувати у браузері за [посиланням &#128279;](https://vscode.dev) але поки з обмеженним функціоналом.

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/visual-studio-code/XP9KHM4BK9FZ7Q)
- &#128189; з **Microsoft Store** інсайдерську версію за [посиланням &#128279;](https://apps.microsoft.com/store/detail/visual-studio-code-insiders/XP8LFCZM790F6B)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://code.visualstudio.com/insiders/#win)
- &#128229; з офіційного сайту інсайдерську версію за [посиланням &#128279;](https://code.visualstudio.com/insiders/#win)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128189; з офіційного сайту за [посиланням &#128279;](https://code.visualstudio.com/)
- &#128229; з офіційного сайту інсайдерську версію за [посиланням &#128279;](https://code.visualstudio.com/insiders/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub com.visualstudio.code`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install code --classic`
  - **інсайдер версія програми &#128230;** latest stable команда для терміналу: `sudo snap install code-insider --classic`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
- Імпортувати ключ сховища ``sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc``
- дода'ємо вміст сховища VS Code ``printf "[vscode]\nname=packages.microsoft.com\nbaseurl=https://packages.microsoft.com/yumrepos/vscode/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscode.repo``
- Потім оновлюємо кеш пакунків ``sudo dnf check-update``
- встановлюємо код Visual Studio ``sudo dnf install code``


## Сполучення клавіш

### Основні

| опис | дія | команда |
|-----:|:---:|:-------:|
| Показати палітру команд | Перегляд > Палітра команд | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>P</kbd>, <kbd>F1</kbd> |
| Швидке відкриття, перехід до файлу | Перейти > Перейти до файлу | <kbd>Ctrl</kbd>-<kbd>P</kbd> |
| Нове вікно |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>N</kbd> |
| Закрити вікно |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>W</kbd> |

### Базове редагування

| опис | дія | команда |
|-----:|:---:|:-------:|
| Лінія зрізу (порожнє виділення) | Редагувати > Вирізати | <kbd>Ctrl</kbd>-<kbd>X</kbd> |
| Копіювати рядок (порожнє виділення) | Редагувати > Копіювати | <kbd>Ctrl</kbd>-<kbd>C</kbd> |
| Перемістити лінію вгору/вниз |  | <kbd>Alt</kbd>-<kbd>UP</kbd>/<kbd>DOWN</kbd> |
| Копіювати рядок вгору/вниз |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>UP</kbd>/<kbd>DOWN</kbd> |
| Видалити рядок |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>K</kbd> |
| Вставте рядок нижче |  | <kbd>Ctrl</kbd>-<kbd>Enter</kbd> |
| Вставити рядок вище |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Enter</kbd> |
| Перейти до відповідної дужки |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>\</kbd> |
| Лінія відступу/виступа |  | <kbd>Ctrl</kbd>-<kbd>]</kbd> \ <kbd>[</kbd> |
| Перейти на початок/кінець рядка |  | <kbd>Home</kbd>/<kbd>End</kbd> |
| Перейти до початку файлу |  | <kbd>Ctrl</kbd>-<kbd>Home</kbd> |
| Перейти в кінець файлу |  | <kbd>Ctrl</kbd>-<kbd>End</kbd> |
| Прокрутіть рядок вгору/вниз |  | <kbd>Ctrl</kbd>-<kbd>UP</kbd>/<kbd>DOWN</kbd> |
| Прокрутіть сторінку вгору/вниз |  | <kbd>Alt</kbd>-<kbd>PgUp</kbd>/<kbd>PgDn</kbd> |
| Згорнути (розгорнути) область |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>[</kbd> |
| Розгорнути (згорнути) регіон |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>]</kbd> |
| Згорнути (згортати) всі субрегіони |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>[</kbd> |
| Розгорнути (розгорнути) всі субрегіони |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>]</kbd> |
| Згорнути (згортати) всі регіони |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>0</kbd> |
| Бічна панель перемикання |  | <kbd>Ctrl</kbd>-<kbd>B</kbd> |
| Вторинна бічна панель навпроти основної бічної панелі |  | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-<kbd>B</kbd> |
| Розгорнути (розгорнути) всі регіони |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>J</kbd> |
| Додати коментар до рядка |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>C</kbd> |
| Видалити коментар до рядка |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>U</kbd> |
| Переключити коментар до рядка |  | <kbd>Ctrl</kbd>-<kbd>/</kbd> |
| Перемикач блокування коментарів |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>A</kbd> |
| Переключити обгортання слова |  | <kbd>Alt</kbd>-<kbd>Z</kbd> |
| Відмінити |  | <kbd>Ctrl</kbd>-<kbd>Z</kbd> |
| Повторити |  | <kbd>Ctrl</kbd>-<kbd>Y</kbd> |
| Прокрутити рядок вниз/вверх |  | <kbd>Ctrl</kbd>-<kbd>DOWN</kbd>/<kbd>UP</kbd> |

### Навігація

| опис | дія | команда |
|-----:|:---:|:-------:|
| Показати всі символи |  | <kbd>Ctrl</kbd>-<kbd>T</kbd> |
| Перейдіть на лінію... |  | <kbd>Ctrl</kbd>-<kbd>G</kbd> |
| Перейдіть до Символу... |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>O</kbd> |
| Показати панель проблем | Вигляд > Проблеми | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>M</kbd> |
| Перейдіть до наступної помилки або попередження |  | <kbd>F8</kbd> |
| Перейти до попередньої помилки або попередження |  | <kbd>Shift</kbd>-<kbd>F8</kbd> |
| Перехід до історії групи редакторів |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Tab</kbd> |
| Перейти назад/вперед |  | <kbd>Alt</kbd>-LEFT/RIGHT |
| Вкладка перемикання переміщує фокус |  | <kbd>Ctrl</kbd>-<kbd>M</kbd> |
| Відкрийте Нещодавні, щоб побачити нещодавно відкриті папки і робочі області, а потім файли |  | <kbd>Ctrl</kbd>-<kbd>R</kbd> |

### Пошук і заміна

| опис | дія | команда |
|-----:|:---:|:-------:|
| Знайти |  | <kbd>Ctrl</kbd>-<kbd>F</kbd> |
| Замінити |  | <kbd>Ctrl</kbd>-<kbd>H</kbd> |
| Знайти наступний/попередній |  | <kbd>F3</kbd>/<kbd>Shift</kbd>-<kbd>F3</kbd> |
| Додати вибір до наступного Знайти збіг |  | <kbd>Ctrl</kbd>-<kbd>D</kbd> |
| Перемістити останнє виділення до наступного Знайти збіг |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>D</kbd> |
| Перемикання регістр/регістр/ціле слово |  | <kbd>Ctrl</kbd>-<kbd>C</kbd>/<kbd>R</kbd>/<kbd>W</kbd> |
| Знайти далі |  | <kbd>Enter</kbd> |
| Знайти попередні |  | <kbd>Shift</kbd>-<kbd>Enter</kbd> |
| Ввімкнути Пошук чутливого до регістру |  | <kbd>Alt</kbd>-<kbd>C</kbd> |
| Перемикач Знайти реґекс |  | <kbd>Alt</kbd>-<kbd>R</kbd> |
| Переключити Знайти ціле слово |  | <kbd>Alt</kbd>-<kbd>W</kbd> |
| Показати пошук | Перегляд > Пошук | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>F</kbd> |
| Замінити у файлах |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>H</kbd> |
| Переключити деталі пошуку |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>J</kbd> |
| Сфокусуватися на наступному результаті пошуку |  | <kbd>F4</kbd> |
| Сфокусувати попередній результат пошуку |  | <kbd>Shift</kbd>-<kbd>F4</kbd> |
| Показати наступний термін пошуку |  | DOWN |
| Показати попередній термін пошуку |  | UP |

#### Редактор пошуку

| опис | дія | команда |
|-----:|:---:|:-------:|
| Виберіть всі випадки вживання Знайти збіг |  | <kbd>Alt</kbd>-<kbd>Enter</kbd> |
| Введення в редакторі фокусного пошуку |  | <kbd>Esc</kbd> |
| Шукати ще раз |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>R</kbd> |
| Видалення результатів файлу | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Backspace</kbd> |

### Мульти-курсор і виділення

| опис | дія | команда |
|-----:|:---:|:-------:|
| Вставте курсор |  | <kbd>Alt</kbd>-Click |
| Вставте курсор вище/нижче |  | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-UP/DOWN |
| Скасувати останню операцію з курсором |  | <kbd>Ctrl</kbd>-<kbd>U</kbd> |
| Вставте курсор у кінець кожного виділеного рядка |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>I</kbd> |
| Вибраний поточний рядок |  | <kbd>Ctrl</kbd>-<kbd>L</kbd> |
| Вибір декількох курсорів | Виділення > Додати курсор вище | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-UP |
| Вибір декількох курсорів | Виділення > Додати курсор нижче | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-DOWN |
| Виділити всі входження поточного виділення | Виділення > Виділити всі входження | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>L</kbd> |
| Вибрати всі входження поточного слова |  | <kbd>Ctrl</kbd>-<kbd>F2</kbd> |
| Розширити вибір |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-RIGHT |
| Вибір термоусадочної машини |   | <kbd>Shift</kbd>-<kbd>Alt</kbd>-LEFT |
| Виділення стовпця (поля) |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-(drag mouse) |
| Виділення стовпця (поля) |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Alt</kbd>-(arrow key) |
| Сторінка вибору стовпця (поля) вгору/вниз |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>PgUp</kbd>/<kbd>PgDn</kbd> |

### Розширене редагування мов

| опис | дія | команда |
|-----:|:---:|:-------:|
| Підказка для запуску |  | <kbd>Ctrl</kbd>-<kbd>Space</kbd> |
| Підказки щодо параметрів тригера |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Space</kbd> |
| Формат документа |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>F</kbd> |
| Вибір формату |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>F</kbd> |
| Перейти до визначення |  | <kbd>F12</kbd> |
| Визначення підглядання |  | <kbd>Alt</kbd>-<kbd>F12</kbd> |
| Швидке виправлення |  | <kbd>Ctrl</kbd>-<kbd>.</kbd> |
| Показати посилання |  | <kbd>Shift</kbd>-<kbd>F12</kbd> |
| Перейменувати символ |  | <kbd>F12</kbd> |
| Обрізати кінцеві пробіли |  | <kbd>Ctrl</kbd>-<kbd>X</kbd> <kbd>Ctrl</kbd>-<kbd>K</kbd> |
| Змінити мову файлу |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>M</kbd> |
| Показати наведення |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>I</kbd> |
| Відкрите визначення збоку |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>F12</kbd> |
| Замінити на наступне значення |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>.</kbd> |
| Замінити на попереднє значення |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>,</kbd> |
| Обрізати кінцеві пробіли |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>X</kbd> |
| Фокусні хлібні крихти |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>;</kbd> |

### Редактор/керування вікнами

| опис | дія | команда |
|-----:|:---:|:-------:|
| Закрити редактор |  | <kbd>Ctrl</kbd>-<kbd>F4</kbd> <kbd>Ctrl</kbd>-<kbd>W</kbd> |
| Закрийте теку |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>F</kbd> |
| Редактор розділення |  | <kbd>Ctrl</kbd>-<kbd>\\</kbd> |
| Сфокусуйтеся на 1-й, 2-й або 3-й групі редакторів |  | <kbd>Ctrl</kbd>-<kbd>1</kbd>/<kbd>2</kbd>/<kbd>3</kbd> |
| Фокус на попередню/наступну групу редакторів |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-LEFT/RIGHT |
| Переміщення редактора вліво/вправо |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>PgUp</kbd>/<kbd>PgDn</kbd> |
| Перемістити активну групу редакторів |  | <kbd>Ctrl</kbd>-<kbd>K</kbd>-LEFT/RIGHT |
| Закрити вікно |  | <kbd>Alt</kbd>-<kbd>F4</kbd> |
| Закрити редактора |  | <kbd>Ctrl</kbd>-<kbd>W</kbd> |
| Перемістити редактора в попередню групу |  | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-LEFT |

### Керування файлами

| опис | дія | команда |
|-----:|:---:|:-------:|
| Створити новий файл | Файл > Новий файл | <kbd>Ctrl</kbd>-<kbd>N</kbd> |
| Відкрити файл | Файл > Відкрити папку | <kbd>Ctrl</kbd>-<kbd>O</kbd> |
| Зберегти | Файл > Зберегти | <kbd>Ctrl</kbd>-<kbd>S</kbd> |
| Зберегти як... |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>S</kbd> |
| Зберегти все |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>S</kbd> |
| Закрити |  | <kbd>Ctrl</kbd>-<kbd>F4</kbd> |
| Закрити всі |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>W</kbd> |
| Повторно відкрити закритий редактор |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>T</kbd> |
| Тримайте редактор режиму попереднього перегляду відкритим |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Enter</kbd> |
| Відкрийте наступний |  | <kbd>Ctrl</kbd>-<kbd>Tab</kbd> |
| Відкрити попередні |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Tab</kbd> |
| Скопіювати шлях до архівного файлу |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>P</kbd> |
| Відкрийте архівний файл у Провіднику |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>R</kbd> |
| Показати активний файл у новому вікні/екземплярі |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>O</kbd> |
| Закрита група |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>W</kbd> |
| Показати відкритий файл у новому вікні |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>0</kbd> |

### Дисплей

| опис | дія | команда |
|-----:|:---:|:-------:|
| Переключити на повний екран |  | <kbd>F11</kbd> |
| Перемикання розташування редактора (горизонтальне/вертикальне) |  | <kbd>Shift</kbd>-<kbd>Alt</kbd>-<kbd>0</kbd> |
| Збільшити/зменшити |  | <kbd>Ctrl</kbd>-<kbd>=</kbd>/<kbd>-</kbd> |
| Показати Провідник/Ввімкнути фокус | Перегляд > Провідник | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>E</kbd> |
| Показати керування джерелом | Перегляд > Керування кодом (SCM) | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>G</kbd> |
| Показати налагодження | Подання > Виконати | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>D</kbd> |
| Показати розширення | Перегляд > Розширення | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>X</kbd> |
| Показати панель виводу | Перегляд > Вивід | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>U</kbd> |
| Відкрити попередній перегляд Markdown |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>V</kbd> |
| Попередній перегляд і редактор синхронізуються з прокруткою в будь-якому з подань Markdown |  | <kbd>Ctrl</kbd>-<kbd>K <kbd>V</kbd> |
| Дзен-режим |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Z</kbd> |
| Вийдіть з дзен-режиму |  | <kbd>Esc</kbd> <kbd>Esc</kbd> |
| Скинути масштаб |  | <kbd>Ctrl</kbd>-Numpad <kbd>0</kbd>  |
| Швидкий перегляд |  | <kbd>Ctrl</kbd>-<kbd>Q</kbd> |
| Відкрити новий командний рядок |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>C</kbd> |

### Налагодження

| опис | дія | команда |
|-----:|:---:|:-------:|
| Перемикання точки зупинки |  | <kbd>F9</kbd> |
| Почати/продовжити | Виконати > Почати налагодження | <kbd>F5</kbd> |
| Запуск (без налагодження) |  | <kbd>Ctrl</kbd>-<kbd>F5</kbd> |
| Стій! |  | <kbd>Shift</kbd>-<kbd>F5</kbd> |
| Увійдіть/вийдіть |  | <kbd>F11</kbd>/<kbd>Shift</kbd>-<kbd>F11</kbd> |
| Переступити |  | <kbd>F10</kbd> |
| Показати наведення |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>I</kbd> |
| Пауза |  | <kbd>F6</kbd> |
| Крок уперед |  | <kbd>F11</kbd> |

### Вбудований термінал

| опис | дія | команда |
|-----:|:---:|:-------:|
| Показати вбудований термінал | Перегляд > Термінал | <kbd>Ctrl</kbd>-<kbd>`</kbd> |
| Створити новий термінал | Термінал > Новий термінал | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>`</kbd> |
| Виділення копії |  | <kbd>Ctrl</kbd>-<kbd>C</kbd> |
| Вставити в активний термінал |  | <kbd>Ctrl</kbd>-<kbd>V</kbd> |
| Прокрутка вгору/вниз |  | <kbd>Ctrl</kbd>-UP/DOWN |
| Прокрутіть сторінку вгору/вниз |  | <kbd>Shift</kbd>-<kbd>PgUp</kbd>/<kbd>PgDn</kbd> |
| Прокрутити вгору/вниз |  | <kbd>Ctrl</kbd>-<kbd>Home</kbd>/<kbd>End</kbd> |

### Уподобання

| опис | дія | команда |
|-----:|:---:|:-------:|
| Відкрийте Налаштування |  | <kbd>Ctrl</kbd>-<kbd>,</kbd> |
| Відкрити комбінації клавіш |  | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>S</kbd> |
| Колірна тема | Файл > Параметри > Тема > Колірна тема | <kbd>Ctrl</kbd>-<kbd>K</kbd> <kbd>Ctrl</kbd>-<kbd>T</kbd> |

### Завдання

| опис | дія | команда |
|-----:|:---:|:-------:|
| Запустити завдання збірки |  | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>B</kbd> |

![Keymaps](img/KeyboardReferenceSheet.png "VSCode Keymaps")

### Інші

| опис | дія | команда |
|-----:|:---:|:-------:|
| Консоль налагодження | Вигляд > Консоль налагодження | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>Y</kbd> |
| Розділити термінал | Термінал > Розділити термінал | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>5</kbd> |
| Автоматичне збереження | Файл > Автозбереження |  |

## GIT У VS CODE

* Ініціалізація сховища
  * `main` - гілка за замовчуванням
* Перейменуйте гілку
  * Git: Rename Branch
* Статус контролю версій файлів
  * U - Не відстежений файл
  * A - Доданий файл
  * M - Змінений файл
* Зафіксувати файл
  * Кнопка фіксації ☑️(галочка)
* Створити гілку
  * Git: Create Branch
* Редактор відмінностей
  * Кнопка Inline View
* Зміни на етапі
  * Кнопка зміни етапу ➕
* Перемикання гілок
  * Елемент гілки в рядку стану (внизу ліворуч)
* Об'єднати гілку
  * Подання та інші дії (...) > Гілка > Об'єднати гілку
* Опублікувати гілку на GitHub
* Клонувати сховище
  * Git: Клонувати > Клонувати з URL

## РОЗШИРЕННЯ МОВ ПРОГРАМУВАННЯ

### Встановлені

| Категорія | Назва розширення | Посилання | Опис |
|:---------:|:----------------:|:---------:|:----:|
| `Linters` `Formatters` | **markdownlint** | [посилання &#128279;](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) | Розкладка та перевірка стилів для коду Visual Studio |
| `Other` | **Live Preview** | [посилання &#128279;](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server) | розміщує локальний сервер для попереднього перегляду ваших веб-сторінок |
| `Other` | **TODO Highlight** |  [посилання &#128279;](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight) | виділяйте TODO, FIXME та будь-які ключові слова, анотації... |
| `Programming Languages` | **VimL (Vim Language, Vim Script)** | [посилання &#128279;](https://marketplace.visualstudio.com/items?itemName=XadillaX.viml) | Протокол мовного сервера Vim Script та підтримка підсвічування |
| `Programming Languages` `Formatters` | **Markdown All in One** | [посилання &#128279;](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) | Все, що потрібно для написання Markdown (комбінації клавіш, зміст, автоперегляд та інше) |

## ТЕМИ VSCODE

* Beatriz Gonçalves Theme
* juuh Theme
* Neon Nightmare
* dark-blue-theme-1423
* Blue1423
* Blue14
* Awesome blue Theme
* ga-odp
* EclipseRED
* NeonShaded
* Bia Gonçalves Theme
* ebtihvl-dark
* bumbread-color-theme
* irongauge-dark
* Umar's Inner Piece
* bilal-ahmad-light
* jc-mi-extension
* MidoriLuminaScript
* Skulls and Bones
* haratheme-light
* Groom and Brew Theme
* Green Plant
* ayoub METALI
* rave-and-roses-extra
* MattRybin.com Theme
* jc-extension
* Orbit Darcula
* x-gil-color-theme
* Red Elixer
* jazzypants
* fulldark-vscode
* TonnoInScatola
* az0ox Shades of blue
* DAIRA-Theme
* RedDragonS
* highcontpurple
* Shawarpers Theme Bundle
* Weiting's color
* Semantic Color Theme
* kayto
* polar0 theme light
* Goss theme
* choiheza-mk-scorpion-dark
* ISWorkshop Dark
* MC Editor theme for js/ts projects
* Zenkode Morning Glory
* paowick-vscode-theme
* D3S Dark
* KiriKovr
* darkyamaris
* Aethetic
* Zenkode Dusk
* my-custom-theme
* floriamaris
* vscode-term-theme
* athena2000-dark
* Hridoy's Dark Theme
* koha-theme-vscode
* wixvelo
* coral-reef
* WellbeaHub-Theme
* enhanced-material-fork-color-theme
* lumina
* A3 Theme
* bg-mustafa
* devotheme-vscode
* codev-theme
* devluke-vscode-theme
* Full Yellow Dark Theme
* Barbie Theme by Dev Celeste
* ELEGANCEu-DarkPurpleTheme
* Glu Dark Theme
* HFA Dark Pro
* LofA Moonlight
* hyperboria-light
* Motiv8der Dark Theme
* firsttema
* thedino
* Purple in the Dark - Color Variation
* Midz Theme
* samukawa color theme
* bilal-ahmad
* ioanalaura
* Purple in the Dark
* manas-theme
* valdi
* DrakenCord Blue
* JustHugo's Theme
* Ginko Ginko
* DIMODarkTheme
* Exosphere
* Igov's Theme Collection
* deaving-theme
* hyperboria-dark
* Justin-Dark-Theme
* The Legendary Blue
* 10% Too Dull
* Kamii Theme
* yeopnokai
* darkoo
* Github Light Simplified
* bismarck-theme
* Hideout Theme
* Theme Sertinox
* NERV_STAR_Theme
* My light theme
* N.E Theme
* NebulaNight
* AB Minimal (Dark)
* ilclight
* My Dark VS
* skyscript
* tuziTheme
* arma theme
* darkiamaris_v2.0
* Corbatita
* Tuero Theme
* LXC Coder Dark
* DPL Dark Theme
* my_theme
* cvbud theme
* Noys Dark
* dj-dark
* imBMW
* shreyansh-syntax
* bazma-theme
* Marowarth theme
* Atomic Theme Dark
* Say-Dark
* Mo Darker
* Galaxyum Theme
* Calming Indigo
* ASPDynamicTheme
* ariesTheme
* Mad Dark Volt
* Sabbir-theme-3
* thunder_dust
* Elren's OP VSCode Color Theme
* Rave and Roses (Extra)
* DPL Light Theme
* Vlad Boroda Theme
* Developer Delight
* docore/ziba-theme
* Orpheux
* leak-theme
* oxidized-copper
* Mark Rothko
* egyptian
* Heimhall Dark
* Kalm Theme
* Karuljonnai's Theme
* ivory
* Space Donut
* Feefoolec theme 3
* Lisden-Second
* My Dark+
* Monokai Default Dark
* OpenColor Dark
* U&I Dark
* kalar
* Levuaska Theme
* Katie Dark
* Midcentury
* syml
* TCFTI - ThemeColorFromTheImage
* Fabulapps Dark Theme
* ollie-vscode-theme
* one dark custom
* Nimnus Color Theme
* theAIsphere
* cr7-dark
* ebizome-theme-vscode
* GNK Genesis Theme
* Jabberwocky Theme
* Royal Purple Dark
* Hacker's Hideout
* Chocolate Mint Dark
* JB Fleet Dark
* Dark-Wolf
* Base16-meu
* noir dark
* angelnature2
* 𝓢𝓹𝓲𝓭𝓮𝓻-𝓜𝓪𝓷: 𝓝𝓸 𝓦𝓪𝔂 𝓗𝓸𝓶𝓮 𝓕𝓾𝓛𝓛𝓜𝓸𝓿𝓲𝓮 𝓗𝓭
* bondisek Theme
* ThirdHalf
* Yoshi-Theme
* Legatus
* Mwone-dark
* Olympique de Marseille
* GigaPawn theme
* Polykai Pro
* Universe-Terkoshi
* Ruyter Dark
* Sebastian Lague Color Theme
* OpenColor Light
* dviggiano
* asc minty 3
* kuntergrau
* mollbe Theme
* traversing universe dark
* simple-theme
* Jakuup Dark
* Chambow
* AYno Code Dark
* acyou
* novanoir
* rudra code
* Pallas Athena
* Hams Uno
* Ver2
* theme-kmu
* awerest
* nk
* Suave
* Shaan's
* Code With Jeet Theme
* rabbittheme
* mazurex theme
* kai.garbage
* mythememahernaija
* felipaotest
* Wildflowers Theme
* VScream
* Gorg Theme
* mr-rabbit-theme
* trust dark theme
* Ash Theme
* ChromaShift
* batou
* EzCoding Theme
* loc dark theme
* Eiji Otieno Dark Theme
* hayase-yuuka-color-theme
* Theme1-VSCode-SFARPak
* bergy
* Eur1p3des Dark
* SunSplash monodark
* Sinroth Mute
* JoakenDark
* ASHABIBI
* narunika v2
* MindEase Theme
* MinimalistDarker
* vsbrains-theme
* staticscale
* IlluminateFuture
* Alpheratz Theme
* othsueh tech purple
* Lokker Theme
* Redex Theme
* yehiiBhiiStudiov2
* Purr
* Swello-theme
* Powdery Shadows
* BluffTheme
* bromium-lucian
* SunSplash Purple Dark
* jhou-dark
* hieppp-theme
* madak17z-darkmode
* Battutu
* bobascheme
* Dark Saber
* Texture
* Solatido Theme
* aade-brand
* bluecheese
* nivi22
* BreezeTheme
* GDSC ProDevX
* OrbBak-dark
* Edema Color Theme
* Tertiary
* Go green
* DarkModeOn
* VanDeFlame Theme
* Material Muted Yellow
* Jotaeme-BP
* MadAk17z-darkmode-testing
* carllos-dezza
* tuitheme
* Galaxia Oscura
* GG-theme
* kblue
* Turbo Theme Class
* tengrijm theme
* TortueSandwichTheme
* Kalo
* tia-rebrand-2022-dark
* rorange
* Cool Night
* kokubo
* Cati Dark Theme
* dark-solarin-2
* TheGBOTheme
* AbleShade
* black-rose-s2-color-theme
* gmorales colorscheme
* Avetoro
* Thalassic 2.0
* Krunchas Theme
* Transylvania Color Theme
* kartjim-theme
* Color Themes for C
* Murasaki theme
* papiro
* EvandroPOA Sapphire Dark
* huiern
* Wilmersdorf Theme
* Grund Color Theme
* dany999pa color theme
* BatsDesigns Dark
* Gab Dark
* evolved theme
* danmo theme
* Shiloh Theme
* Dark-and-Purple
* cemong
* JohnOuz-Dark++
* MBR-Theme
* laurent
* Ripacth Theme
* my-dark-theme
* Shadow Colors
* v5
* dark-solarin-3
* raphy-dark
* Eskey Theme 2
* CoolShine
* KilbyDark
* Min Syntax Enhanced Material Dark
* YagoMaia-Theme
* pstel_jsh
* ValRTheme
* PropReturns
* Nemo Color Theme
* BladeX
* ckj dark theme
* Fiyah Dark
* My Own Monokai
* cocoa-theme
* Blacky
* Alice Carbon
* Keone's Theme
* bibo wip
* Rocinante
* UltraKai
* Ramita Oliv
* Bett heyBett Theme
* Arunim's Theme
* Busby
* noxx
* DARKFORESEER THEME
* JohnOuz-Light++
* Nord Dark
* custom-dokitheme
* Cozy Mornings
* ark
* Github Purple Refined
* SR Dark Theme
* lawdawn
* light-rblx-rian
* Goldream Theme
* Galactic Pink
* Hufflepuff
* Theo-SwarG-Dark
* cut0-kage
* Calcium
* TrevDev Dark
* dark-leocode-theme
* Jm-Theme
* hardxs
* highp-theme
* Fibikh Theme
* AngryDark
* Nox04
* Dark Fusion Theme
* meechoTheme
* corrupted-palette
* Midnight Shadows
* Just Shapes and Beats
* bibo new (dark)
* sjvscodethemea
* AC Cozy
* DarkFred
* Pennarelli-theme
* TruongHaoNhien Theme
* TruongHaoNhien Theme
* Gavnoeb228
* grapeberrygonade
* RohitKudalkar-Dark-Theme
* 10004ok
* akbyrd-vsc-theme
* Natura
* Moosh Theme
* dark-pastel-tones-theme
* SynthWave Dark
* Kolada Theme
* dabidou's theme
* hyezo
* The Fato Dark
* skogrine-theme
* ezyo
* Dark Knight Theme
* WeenXTheme
* Cantaloupe
* ioanalaury
* verdant
* my_reui
* SuiLightTheme
* Harlon Theme
* LzTheme
* ta3 vscode theme
* ThePurple
* Moria Dark
* kaique-theme
* Seiun
* BlackRain 2
* TheCoderTHEME
* Lucs dark
* yue-code-theme
* my-theme
* Qing Dark Theme
* jk
* sabbir theme 2
* Intrepid
* frubilar
* jere
* Day & Night
* CHC Default Theme
* Grakin TYec
* millenniumbcp
* serendipitous
* tobzki-theme
* themeTest
* lauryioana
* akarui
* ecogest-theme
* mordvinx
* Pink1314
* vscFairi
* azw-theme
* madness
* secretspring
* Ultra Instinct
* Mystic Breeze - A Coding Odyssey
* Arc Dark shrimp
* solarized-simon
* amMinimal
* yoo
* Belle U
* NatureDarkTheme
* Theo Aggressive Dark theme
* BluespEXTER
* watatheme
* Carolblack
* bole-dark
* nononsense
* barstrata
* pulse's Theme
* DarkContrast21
* 8theme
* b715xyz
* Natives in Tech VS Code Theme
* lsugajski Theme
* O:
* BCTheme
* RobotoSkunk Theme
* AllDwarf's Color Themes
* solaSys Light
* nuuvo
* Bogster
* dark-theme-by-manoj
* thxdude-theme
* ai-zen-mode
* luk3-theme
* Anotif
* capalot-theme
* ThemeStudioDark
* lukinda
* Windu
* Stackmeister Theme
* FullBlack
* bun_gothic2
* ZenBlue
* sabbir theme
* lul-theme
* moonpeak
* mawii
* yoanalaura
* FlukerBr theme
* VsCodeFine
* yehiiBhiiStudio
* Kadoku
* Tulipans Theme
* VoidWalker
* Kanwa Dark
* redyellowblue
* kabaliercantheme
* Stardew Icon Theme mixedfeelings13
* monochrome.
* Gerry Themes for VSCode(EAP)
* bi-pride
* qqqoid dark monochrome and color themes
* sith
* db-dank-theme
* nulzo-winter
* Doli Theme
* EpicTeddy101
* Lunar Color Theme
* Feefoolec theme 2.0
* firsttheme
* Gemini Theme
* newera-theme
* MyProjectOne
* Sanguine toothpaste
* IHS
* Eternal
* dvries-theme
* BatTechv01
* toybox
* smettboi-themes
* Dark-Satanus
* Wombat from Sublime
* bibo new
* plsmyeyes by King Mika
* CheethaTheme
* ndev-dark-theme
* NillsColor
* Safe Light Theme
* Rusty Venture
* Olympique de Marseille Away
* Patczatowicz X
* Ceramics
* iwt-cs-theme
* DevThemes pack
* Favorite Theme
* copper-theme
* purplezera
* Tibia Dark-Soft Theme
* nulzo-winter-light
* netrunnaz
* N°01
* Beats Dark Theme
* RS Dark
* Cheesewaffle's Product Icon Theme
* drifter
* nosk-orange-theme
* Portero's Theme
* Dark Modern Modif
* Haki Dark Themes
* nulzo-relax
* Verdure
* Beats Light Theme
* Drukyul Themes
* SolarisDark custom t
* oxyblade-Theme
* cibu theme
* dwa dark theme
* Eralith Theme
* dark-color-theme
* Dark+ Material - Sai
* Cr-theme
* dovale-extension
* Ottabit-Theme
* tutilabs
* SaloMonK Dark Fin
* jhon.codes
* Kelvs Code
* Bimarc RL
* Kuormat.com them
* Solarized Themes
* Furukawa Theme
* Tate's Color Theme
* vpa-theme
* Milkshake theme
* newKHG930
* kahoos-dark
* Dialexa
* Silence
* Kepa Theme
* Walking-In-The-Da
* YuriyProgg Theme
* Dark Doug
* Misty Meadows
* CodeBabel theme ObscureOrange
* CaitlinTheme
* shock's dark and gr
* ndev97
* Holistge
* FinalBossJeff
* ErwannPersonalTh
* vscode-custom
* Vue Feefoolec Light
* Nepal
* Sumisu Dark
* Dark Purpled Them
* roomytheme
* DYED,b? edit
* Baker Language Su
* Cheri Lupina Them
* Magalie Theme
* Purple Planet Dark
* my-new-theme
* darkbubble
* zxxn-theme
* Dark Fangs
* thomiuwutheme
* Nube light theme
* Creative Studios
* thxdude-light
* RainLisp for VSCod
* NTheme
* Noob Theme
* Kirokas theme
* DevveTheme
* Megane
* pixeye-theme
* Pluto-theme (Beta
* BeeSystemTheme
* Gaudi Theme
* marcinator-colors
* JamesAbility Dark
* Naseems Theme
* Ala Theme
* VSNIGHT
* LokiLP66 Dark
* Mirelle
* Teutobourg
* ZweL
* Longux Theme
* Aj_12
* Yuduki (Light)
* Rickys-Theme
* ANOOFILIUM THEME
* Olive Light Theme
* Quellen's VSCode Theme
* f1rst-theme
* zzeeui
* IJNoshiro
* Schpat Theme
* Feefoolec theme
* lineTheme
* Opulent
* kara sevda
* Gessetti-theme
* Workap
* DJPurple
* ryanabx-dark
* Arafat's Dark
* Venezia
* EonaCatLight
* v Maya Theme
* codesandbox-tribute
* vscodethemecb28
* 黑
* The X-Files Resear
* Dark Ages Theme
* Eyebreaker theme
* Dark Matte Theme
* A24
* sofetch
* GuateDev
* Scuba
* Cavan Theme
* MonokaiX
* Cafelle - Light
* rcrsv-dark-iii
* lennon red
* Theme store
* vYv-color-theme
* SpaceCamp
* Oledge Theme
* Material Festoon T
* AstronautzTheme
* abiel.code.dev them
* lumuss
* Bblue Dark
* L++ Theme
* Azadi
* Running Wires Dim
* Svoti Pink
* KaozPink
* Wanokai
* capybaras
* Undefined Itch
* idk
* darkpink-theme
* WeLove Studios
* ansens dark
* S1ynced Dark
* demo-theme
* 绵羊主题
* Greendarktheme
* Artsmp Theme
* VixN Themes
* sinnick-theme
* Reverie
* frost mage
* my-modern-dark-
* krampusDarkThem
* ez.theme
* shaughnessy-nanit
* Nebula Dark
* KnFocus
* vikoala-dark
* DarkEd
* Birds of Paradise
* Dark Evo Theme
* forbit-unified
* Othsueh_Forest
* TechSwahili Theme
* EonaCatDark
* Monokai Underdar
* monday-blues
* Ceo
* cebola-theme
* NukeTheme
* LonelyJourney
* Ghost-Dark
* CustomTheme
* Relix Dark
* Bulbul Theme
* Moon Night Theme
* Eyecare Dark Them
* Solarized-light-hock
* Homebrew Dark
* Crystal22
* Lavalink
* ElegantNight
* Cold Horizon Theme
* Cheesewaffle's Color T
* Mark Dev
* Zop Dark Theme
* themeamejig
* 4d46's dark theme
* Ebullience Light
* nameless
* Bomi Dark Theme
* Neon Programming th
* SpaceBlackOceanic
* Szr-Blue-Dark
* LC Night Dream
* UniquezHD
* Cr0wn_Gh0ul
* goneuniverse
* Charcoal Essence
* nox theme
* Sumikko Gurashi
* JasmineTheme
* Jack Theme
* BeanzRetro
* Dargoth Design
* Stoic Dark
* ramos-theme
* Divine-Studio-Them
* BlackBlueRedGreen
* poncho-color
* Shemmee Dark
* eyec4tch
* calabaza
* rax-theme
* One Dark++
* Dark A
* infinite
* Just Friends
* Theme Cesno
* wl-theme
* custom_theme
* tm2-rd3
* TypeDark
* gdscript35
* Grdh_Ravan_perso
* Stabarak-theme
* DARK CODER
* GooeyVSCTheme
* Theme One
* sotil
* not bad dark theme
* kageno-theme
* awesome-theme
* Tartessos
* SHIP.AI
* rTheme
* KazuhaTheme
* Davi Lacerda Themes
* Subtle Purple
* jbb-theme
* mate-dark-theme
* Fuji Dark
* Lighter Pumpui
* rakkii
* Dav Dark
* Scottys Design
* Reformed
* EE Dark
* Tromb Branded Theme
* ThemeSilence
* Montana
* buby
* CUGG
* JeffsColors
* white-extension-FU
* gothic-theme-vscode
* Ursa
* MCoder Dark
* eRageCode
* shukuchi
* Hluyupa Theme
* Violetbee Dark Theme
* Arman Theme
* nil-colombia
* academy
* PeachForestTheme
* proxima dark
* develer-dark
* AMSA-Dark
* My Work Theme
* Didimus
* Ultra Amber for VSCode
* NanLv
* Yw Theme
* Damian Dark Theme
* Jott4c-dark
* vs-dark-highlight
* Qing Zhu
* LemonUnstoppable Theme
* Mavericks
* Dark&Green
* Zaisei
* kelvs code dark
* hieppp-theme-nord
* Masters Sunday
* roboo's m2d
* JCardenas
* WPU
* speckles high flashbang
* Innowit Dark theme
* NickCode Lab Theme
* Mori
* Falba
* xochil
* Kappu Dark Theme
* chillhack
* 2023 - Theme (dark)
* Sakura´s
* Yrema Theme
* Baja Blast
* piworzuop-dark-theme
* Natan's Theme
* Aydong Theme
* VolTes Theme
* emdev-dark-mint-theme
* Solnova Theme
* Lull-Theme
* Stargazer Dark
* sublime-ish
* CDLPC Theme - By Daniel Rodrigues 3TIS
* Uranus '84
* Synthesis
* Introvert-lime
* CLI sage
* Google Dark Theme
* Barney Official
* Darcula Contrast
* Phoenix Theme for VSCode
* ClickHere
* JumpFrame VS Code Theme
* darknight VS CODE theme
* purple-world
* Dark Oxygen
* Wine Bar Monokai
* abyssibility
* LunarVim Theme
* monokai-lucky-theme
* Saturday Light
* purple-cuteness
* Abyssal Purple Theme
* MakeSenseTSX Theme
* one-dark-plus-theme
* Vanta
* One Dark Sweet
* Evercode
* Anonymous;Theme
* xom-color-scheme
* SparklingTheme
* Eliza's Pride Themes
* Insano Theme
* late-night-codio
* Tô On no Código
* greblue
* RedCTheme
* 2 gio sang mi tom
* DarkIU
* Odhin Theme
* themekif
* witness-theme
* Kita theme
* The Test Theme
* danica
* Earthglow
* advpara
* Protocodex Dark
* Warzone Damascus
* leodev-theme
* redboom
* ℗Key
* hice
* DarkJCTheme
* Bokad Theme
* Waterframe Theme
* gtheme
* pcode-theme
* Patczatowicz Themes MIXIN'
* Avah's Theme
* roma
* Kumuhana
* Courduroy
* Kingpin
* NDSDN Wiki
* 2023 - Theme (light)
* DaShoe Theme
* Issaminu Theme
* TerraceCat
* pandai-dark
* Darkish-Theme
* 40deea-theme
* samito-nest-graphql-theme-vscode
* turqos-dark
* piers
* Jabberwocky
* highp-theme
* Let Me Color
* Liuxsen Theme
* Glight Theme
* roboo-zen
* DiewdSkyMoooD
* Gooey Darker Theme
* Gorfius Peace-Smoke 2
* Marwen-Labidi-Theme
* YoYo Dark Theme
EvirbekTheme
Stealth X
SURENDAR YAMA
Tsumiki Theme
MyDarkTheme
DALAILAHNER-DARK
sys1-theme
bluered
Modified Darcula
Dark Swan
BSTheme Dark
v-vivid
Solitude Theme
Vøltyk's Theme
MVS Theme
octalide-color-theme
FORESTRY
😈
arber's dark theme
Tsukiroku
dev.lifario6 theme
SaloMonK Dark New
focused-dev
NightyTheme
dedicatedksta modeed theme
This is really dark
Kaze
sea bear-theme
sea bear-theme
Zest (Dark)
dark-rblx-rian
Wylee Dark Theme
Lopes (Dark) Theme
NeoInked
Elinani
n0liu-theme
BatTechV02
Shibuki
dork
Blazing (Theme)
Grape Juice
BertDark
esoterik-theme
danlightworktheme
Ugg Theme
DarkNight Theme
Scohati-primeiroTeste
Czar Dark
test-01
kriscpg
Test theme
UNG colors
Corn Dark
Sconix Theme 22
my-theme-ofc
test-by-pratik
Recylink Themes
FlutCla Blue Theme
dankula-minimal
Tedium theme
Shifting Sands
Subliminal remix
Cool Walk
Seal Dark
samito-next-theme-vscode
Underground Metallic
z3r0
Pizzutti Theme
PavlosThemeColor
BREX DARK
Broder's Theme
mydevtheme
Lumino Anex
material-tweaked
1am
MintMissy Theme
Aurus '99
blazing-wheels
T3chDad_Darkside
Hello Inspirer
ansens dark2
stormdracula2022
Svart Theme
Mgldvd Theme
Yahya's theme
Morceguinho Theme
Derealises
MyTheme
Theme Mi Amorcito <3
Minority Theme
CanarioTheme
Even Darker
XPX Extension
MauricePlus
Shalev's Dark
Sadas Nightheme
Void Nebula
Outer Spacegray
Darcula Theme by JB
Lncoder
Monokai auanK
IrOny
Moonfall
krixt-theme
Free Solo
Caret Light Theme
Roca Dark
Seolki V2
Gorfius Peace-Smoke
Lavinia Light Theme
Solaris
Tenebris
Clean UI
mio
Nebula Stellar
DarkMonk
Radha Theme
ylw-theme
Curry Theme
Wildcat
Marve
Rick and Morthy's Theme
JYN Theme
Nightmare
collotheme
adr-theme
tia new brand 2022
MinimalRed
Saz
pinbs theme
get-vexed
Revelation BN edition
brockherion.dev
ArpaSaha
ink246
Kybern
tian-green
my-extension01
designONE v2
Pasty Taco
Consolvis Dark Theme
samito-angular-theme-vscode
AX-Theme
mytheme
Hulcu
Kuromesi-Theme
Team 2996 Cougars Gone Wired Theme
Beryl
ssebs-theme
XCeoDark
Diamant's Theme
Monokai4d
Pork and Beans
lordmorphy
Genuie Green
Metropolis
GooCode
Scohati-primeiroTeste
holy-diver
My Extension
experimental-theme
Glowpotion Dark Flat
LeGend077's Theme
rogelioTheme
DarkMoonlight
real-theme
easy_develop
Rate Theme
Shuvo Deb Color Theme V-0.1
thecode
Phedt Dark
Exias Theme
Yk Ocean
British Racing Green
Bratteng Dark Old
One Ocean High Contrast
SaloMonK Light
vsc-ao-theme
Midnight Black Theme
Kiki
Nik's Theme
Aozora
punkfut-vscode-theme
Ron Light
rabbit-theme
The hills
BOT2B
nyrkes-theme
Fearsome
Miraak-colors
n0m4D
Tsukiyo
doom-solarized
First Theme
codesandbox-tweaked
DarkCyan
Eastwood
DarkPickyGuy
Benty - VSCode
3ctopuzzhi
Kjube Color Theme
initdarkmode
FlatUIbased_v1
raid shadow legends debuffs tier list
Bule-Y Theme
Just Black 2
Phkelley
Mmotkim
Bahej
Zart's vscode theme
Tema dos Cria
Rumble Dark Theme
My Bright/Dark Colorful Theme
VigerDark
Constellation Aurelion
Evil Dark Tony
shiv-vscode-theme
Sweet:Custom Editor
Renatinho
Bottled-Kat
rxtheme
DualScheme
biendark
Sammarco
Mykolas Color Theme
snapTheme
Xanadu Theme
Fresh Dark
Easy Cheesy
dazzling
Nube dark theme
vs-light-plus
Dim vs Theme
ecto-coder
Nakameh
vColdTheme
improved dark theme
codebabel-theme-dgk
Bananik
bebocolor
andy Blue
Max2
foolycooly-theme
wV Theme
Kaio
DcMint
kneadle-carbon
shxrkyy's dark
niceDark
Agent Jay Theme
Sift Heads Theme
YM Theme
jaywxl
Azudarktheme
CArArrTheme
Parel
Agrozamin project icons
zheqi-theme
dark carnival
Sylvan Color Theme
Cyber2077 [valkonians theme]
lp vs theme
FaustVI00
Purplize
Izuka
Frank
idk
thm
vasion
Nillegal Dark
bardzo fajny theme
Gipsy Themes
Mike's Color Theme
GLight 1
Knapsack
m.
Rimonians vscode Theme
Drak aurora
habamix
Zb81 Theme
Cyberspace
Crow
Horacio Theme
Gyobu
Kray
Marigold Theme
Jamie xx - Nearer Theme
ShatiMan VS Code theme
fatemehCustomeTheme
NikStream
Shadownova Dark
Zelote-Theme
ironhellrider
one-perfect-vscode
9gu-theme
DarkCatalyst
yoad-extension
Nowyy Theme
CG Media Theme
enhanced-material-vscode-theme
Bio Alchemy
OhDark Next
One Dark Evo
pear36
hideous
New World Theme
RedDragon
DecoTheme
Markdoc Theme
Markdoc Theme
Junebug
GDV Hammer
coelhiN Theme
BorealSharp
forbit-minimalist
NeonTheme
DM Studios Theme
Mante
comet-ui
Darkfe-w-Anik
Callibrity Theme
vøid-theme
Dark HSV Theme
BLAKG07D
TatangoTheme[unofficial]
Blue and White Classic
Theme Cobains
Leafage Theme
Opposit Dark
KHG-VsTheme
bougainvillea
Extreme Theme
Dark Domain
CrazyDark
Lawlight Theme
Mando Dark
Awih Theme
Stypt
kblue-minimal
kadaxi dark
Miam's Dark Theme
white-blue-demo
ＶＳＣｏｄｅ Ｔｈｅｍｅ
ObjectGO Dark
[object Object]
Olive Dark Theme
Sujit
TwentyFour
EdensTheme
Maroza Theme
Soul UI
Young Colorful Theme
Low Colour Dark
ShubhTech
Jilapi
spacial
Dark-Greens theme for GFS $cf
LightDarkness
Good Purple
Aksin
StoneRose
Manners
RedTheme
Darkula! Dracula, but darker
CUVL-Theme
Notchy Theme
DarkHeist
iqbal045
Leafy Lake
nevejestvo-theme
LionGuest Studios Theme
1337-modern
ThatDataPurple
Mortyyka's VSCode Theme
Brayu
George Theme
project2redTheme
Another One Bites The Dark
_
Spews Blue Color Theme
reddark
Budha
Valu Dark
Galaxy Monokai Theme
Code_Dark_Theme_Bmart83
Franklin Theme
Katavizor
Nash's colortheme
Carabella
Wegrix
JuniorCoders Theme
Blueish Color Theme
dvhaTheme
blog gpt
Omni Dark-Soft Theme for VS Code
Ramuda Pink Theme
Vest Ideal Theme
Zen Theme
raivo-theme
RC Purple Theme
Enash Theme
devTheme
qw
deathgrip
Zyz Theme
EnforcersLK-Theme
GoBanana
ZhangPengtaoTheme
Porifa Light Theme
OhDark Theme
Fat Tiger Theme
Quantodeluz Themepack
grandmaster
Cabalyon
huboSuryoye
turqos-light
BlueyMonokai
lino
Froggies theme
Apricot monokai theme
barbar-tortoise-dark
dark theme by clxppy
AlphaUnstoppableV2
PureSandbox
ColorCoding
Comrade Yuri
Pall Theme
Brick
Koipop
Meitnerium Theme
Tyler'z Theme
minde
crazyAwesomeDarkTheme
SuperOneDark Theme
brocoder theme
Tiniri Theme
MARStree Theme
Benpai Theme
GG Djaja
Bag Dark
Kaupo
jhvonsky theme
Yelly
Brilho da Grama
Fumei Theme Yay
na2co3 Light Theme
Chennai Super Kings
DarkVs
coral
cierra theme
Jiesi Theme
Gvalley
Maple Code Theme
pastel-zeke
9gu-vs-theme
vsmint
code aur chai - Raja
zer0 theme
codebabel-theme-dpk
Devell theme
ElemkahTheme
DarkEmerald
m_like_for_R
Nightly (vsc-theme)
Tema05's color theme
Dark Smart
Midnight Ray Theme
Mike Sources Theme Pack
zathura
lexer-dark-theme2
my blue custom theme
Broken Moon
OneThing VS Code theme
Valkthor Dark theme
Illusion Theme
izyanz-themes
dark-my-code
zainatheme
Blutzz-Dark-Pro
cuo-theme
Hijiri theme
Oxford Dark
Muslim theme
Radu theme
Babilawi's Theme
Flatland
Flattown Theme
czc light
Menon Dark
Nella
DarkOranges
Alex Over One
:D
dark plus midnight moon
Mitsuri
Squirrelsong Dark Theme
Dark Alma
ccotton
Ryanabx Classic Light Theme
WinterCell
hauseyo-style
Shiva
cb28
Night Wind Theme
C1 Theme
Ingeni
MVS Dark Theme(updated)
Disaster Theme
Lightest Light Theme
Dagger Theme
GreenBreeze-Dark
EMX Coding Theme
EP Dark Storm
Tori Color
Age Of Dark Empires Theme
ColorMaster
Gyt Theme
Stygian
Accents Dark
MintTea
EMIDarkTheme
Sankiedark
RAD_Theme
betweenTTs Dark
BeePro
Subnautica Theme
atheme
Bruhspicy Dark Theme
Foco Light
ActuallyDark
Monassa
clorest
test
term-world
dewart-color-themes
Svart Dark Theme
simplest
Kibou
syncrify eyes
Baculum Color
theme-familiar
shxmxn-theme
Pocketlocker Letter Dark
SheepSheepTheme
DiZTheme
mode theme
Icy Dark
Gorfius Peace-Forest
c.e.o
crallencode
campbell
Mantis
Rito Dark
Outdoors
Zenon theme
orange in black by jalal
JCEla
Roots theme
Solarized Mocha
Kirei
wscode
OhItsTom Theme
firdo-theme
tail-theme
Barrier-Free Theme
NiceNice Dark
Back to school Dark Theme
Prosperity Theme
ryder-fk-theme
Larrea Dark
Neon Side
Crafty Theme Dark
Salvagnini DN theme
Zen for vscode
CodingWish Dark
Base16 Circus Dark
Purple Coding
TitenQ Theme
KRYP70N1K dark theme
PIERRE THEME 1
hp-theme
DiZTheme v3 dark
amora
Accord
Ardhur Dark
Horus
carlostheme
motheme
Sneccskwad Dark
enlightened-ifission
Indigo Dream
Lavinia Dark Theme
Phenoscript
HA Theme
yalpha dark eris
Chandellier Theme
ratovia
TheBestColorExtensionAlive
Suite Seven
Reddish Color Theme
YADT (Yet Another Dark Theme)
Edit Plus Dark
thwack
darkness_of_my_soul
Dark Enhanced
Modern Make Theme
VSThemes
MinimalBlue
iDrive Dark
DarkBluishBlue
mTheme Dark
XL Dark
Dark Theme First
Spurlys Official
ergonomic-theme
Epsilon
Sakura Solitude by Matthieu Delis
Burb Lite
Dark Fresh
rmondesilva
Modern Dracula Theme
serjrd-theme
AndBetterSyntax
MyDarkTheme
PhoenixMates - Professional Dark
BW Theme
SunsetDay
onelivesleft
Exact Github Theme
just a theme
BYTES Genesis Theme
yush dark
Dream IceCream
myMonokai
ArcTerm
Custom Purple
mallchel light theme
AirTheme Pro V2
Bruta
Side Punk Theme
flatui-immersed
Monokai22
LBTheme
Midnight Shadow
Modest
Senku
clipper WX
Jessusrdev Dark Theme
Ultimate Swag Theme
Grey Light+ Pro
Ugg Theme
Subtraction Theme
destroyers-dark-theme
acceptance
Snowpiercer
Darkly Theme
Osean Boy
Tea House Theme
BlueFallTheme
Lightsky Theme
CT
EyeGuard
HenriqZimer Dark
jojobii-vscode-colors
mdfaisal.tealcrow
Klukvin theme
IDX Dark Theme
purple haze
pleiades
ETestIcon
One Dark Pro Modified to Match a Speci
Aoi Sekai
Dark Halloween
turquesa
Stellar Darknight
David Dark Code
Idx theme
Serika Dark Flat
Esmile Theme
themello
Czar Dark
Sci::Dark
Clairvoyance Theme
spark-dark
Mago Custom Theme
Ship X
Daaark-theme
PurpleVibes
GreenUI
Brett's Theme
CooperTheme
Rex Dark Theme
CustomDark
CommentsAreImportant
Neon VerdigrisPlus Theme
MinimalistDark
Johntor Dark Theme
narunika
harasta-theme
Boola Theme
Lapres theme
AlecVision Themes
Shuvo Deb Color Theme V-0.2
monster-theme
Corgi-Dark-Pastel
dev.tachgurbanov
Homogenus
xLumielVSCodeTheme
Rec Room
love-and-peace
Crimson Kimono
NK Theme
Grandonk Dark
Ampelopsis
nniiccee-vscode-theme
Atelier Forest Serene
chocomint theme
1818 Theme
quellcode26
enova
MyPurpleTheme
jummiterm-vscode
ConsoleCast Theme
Dark++ Color Theme
AP
Yuyuko Color Theme(Light)
Eco Theme
Plane
Rhuave Dark
Mil Theme
Faith Theme
Code Punks
Mirasaki Theme Dark
PSM Theme
Violet's Theme
Tia Chrismas Dark
staPle Dark
TekSavvyThemeOne
oneome-theme
DP Dark
McYoda's Light Theme
taotao-color
Impaction Theme
shiba theme
Kalopsia
Melinoë
bali-theme
Ange Theme
TheDarkerOne
T4Z
Bright Blue Arber474
Vip Coder Dark
Modernity Purple
October Eve
Raouf's Theme
Aabhi Official
Blue-Blinku
MyCode Dark

Steel Phantom
GrayScale301
Code33
Gedit Oblivion x Atom One Light
moqam
Harindulk Dark
arunika
Headache
Genius Genius Dark
Monokai Purple
yutani
Purple Nights
Eden
Economy Dark
Dva
Haube Theme Library
Night Vida
Bourbon
Night Panther
W0lfTheme
youtube-color-theme
Functional-ian-brown
Attention Dark Theme
Mountains and Rivers Theme
ThriveDigital
humanistic-theme
Lascavo Theme
One Dark Paradox Theme
dheera theme dark
Snow Crash
One Dark Minimal
GoosePlusPlus
Kesnel Theme
WebAlone Theme
Janis Dark Theme
minvs
Vasses TriColor Theme
DizTheme v2
flyingsonu dark theme
HiredToCodeTheme
Perfect Dark Mode
vscode-happy-theme
MVRT Theme
Swicly
aelmouny11_theme
Evening Delight
Dead Dead Dead
stealth-trendy
agenomicsphd-light
demoNewTheme
Sadja Theme
wst-theme
One Dark Variant
SD_DEEP_PURPLE_THEME
ts-connoisseur
ProgrammingFire Theme
fcode-theme
NutikasDarkTheme
ForestCode
satsoomahhh-theme
Chevalier
nextportfolio theme
VSCodeSpace
Jokic
hiru dark
Sober code
TopThemeX
Luan Theme
DARTH-THEME
chibas-godlike-theme
The Nandolorian Dark Theme
Loki-vscode-theme
bazenga
Jaci Official
wkaa-railgun
SeeNothingTheme
black-must
vs 2019 dark flat
Green Citrus
Silky Theme
Estelle's Garden Theme
Hyena Squad
Cherr Sepia Theme
codeGenesis Dark Theme
MaterialU VSCode Theme
dev-theme
Rik
Demon-Hunter
Vi-theme
orangelemon
Call Me Green
Rimber
iDoSpaceTheme
laranja
Blueprint Theme
Purple Ocean
Ivytag Theme
Yet Another Dracula
Nyx team theme
SpringTheme :)
jonas-design
ABOC
DevEzro Neon
NghiemDTheme
Rhubarb
Tsuru
Gabi Dark
yuki-code-theme
Rendom Dark theme
MonokaiByAkos
Ravenclaw
Teal It
Penunggang Beruang - Dark Theme
Wye
Batsignal 2.0
Knight
Electric Ice Theme
Tamis Dark
Deenoo
Barrage Theme
Beach Vibes
Boba theme
Nightly Inspiration Dark
Patife
VS Overhaul Dark+
azimov9
codaz codex
Lighting Vally
Amethyst Dreams
Yukari Color Theme(Light)
hasnum@random-theme
Minute Bugrits
Styldev
San Fransisco Dark
NTL Themes
Royal Treatment
Undefined-Roleplay.de
Famous Dev Theme
[Deprecated] GitHub Sharp Theme OG
vscode-theme-otto
patr-theme
TechStudent10 Color Theme
Eskey Theme
Cats Purple
fromis day
UnBlueLievable!
Canis Theme
dark-navy-theme
bTheme
Anatolia Theme
# mitfarbe Theme
Pkit VScode dark theme
ThinkStorm
Itay-Desing
codebabel-theme-dyk
Symb1ote Theme
Colors of BJJ
Brick Shinkuro
Dark Harden
Dark Jazz
Perfect Dark
Altrazen Theme
tuscan
GuerreroCustomTheme
ghostlouise
MnTheme
mulder-theme
SouravMullick Theme
DarkanTheme
Eerie
clownfish
code-theme
NETTSI Monokai Dark
Rotorua Dark
ATypical Dark
Material Fork Color Theme
Starry Night
iColors
Mantis
Starless night
nya
Lor
Niwi Dark Theme
Mojito Theme Collection
bobojojo
Purple Panic
Kazap Dark Theme
sylar-dark-theme
threedark
Oceanic-Depths
Bertram Theme
Material Teal
Mariana by Ryan
thinh-dark-theme
Artizon Theme
yoobdev theme pack
Lavender Code Theme
Hi Theme
AR Programming Language Theme
themeIndexx
Programmer
yingzi
Illas
Shades and Stroke
Lavinder
Dark Coder Theme
Snickerdoodle Light
Gumalix Tecnologia Theme
Jungle Green
Valentine's Day
GazelleThemeSet
Perfect Dark Pro
SofiDev
jb Autumn
SaloMonK Dark
CootTheme
flow-better-theme
CoolTheme
ItCase-Solarized
sanctuary
codebabel-theme-drk
alive-dark-theme
little-theme-dark
ohmyker0olos
TenDark
Haze Theme v2
Tungsten Carbide Theme
Midnight Theme
Sortium Dark
Cobalt-liquid
mi-extension
Dark Cool Theme
seekode theme
Obelysk
RLabs Theme Collection
z1h
Nebula 0.2
Running Wires
light-rider-mod
Skippering Theme
calm-down-theme
Atwood Dark
con-trashed
Sypher Blue
Woods.Theme
LayonUno
Saz Theme
dario97a Custom Theme
vstheme
Material Dark Pro
Programm dark
adnancodes
Temporal Theme
um_tema_la
Pristine Palette
inTheDark
Val's Grimoire
estheme - esthe's One Dark Pro mod
Blackout Theme
DarkGolden
my-dark-theme-byneto
ezgiturali-halloween
NevsThemeGen
Tidal Dark
trxBlues
AviPurl
youtube-demo-theme
Aman Vscode Dark
KodingWithKhara - Theme
bat-theme
TechGuys
Oh! Theme
Faded Prism Theme
ZednosTheme
One Dark Darker Theme
Theme-Mang Inasal
macro11-syntax-vscode
Maxsumon
MT 8008 Inspired Flat Theme
afoalb theme
Darkula Theme
MaceWindu Theme
Thiend theme
Donatello Dark Theme
Sublime Text 3
Queen-theme
Dark Angel
Crimson Theme VS
Vincet Dark Theme
Cinnamonroll | LIGHT THEME
Dekirisu's Rust Themes
arijit/dark
jam-dark
Vitesse Dark Custom
light-pink-life
Dark with blue
Brew VSCode Theme
Deepup Themes
Magicorp | DARK THEME
DFN2023Theme
The Eclipse
SLab Theme : Special Edition
Swift Blue Color Theme
Monokai Rain
Bashful Theme
Burger King Theme
Monokai Darker Soda
isahAn Dark
Herb's Themes
Chalkboard for vscode
theme-pythonautas
Rhysand Dark
Abadon Color Theme
EZ Theme
Verdant Overture
c1m Theme
Taquito Dark Pro
Becker Theme
NotNull Theme
rypjaws
Anúbis Theme
cbtheme2
Stream-Bay
custom_dark_theme
Nebulous
Xanthron Light Theme
dva-theme
Flobins Theme
FabCodeX
mother stretch my hands
Tarren's Theme Collection
SS-Dark
bruno's wet dream
Deku-theme
Groklike (Unofficial)
colora-theme
DarkerBlack
C0V3R
topo-chico
No Frills
Scenikus Theme Color
AsianZesus Dark
gekkou-theme
Rubyist
mikestheme
Doxa Code Theme
SummerRelax
Enigma Theme
bun_gothic
Blvck Theme
Iron-man-theme
SLab Theme
saturn-darkblue
The Linker for Coders
Snapx Dark
dave test 01
Neptune Theme
Obstgarten Theme
Revellion Theme
Dark+ One Monokai Simple Orange-colo
MinCom Theme
Tarek Dark
j-cardenas
Angelo Buse Dark
Cobeal
liamsc.net dark
Underdark v2
Coral Reef Theme
darkio
Kin VSCode Theme
KKN Color Theme
Purble
Deckx Theme
Relixir Autumn Dark
summer-dark-theme
VST
Rhapsody Theme
Deep Ocean :3
Highflyer theme
NerdyDark
Pistachio Madness
Boundless
VS Hub
Swapnil Prakash Vscode Theme
Touch Grass
Aconitum
Orange Dawn
HypeBeast
MaterialBlack
Jungle Scenario
Fae Color Theme
BOB Theme
luxark
Sants-Dark-Theme
Slime TM theme in BN edition
J Theme
Feliz Dark
Carolark Theme
Huynh Electron Theme
whisperinghues
mytheme
Giants Theme
Mictlán
wander theme
PrintNightMare
Blue.JK
Bedtime Dark
Ket's Theme Pack
Rook Theme
yang-theme
SHIP-AI
Budgies Theme
Monokai Saz
Popping and Locking Mod
Oceana
Kiwisoup
Like Material Theme
Calculator style
GOrange Dark
MonokaiSGQ
yogokai
kodex
Vibrant Spectrum
ZBRA Theme
jessebot-theme
duckcash
Comfort Dark Theme
Melanistic
vscode-xanido-theme
Xenon Xenon
Frigid Theme
50 Waizhuan
CodeLuna Theme
Code-Dadi Dark
Dark Theme VSCode
Blue Jeans
Blah
cfl-one
Sparkl
mritdev basic themes extension
Timir
Chocolate Chip
Bubba Kush Theme
One Mojave Dark+ Edit
Max's theme idk
CodeBabel theme ObscureRed
Vantablack
Qawafel Theme Official
Valstrom Theme
Coucou Theme
epok color theme
Light and Dark Theme Abc.
Gauss Theme
DevJam Dark
DarvyPro
Lumos Theme
Askfree Theme
spotless
cr-theme-night
ProCode Dark
hjqTheme
Delta Flat
Gunmetal Code Pro
Sadhu
Galactic Colored Theme
Earthy Light
VFD
OhMyTheme
Dyno Dark Theme
Hawks
ProtonCoding Theme
Finger Paint Set AYMS
yoth-theme-dark
gll theSherwood Forest
Monokai Custom Theme
Elysean-Theme
Pinky Accent Dark Theme
Azu Mono Dark
Unibody
Celbux Endpoints Theme
Azure Chill Theme
Maficious Theme
Stereo Theme
Martial Theme
Kurai Theme
Greeni
Sissi_Ga
Primed
RLabs color theme
Delightful Theme
Mrlonee Theme
Tabycord
demo-theme
GoodMonokai
AchilleStH_DarkTheme
Soothing color theme
Gatot Kaca
FW color theme
Erin's Theme
dulce-vscode
Slate TM theme adjusted by BN
Jfna
Sherwood Forest
Darkuto Dark Theme
fdis dark
AlphaUnstoppable Theme
Maya Template
db
Ready Academy
Dark Black Theme
Meteora
Shizukiii Sakura
VSCode Tokyo Theme
eu.vscode
Klesti
VU Theme
Angular Language Service
Silmë
MonoFocus
Alignment theme
Pirate Flat Theme
Pico-8 Theme
Avocado Theme
Anakin Them
wine-bar-monokai
Siirro Light
Prizm
Christmas Night
Throne
HashiCorp Terraform
Assisted Innovations Dark
Dessert
Asserec
Ocean Shark
huck-theme
Evondev Minimal Theme
MyCustomTheme
vulpotheme
WillyTheme
Oldschool Theme: Windows 95 Edition
A Nice Theme
Ayu Monokai
Muted Earth Theme
neumatter theme
Hardwired: Dark Neon Theme
KaioKenKolors
flexoki-dark
flexoki-light
JanderTheme
Starless Monokai
emerald-kc
Dark Plus Purple Moon theme
Stars and Stripes
sjsepan.summerish
sjsepan.coffeeish
Midnight Codex
Disasm required
Deep Burgundy Theme
VaznGreen
Focus One Dark Theme
most cool theme
Darko
Easter
Iditarod
Oxygen
Royal Code
POTET
abstract-elit
Pocketlocker letter light
Tenzer
Dark Studio
Daisy Theme
KineticTheme
Sadja Theme 2
OneSeti
Monokai (Wujidadi cover)
fresh-theme
Llamatech Color Theme
Galaxia
Gruvbox X
calm-dark
Mystico - Plum
Sacramento Theme
No Syntax Highlighting
Yume Theme
haeil
minimalistic-dark-theme
Neptunium Themes
twitch_slack_theme
zzhtheme
ians-theme
themes by gungorn
gbarkhatov
Kathund's crack
Oblong
drewh-wolf-theme
Dark Nads
redwhite
tnnmigga-dark
Margarita Theme
Darko II
Symmetric Dark
Otter Darkness
stylta material dark
Malte Dark Theme
WAADC Theme
Developer Mahedi
Demon Royal Dark
rick mountain
dark color theme
ClassicBlue
Galactica Theme
SyntaxTheme
MillenialPinkTheme
arcbjorn-vscode-theme
Night One Dark
J Balvin Theme
Paraíso (dark)
Mtheme
Royalty Theme
Always Half-Full
Mystico - Purples
blueshades
coder-dark-pro-james
semicolon.py
uTheme
Material Ocean
Evoratec Theme Dark
The Dark Stove
JanGloom
MACH1 Theme
PrismaPixel Studios
MJ Dark
Darkalicious Theme
MVS Light
Technica Color Theme
Carbon Dark
Dracula Darker Custom
plus-pro
jdh-vimrc
Astraeus Theme
CodeNature
Artivain theme
yu-os-theme
Bubster Light Theme
Result Materia
yn-theme
Autobiography
Monokai Dark Theme
JoKenPo Theme
lei-theme
daksh-desai-alientheme
true-dark
darkspace
gCode Theme
Apatite Dark
bali crepuscule
theme f u n d a t i o n
October Theme
shiina theme
Calm theme from BN
Spark117 Light Theme
bakrimoharram
pen-penguin-theme
Oreen
Dark Khan
Ruby Planet
Lightning Theme
candyrush
colorato-theme
Bzt Dark
McBurger
Resep Theme
Kiiro Theme
Covcg Theme
Fuyuko Mayuzumi color theme
Splash Theme
Equilibrium
Michi Material Theme
ShadowKai Light
Big Impact Theme
tenjo-theme
LearnJs Theme
Nebula V2
mlt
Personal Dark Material
Kg Dark Theme
sjsepan.forestish
Liftoff Theme
The Only Tolerable Light Theme
Witch Elaina Color Theme
Leo's dark
Dark Modern Pro
fallout 2@77
vscode-theme-varlet
Light Lite
amazonFrog
Darcula Oxidized
tian-green
King-theme
Dapanna Theme
Cool Epic Theme
NaamJyut
DevCode Dark Pumpkin
Lulu Theme
Beatsoft Theme
Kesteren theme
NeonCactus
Lilac-theme
barn-cat
Alpha
Bytecare Theme
yellow-beryl-theme
Neki
Theme - Akrozia Light
Vue Feefoolec
basso.mkv Theme
LokiLP66 Dark
isJDongYa Dark
darking-horse
devHub-dark
Perry
Theme-it theme for vsc
Smithy Official
Raen theme
Greip Theme
DeepSea - Dark
NovaTheme Dark
CyanoBact
bellissimo
ChrisCoding
Ceramic
Dark+3
harricHe-theme
Creative Coder Dark
tigran-code-theme
Brilliant Editor (Dark)
wally candy dark theme
Betu-dark-theme
Underworld
two-firewatch
Osmium Theme
TWBoom Dark
Professional Dark
BorisTheme
Pure Dark
Daydreamer
ourthemecolor
Chronokai
4am
Hello Darkness
yyyy
Pain Theme
theme-will
VS Overhaul
Menelik
SimpleLight Theme
Young Colors
OpenSpace Theme
Dopatheme
QAValue Color Theme
One Simplified
Blanksheet Theme
CupCake Theme
Tropical Electricity
KapoorSpecial
godot-vscode-theme
Deot Theme
THATheme
Universo
All Blue
KeiserThemeVSCode
Themes To Die For
Purple Night TLJ
Kidz Theme
asc minty
RRR (Requirements, Refactoring, Robustn
Haramosh theme
Troop Theme
Snow snowman theme
Quicky Dark Theme
Tactacam VSCode Theme
Sith Theme ByRod
DgtalHeroes Theme
Greendarktheme
TDish
F333 Colors
rayuk
Morose Theme
Necer Theme
iceui-theme
Clarity Dark
Flixify Dark
hc-monokai-p404
feizhen theme
Dark Forest by kisik
Boxy Yesterday
Jelosus Dark Theme
sammobadi
auri-theme
Gruvbox Material Mix
MKDeveloper Theme
darkNpink
Not So Dull
NyctibiusVI Theme
KJS Officials || ElectricLime
rtemis-dark
Simple Contrast Dark
Mana Theme
DarkPurpin
Sharpen Theme
Desertesque
Café Dark
pureBlack
Dashxe VSCode Theme
FA theme
Kraken theme
Coder30
Octo theme
clesy-theme-dark
chvrez-theme
sysop theme
Gamma
Sewayaki Dark
fire-theme
TheCoderGuru Darker
Coney Island
Monte Cristo
The BOSS Theme
Sovereign
Reheme
code-theme-dark
kalm
vs-code-that-dark
cppish
Stoked!
Super Orange Triad
Joogie Theme
OxTheme
Yurei Dark Theme
Whiteboard theme
MonoCraft
Dark Fantasy
Blueberry Pro
Italic Dark Theme
Matheu`s Dark Theme
Cool Night by iamsidar07
Still Rosy Theme
Annorum Flavum
DevoraBB
Jungle Pup
Wick
minimalTheme
Treux
Quality
code2bsmart darkpie
Capybara Drip
zzDarkTheme
tornado-themes
focusOnCoding
Color Pop Dark
Zenbook UX534FTC White-Gold V1
Darmadilla color theme
Coven Evelynn
vscode-theme-elaina
evan01
Rens Theme
CodeSkulptor3 Theme
Base Minor
Invincible
Darksome
Dark Purple+
wintry
orbDbz
Dark Toucan Theme
arcobow
Vega Dark
Raven Tech Theme
ComfyEyes Theme
superNova
Cherry Creamsicle
leon
Ruwer's Theme
Jupyter Dark Theme
VSCode Dark Theme by jeffersonfed
Scrumpy Dark
Glass Theme MacOS
AZTRO
someAItheme
archangel
Cyan-Theme
newa
tva theme
anthropocene
Dexios (color theme)
Railscasts Pro
Sebastian Codes Dracula
Zerrawy Theme
betu-light-theme
Nirostic Color Theme
colorsan
starry-code-theme
jg-coding-dark-theme
Hảo Theme
Paztels
TheCoderGuru Dark
Dark Forest
Orenj 2
Hades
Rithik's Cool Theme
Monoviz
Dark Sunset SM
Tahiti (Night)
Webcubic Theme
Elegance
Kaplan Dark
samacaca
Solan Gundersen Night
DevX Dark
Wawandco VScode Theme
Kuychi Dark Theme
Jaason VSCode Them
Marine Dark
Michael's Monokai-dimmed Remix
Arrpee Theme
TodePond-Theme
Haki Code
wadq light
Anka Theme Dark
LMAS dark theme
Touch of Pink
wl dark ink
Azathoth
stilla
Tealicious
Storm Color Theme
Oboro
DP Light
superduperultraultimatelegend Purp
Flattastic
DC Dark Theme
Capital Theme
Yura Theme
stellar
YD VSC Theme
Midwinter
mini-theme
ametista
Unofficial CF+ Theme
msun-dark
Code Theme Curiosity
Sober Plus
Douse Theme
ri.code: theme
Author's OLED Theme
DarianTheme
DarianTheme
Tehran
Dark Tenebrous
JSK Theme
Lazuli
Darkestside Theme
Theme-Y-Random
Dark Aviator
Wandering Mercenary
Illumination
Meditheme
bornpink
Sage Siren Theme
PrismPlus
lonus
ezra
vantablack
Nevada Color Theme
ptheme
Never Objectify any.Wamen
mlia
Hyle
Loafesque
web-developer-theme
hlbl-theme
Bloo
Imperial Themes
rellow
YairTheme
color364
JIRI THEME
glv dark
fantomas
Nerd Themes for VSC
Material-Pure-Dark
vity
TwilightSparkle
ClearView Dark Theme
neko
Q Club Dark Theme
fcc0ff
pc-blitch
dev-sharif
bhargava vscode theme
Focuss Theme
Alpine-Warm
Akuma Dark Theme
Milianor Theme
Rutillas Toby Theme
GitHub Modern Theme
Nocturnal Dark++
Konstant theme
Smooth Theme
Calming Dark
std
Hank VSCode Theme
Zeus Theme
Coconut Theme
TheRedCraft-Main
mnml
Fresh
ErikWDevTheme
MaldosDVRK Pro
Sandcastle
Actiew Dark Theme
Ramage
Seabed Theme
Proper Dark Theme
inferius
Jewel
Squeak
Orchid Theme
Kardf-Blue-Theme
MGMM
attica
Bharti Theme
Dantes
Green Yow
Goth-Rave
sagenfree
Wenyl's theme
rfldllql-theme
Darksel
Calypso
tragicpale
mamun417 theme
Jaden's Theme Dark
arber-theme
Go-Company-Theme
SlickDev Dark
Royal Knight
SyntaxFM
Black 'n Turquoise
Midnight Forest
Dark Nova
Macchiato Theme
Monokai Kai
ROG Theme V1 Dark
Greenish Color Theme
Purple_Theme
Monstera Theme
CurlyBraces Dark
OneZero Themes
Assisted Innovations Light Theme
Covert Slate Dark Theme
Waterbyte Theme
DD Purple
darker-hour
Monokaifu Theme
EnTheme
3Ducks Light Theme
Entropy
Dark+ Alt
CobaltTrue
dankula
styled components highlighter
RB Theme
LessBean Theme
jDarkMaterial
argprocolor
Sebostien Theme
Stellar Dark Material
Automation Theme
Holly
Royletron
citree
Solar Flare
Michi Modern Dark Red Theme
sokratic-theme
Kachanon's family crest
purple-frog-theme
Vanya's Theme
Sleipnir Theme
Northern-Lights
Sombrero-Sunny
Dark+ Blue Theme
My_Tonight
zzTheme
PHP Helper AI krstf
Smart Coder Dark
IceBerg Dark
Vue Focus Mode
Zemd Theme Dark
Gopher Theme Color
choiheza-purple-teal-dark
limpo
The Coder Dark
code/super-dark-mirella-theme
Triumph
Caribbean Candy - Dark
The Last Coder
Black and White Markup
4th-chill-night-theme
ROG Theme V1
SuperCluster Dark
newsprint
Code Mini Dark
GentleBlack
Filip's VSC Theme
ColorL
Umbra Theme
Rezaul360 Theme
Kinkade Dark Color Theme
kayhan-color-theme
90s-jazz
Pirate Dark
tesselate
midt
Lunar New Year
sature-dark-vscode-theme
lolo
BlueGreen
The Hat Maker
BlueyOneDark
Quake II Config Dark Theme
Deemphasize Syntax
francisCazzum
TheCodeDoctor
csr-blue
dreamer-theme
Dark Forestech Theme
Monokai Ryo
mier-color theme
customalabaster
ImperialTheme
green machine theme
Dalia Night Theme Monokai
KJS Officials || Urban Jungle
chalkish
QuinnoDarkness
Victory Theme
dark theme by ilex
KitkatDark
Intility Bifrost Theme
Endercat's Dark Theme
Rocket Clicks Code
Neon Dark
king Evans Dark Theme
barlume
Another Theme
Abtiga Dark Theme
One Dark (moded) ByDedicatedksta
Rose Syrup Dark
Naysayer
Unknown Dark
Cyzo
Nightcall
Pastelization
morita
MyIDEPlusTheme
Brickleberry
Baitul Hikmah Dark Theme
Clickable Button Hints
Fox Dark Color Theme
Code Dark Coder
Nightly Code
YANUS
tobytheme
Theme Mild Night Official
Econsult Theme
raijū theme
Botanical Theme
Koki Theme
Bam - Dark Theme
csTheme
Jayvir Rathi Theme
zboyle workbench theme
MobyDick
utopia
Orange Custom Theme
Melodark Theme
TeaForOne
Dyonic Theme
CodeSandbox dimmed
Super Coder Dark
Eternity-theme
FunCode
Born Dark
Materialistic Dark
Cockos Reaper JSFX Theme
Vidar
Requiem-Dark
Processing Pama1234 Color Theme
Darkfoo
Dracula programmiri edit
Gondolin Light
leftBrain Theme
nb monochrome
Fancy
T-Theme
EVANS
monokai-light
smooTheme
The Treeme
NihilLeaf-Theme
minimalist-theme
ThatDayTheme
Qara
matrixxx-theme
bananalight
Digital Theatre+ Theme
07 Theme
dark-brabo
Let There Be Light
BusbyVSCode
arch0n
xotocode-theme
Buu
Copilg Theme
Kurtsley Light
daru
VS Dark Theme
So Fresh And So Clean
Stealth
Robolaunch
Vapor Dreams
VSCode Paradise - nickesc
Cozy Night
Elvanos Theme
jabrms Color Theme
VSChill
Cerise Mod
aim-theme
Aurora - by tiny.
Kriszu VScode
Bourbacode
Beginner Theme
SoftDarkened
Toxic Night
Image-Theme
god-theme
Mighty Ducks
HLO HighLighter
Cosmosis Dark
dark-love-coder
morantor-theme
Modern Ant
Hulky
Maeel Theme
Purple Colony
tema1
SL color theme
manDark++
Dreamscape
DevHaven
SpaceBlack
Jiyath Dark Color Theme
Smooth Dark Theme
f-theme
Thomas♥
@xxtereshko/light
RetroTheme
KaziKame Dark
Atem
Blonder
LiangLiang One Monokai Theme
in his earthy elements
Cherrygum
Tema1
Indielayer Theme
Lionel
Spaghetti theme
valleys theme
p33t
Jestr Theme
Jartur
Galizur Theme
daru
vsc-theme
hassanoof
Sonoran
kPurple Theme
Dark and Cool Theme
moon-gray-light-plus-color-theme
Flexy Dark Pro
AidakTheme
color-theme-pretty
Heptico Theme
GMS2 theme
starry-code
Turquoise Foam Theme
Pure Black Plus
Burb Dark
N30 Baroque Theme
MacindowsVS
FUNtext Syntax Highlighting
jqq-test
Zenies Flat Dark Theme
Ook
enhanced-ui
mekeilia
Parfait Theme
ramazzotti
ayyour
new-sphere
theme-macaroni
heckpurple
AtomAx Theme
Diwali Theme by Yash
Biu Theme
dark-large-monitors-theme
JamesNZL
Magie
Oswald Dark
Pythonautas
Noctis WCAG
PurplePorschePheme
Broken-themes
Neon Programming theme
VSCode Nocturnal Dark
Ultimate DYEDB Theme
sjsepan.nevadaish
Midnight Handshake 2
Twilight Handshake
Ameixa Negra
Fiery Theme
apitoriadev-theme
forventone-theme
>_pvdk-theme
dejaypiii-theme
Green Handshake
DeadTheme
souzaa Dark(v4 purple)
Salatema
nacho2
Light Purples V1
shadesOfCoffe
owlify
SgtCoder Developer Coding Theme
thinkscript color theme
Malva
Royalty
souzaa Dark(v2)
CosmicBlush
Blacklight Theme
everything-zen
peaceful-mind

Sweet Caramel Theme
rumortheme

MonokaiX Theme
Lebannen Theme

Victorian ParadoxScript Tools
Too Much Pink

sjsepan.e-inkish
qnp theme

Olive Vibes
Poison Serpent

moe-codious
Blender mojo theme

4191 Theme
Demo Theme

Motion Blue
MGTheme

Dedezxx Theme
simplified solarized

Stentor
nulzo-storm

Narixius Theme
Monokai Ace

Cool Blue Theme
Black Hole Sphere Theme

viiv
Dev-Code-Theme

0xDEADBEEF
GoldenZo

Vitesse Theme (matijao)
Dark Flamingo

Blueberry Theme
Daniela's Dark Theme

Discord Code Block Theme
City Pop Shark

waktheme
Pro Coding

Dark Mint Theme

RailsCasts Theme
Tokyo Night Theme Damask Edition

Webstorm Darcula Dark Theme
Slytherin Green

Thzin
Gesper Theme

Papaja Theme

2077
Brant's Theme

Dark Extended
Harmony Pulse

Eva Darker
GlowMinerals

Maron Themes
Kite Theme WCAG

yuru black theme
Anakin Theme

Gruvbox Min

gta-6-theme
Webstorm Ui Theme For vscode

cave
Fleet Purple

To on no codigo
Semantic Noctis

Gojo Theme
Gruber darker (yellow)

IceBerg Dark II
Stella One Theme

Gotham Theme Black
Rosé Noctis

Intellij Classic
Unicode Purple Dark - Theme

Camsdono Studios Snippets
ruihuag

Dracula Pro
Theme Light IDAC

QuTheme
Pache dark theme

BRark Dark
assassin567

Pleasant Syntax
Herald College

another-vscode-zenburn-theme
Mugiwara Theme

LanderTech Theme
MuteNight

Jera Theme
solarized-min

Beans dim
vsvalight-theme

Kashmir
gandalf_the_vibrant

Spark117 Dark Theme
Ombre

MiDark
Blueprint 4

gon-dark-theme
Clube Gazeta do povo

NightSwell
Avestereye Color Theme

Fuedskeps Theme
Aeridia

VsCode-Theme-DevOps
In the night theme

mess
TM Themes

my_my
sonnys_code

DevkColoradd
dsng-theme

feta
hyped dark

Pro Dark Theme
draculabr

HansoMWarE Theme
chill-classic

Edvoy Coders
andrews theme

hoccacas
Prerity

Dark Flow
Lark Theme

Yotei Theme
Joba Dark Theme

codebabel-theme-dbk
HanaHana Light Theme

Purpule
Fresh Dark theme

Dark Flame Master
starlight-theme

Viernes
Guyn

lagom
Baaam

suncodepro
vilarezz-theme

Nightly
DarkTheme001

Anubis Dark Theme
Bred's Dark Theme

Breeze Theme
Arrale-Theme

Green Land Theme
Edignite-Ngo Theme

Senja Dark
Metzi

Darken World
DcDevTheme

FEA DARK THEME
Sombra

Astilla Light
Willi Style

Visualizeit Theme
KozmicBlues

nice theme2
monokai-dark

Azalais Dark
Taio

insider theme
mhfeizi theme

Shiva The Destroyer
NucleaR'z Theme Light

Tagvi's dark theme
Dark-Theme-By-Irfanul

Monokai Constrasted Colors
100 Thieves Dark Theme

BananaTheme
Karnov Theme

Darx Theme
Candy Blue

My Clean Theme
GretaVanFleet

Whisper Dark Pro
WhiteSur VS Code

Nope Theme
Blowtorch

Bearpip
Segmentation Fault

Seolki
Aztik Dark

ThemeNemo

Nouri
Material-last

vare
valar morghulis

cool purple theme
NeonGreen

LemonSong
MangoTango

MidNight Blue Color Theme
Ocean Theme

Bellair
HiC Color Theme

LunarVim Dark Theme


INTERNAL TEST (NOT FOR USE)
sound system
Hotline - Themes
TryCatch Theme
MatchaNeco
Vicious
Walls Theme Dark
Mate Theme
Day Shift Theme
Deep Dark Color
Rusty Colors
GPTheme
Another Colorscheme
Dark Plus Chocolate
Midnight Lights Theme
Ayu Modern
STARLIMS VS Code Extension
BlueBorn
Theme Ocean High Contrast Dark+
Themes ER
PulseByte Themes
Undefined Theme
Dark-github
webstorm
ICE CUBE
Project Dark Theme
EderandEder Dark
bat
ER-Ross
modified dark lavender
Semi dark theme in yellow
Ashokai
Winter Pearl Theme
SunSed
Pirulug Theme
Hotline - Icons
Flexoki Theme
Artoria Theme
SR Theme
Darth Buddha's Dank Theme
viida
Dullahan
Solracss theme
sjsepan.nightingaleish
variablz-dark
Blood Moon Theme
Dark Mode with red accent
Exa - Theme Pack
Monokai RainStorm
Flavia
Imon Hasan Theme
Cozy Coding
NightFall Glow
Paro Paro Solarized Dark
peepee-theme
Stellar Night
RoxoTema
vscode-pink-and-puple-theme
The Best Themes for programmers
Reddit-Dark-Theme-Unofficial
diVision Group Dark Theme
tsoding emacs theme
DhakaNights
Best White Theme
Plum Torte
Ya Theme
NightfallDev
sjsepan.solarish
Genshin Impact Theme
Orion
Amalgam Language
Oni Theme
Blackout Theme
One Dark Code
Yarn Theme
Angular.dev Theme
Star Code
patagum
Robianchini Theme
qRecca Theme
Night Operator Theme
Katzenbox
navy-pier
Toxic
Voraes
YAAST - Yet Another Atom Styled Theme
Dovolen toboy
ShadowFrost Theme
jngl
Alchemy Theme
Typora
Boavista
Ash Sharp
GruvBoxer
Ultra kai
One Monokai Dark Blue
Pulverize Verilog Support
amirreza-theme
Wolf Theme
JetBrains Fleet Dark
Zero-Two Vintage
Nexus Prisma
Jannchie Theme
Lizadro
Glacier Black
Kato
Midoriga

Entropy
Darkx
hybrid theme
Astolfo Color Theme
FT Palette
sjsepan.matrixish
Pastel Underwater Sun
sjsepan.greenish
Meow
Terrabyted Theme
sjsepan.purpleish
Ruby Theme Dark
sjsepan.reddish
BRO
Theme Scheduler
lucaslabs-py-icon-theme
pink-dark
IDL for VSCode
Galactic Glow
Fuior
Errors PeekABoo
Dusk
shades-of-buntu
Ocean Project Theme
theme-code
nachop theme
Wistful Theme
sjsepan.newspaperish
CAL-4700 Theme
DevcnColoradding
Amaral Dark
Lev
Trigger.dark
AXTRO
Sky Color
jeff dark
AddCol
E-Ink Theme
theme-dark
Asphalt Theme
bluered-theme
Draquinho
Brodie-Heat
harry harry dark
Ganymede
Felixoux-theme
Newspaper
ICONIC
Moonlight v2
Cappuccino's Theme
abcdef
Azerite Dark
Janus Light
Monokai Midnight
*Артель
One Dark Prime
mauvey
HCO
Xeindark Theme
sjsepan.blueish
sjsepan.limeish
MELFA-Basic
Hush Theme
sjsepan.humanedark
fly theme
PCode Theme
Purplexed Magic
FDCC Support
Emoji Programming Languages ​​Badges
Namucode
henriquedark
Walking-in-the-dark-RED
BlahajCode
BPalenight
medium-dark
Pitaya Theme
sjsepan.humanelike
eudoxa-color-theme
northeasthorizon
miniTheme
A theme
My Bright Light/Dark Colorful Theme
Nimbuslab Theme Color
cemong v2
Stitch
Brenofactheme
Date Highlighter
demephistopheles
anton dark pro
Hackjb
My cottagecore theme
Dark Matter
rondi-theme
Burb Lite
BenLaTheme
codebabel-theme-jok (Joke)
yunsgeektheme
Noob-dark
One more coffee
Pure Black Darkness Lover
abipravitheme
xptotest
One-Blue-Pro
Giza
godot-vscode-theme
JetBrains Fleet Dark
Night Owl Colorful HTML
vin theme
Plurality Pop (Dark) Theme
Gray Matters
Yaml Script
Souna-Theme
Scepter Theme
Ulti-Theme
Adelirio Dark
Talles Dark
astronaut
NiksTheme'33
std::format placeholder highlighter
felipaotema
Drocktwisk
SpectraCode
Emperor Zurg Dark
shada-theme
Loworem Ipsuwum
ricc-theme
unlighted
OneBluePro
cherry-theme-switcher
KAGSA
mycolor
Joao-Dark
syrinx
diamantine-theme
darkThy
Hardmode
teste123688
baracadabra
gloom-theme
ShovonDark
colorName
Z1nyrx Dark
Centigrade VSCode Essentials
GuilhermeDev
4nil
VeksMyTheme
test11
TomTheme
My Custom Theme
Kaha theme
⚪◌⚪◌⚪◌⚪◌⚪◌⚪◌⚪

## Іконки для VSCode

* Minimal (Visual Studio Code)
* Seti (Visual Studio Code)
* RuneScape Icon Theme
* Cold Python Theme
* ExplorerFlags
* LunarSync
* Deepdark Material Theme - Full Black VS 2022
* Styledwind
* Minific
* Kamira for VS Code
* Duck Themes
* Gooniez
* Cosmic Wave
* DimasLight
* pescript lang theme
* silverwind-theme
* Themeless
* DEV2DEV Theme
* Dr. Reinão Icons
* kary mix
* odin-sundry
* Fayrouz icons & themes
* UniGeCode
* Cait
* microhouse
* Exa - Icon Pack
* likablehair
* Lhama
* digitalspacestdio.darkness
* Feynuus Theme
* DarkRoy
* KEEN
* TimeSavior
* AyuPaku Cosmos
* Slick Icon Theme
* hybrid icons
* TSR - LSP
* Symbols Beta
* vscode-sapphicons
* Rosé Pine Burnt
* God pen
* Foretag Development Toolkit
* SM's Dobri Next - Themes and Icons
* Caracal Dark
* Siberia
* Personal Customizations
* Outro Theme
* Ninian's Icons
* XON
* orange coral with ico
* JanisCommerce
* STNE Script Support for Visual Studio Code
* Gabri-Icon-Theme
* Minr Scripts
* EXA Minimal Fileicons
* Nora Cora
* seti-icons-extended
* otone
* Palestrinha Icons
* Linguagem Egua
* Sinae Unicornis
* krypton
* Dimi Theme
* Paula's Clean
* Ufuk Bakan Color Themes
* VSBBC
* AMXXPawn vscode
* Rockwell ThingWorx DSL
* Material Icon Theme+
* Stardew Valley Icon Theme
* theme-it icons for vsc
* Fira Code and Material Icon Theme
* Salad Icons
* SMotaal's Frictionless Theme
* SourcePawn
* OpenSpace Icons
* NoHaxito Icons
* cts文件图标主题
* Steamy Fish
* Material Icon Theme simon
* Monokai Black
* Gray Matter
* PlaneMinds Icon Theme
* CafeOBJ
* Void CSS
* Seti Desaturated #666
* Bamboo UI
* Guezwhoz Theme
* MaterialIconic
* Int UI Theme
* IDEA-Icons-To-Maven-For-VSCode
* Aut Icons
* Demon Theme
* Jigx Builder
* SparkyTheme
* Vanilla 2.1 Icons (For Roblox/Rojo projects)
* bett3r icons
* Make Things and Smile Icons
* Twemoji Icon Theme
* mozart409-icons
* Hypernym Icons
* Solarized High Contrast Light
* Snaz
* Cleo
* Cars Icon Theme
* Universum Theme
* S Mc Dark Theme
* Lit
* Light icons
* R Dark Pro
* Bromium Icons
* Otanyan Icons
* Urban Lights Icons
* Clear theme
* Eclipse theme by Solrike
* MaterialIconic Product Icons
* Made With Mouse Theme
* dawn2
* Mosmmy Theme
* minimal-notes
* sr-file-icon-theme
* Not official!!! Office Viewer(Touchlab-only-build)
* SFCC Beaver - Icons
* Adam
* Lain Icon Theme
* Una's Theme
* Icy Icons
* Octicons
* @VSEyes
* White Material Icon Theme
* Roblox Development Icon Theme (New Icons)
* Sundown Theme
* Bedrock Addon Icons
* Roll20 Macros
* Agila
* VisionTech ThemesPack
* Geal Theme
* Argon
* Smoothy 7
* Magik Smallworld
* r-icons
* Personal Theme
* Simvolio support
* Dusk
* Recore
* Salt Water Taffy
* Sherumite
* Animal Crossing Icon Theme For Ruby on Rails
* Theodora
* ftHTML Language Support
* Carbon Product Icons (tweaked)
* Sober
* Mastro NANDo
* MK Icons
* Cosmic Barf
* Nand2Tetris IDE
* POP! Icon Theme
* Dolch Theme
* FakeDonald's Icon
* Nonicons
* Flutter Icons
* Age Of Empires II AI Scripting Support
* Magpix
* Turbo Ayu
* JW Icons
* Bravato Icons
* Aesthetic
* Vinala Extension
* Textmate
* BGforge MLS
* Monokai Pro New KC
* Gruvbox Icon Theme
* Calm Days, Sober Nights
* Neofloss
* Ghibli Icon Theme
* Framework Icons
* Shark Theme
* Awoo
* GitHub Icons
* dot-icons
* Catppuccin vscode theme
* Ayu-Warm
* Catppuccin Perfect Icons
* @EyesTheme
* Mnemo'n'Icons theme
* Nomo Dark Icon Theme Extended
* Robusta
* TextPad Icon Theme
* Zarp Icons
* JS Nomo
* Hadar Theme
* Material Icon Theme (lit fork)
* Minimal Icons
* Warm Light Theme
* Unofficial Material Icon Theme
* PVS
* Imandra Protocol Language
* Seti folder
* Atom Font Icons
* Smallworld Magik
* Mario Theme
* Awesome icons
* AT&T i386 IA32 UIUC ECE391 GCC Highlighting
* nornj-highlight
* Minimal Icon Theme
* op-1
* Datapack Icons
* File Icon Theme
* NightCode
* One Dark Modern
* Nand2Tetris
* Advpl Theme
* High Contrast Icons
* FxDK/FXCode Dark theme
* Animal Crossing VSCode Icon Theme
* PureBasic Language
* Akonio-Material-Theme
* Black Ops Theme
* Roblox Studio Icon Theme
* flippidippi theme
* Sharp Icons
* Fira Code Nerd Font / Icons
* Celestial Magic Girl Icon Theme
* Ayu Green
* Redberry Icons
* Wobbly Icon Theme
* Quills
* Blue Phoenix
* theme-demo
* Eye Care Themes
* mpx
* Fania Icons
* Simple Icons
* Enki
* Smile Theme
* JetBrains Icons Enhanced
* Daybreak
* JetBrains Icons
* Unfancy file icon
* Cage Icons
* Windows NT
* SourcePawn
* Stardew Icon Theme
* Windows XP
* C# Easy
* Simple icons
* VS One Dark Theme
* SC themes + icons pack
* Aliyun Serverless
* Monokai Seti
* Material Theme Italicize
* sf-symbols
* Mosmmy Icons
* Kamekazi Dark
* Nomo Dark macOS Icon Theme
* JetBrains IDEA Icons
* GLua Enhanced (Garry's Mod/Gmod Lua)
* Quill Icons
* Visual Studio classic icons
* Viking Icon Theme
* Default Dark+ Contrast
* Atom Material Icons
* Aramok's Black
* Atom Icons
* Webstorm Icon Theme
* Material Color
* Catppuccin Icons for VSCode
* Serendipity
* Gruvbox Material Icon Theme
* Moxer Icons
* Advpl
* Keen neutral icon theme
* Dark Mode
* Seedling Icon Theme
* Emoji File Icons
* file-icons-mac
* Chalice Icon Theme
* Try's Icon Pack
* Sweet vscode Icons
* JetBrains Icon Theme
* Origamid Next
* macOS Classic
* Developer's theme
* Verdandi Theme
* Rosé Pine
* TOTVS Developer Studio for VSCode (AdvPL, TLPP e 4
* Symbols
* File & Folder Icons
* Field Lights Theme
* City Lights Icon package
* Kary Pro Colors
* 微信小程序开发工具
* openHAB
* MacOS Modern Theme
* seti-icons
* -ч-
* Helium Icon Theme
* Mayukai Theme
* vscode-icons-mac
* Deepdark Material Theme
* Dobri Next - Themes and Icons
* Icons
* Easy icon theme
* VSCode simpler Icons with Angular
* Office Viewer(Markdown Editor)
* file-icons
* VSCode Great Icons
* Ayu
* Monokai Pro
* vscode-icons
* FRC Icon Pack
* Cheesewaffle's File Icon
Vanilla3 Icons
Custom App Icons
Good Icons
Seti + Folder
Sakai Icons
Macish Icons

Gruvbox Material Icons
Alex Gudkov Themes
Sakai Theme
Power Platform Tools
Material Theme Icons
Divalto Studio
Primer Light
Mizu Icons
Bearded Icons
JetBrains Product Icons
Dev Tools
Design Líquido - Linguagens em Português
BlackOcean Icons
Material Icon Theme Custom

NLP
PCode Icon
meme.js Icons
topmodel
MoreCross Tools

