# DEEPL



## Розділ &#127991; Інтернет &#127758;

>&#128161; Це перекладацький сервіс, який надає точні переклади для приватних осіб і команд. Він підтримує 31 мову і може перекладати файли PDF, DOCX і PPTX..

- **версія програми &#128230;**: :two: :three: . :nine: . :two: . :one: :zero: :nine: :zero: :five:
- **розробник &#127970;**: Jarosław Kutyłowski
- **офіційний сайт**: [посилання &#128279;](https://www.deepl.com/translator)

## Операційні Cистеми

>&#128161; Може використовуватися у будь-якій операційній системи у браузері.

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/deepl-translate/XPDNX7G06BLH2G?hl=uk-ua&gl=UA)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://apps.microsoft.com/detail/deepl-translate/XPDNX7G06BLH2G?hl=uk-ua&gl=UA)
- &#128229; з Хмарного сховища &#9729;

