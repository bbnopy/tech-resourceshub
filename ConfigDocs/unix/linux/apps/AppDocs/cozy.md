# COZY



## Розділ &#127991; Офіс &#128209;

>&#128161; Cучасний програвач аудіокниг для Linux.

- **версія програми &#128230;**: :one: . :two: . :one:
- **розробник &#128422;**: Julian Geywitz
- **офіційний сайт**: [посилання &#128279;](https://cozy.sh/)

## Операційні Cистеми

### Linux &#128039;

- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: ```flatpak install flathub com.github.geigi.cozy```
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: ```sudo snap install cozy```
  - **версія програми &#128230;** latest candidate команда для терміналу: ```sudo snap install cozy -candidate```
  - **версія програми &#128230;** latest beta команда для терміналу: ```sudo snap install cozy -beta```
  - **версія програми &#128230;** latest edge команда для терміналу:```sudo snap install cozy --edge```

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**
