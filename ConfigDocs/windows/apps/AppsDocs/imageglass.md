# IMAGEGLASS


> :information_source: **Інформація:** Це безкоштовна, легка :package: програма для перегляду зображень з відкритим вихідним кодом, яка надає користувачам інтуїтивно зрозумілий спосіб перегляду зображень.

***
:open_file_folder: **Розділ** :framed_picture: *Графіка*
***

## Про ImageGlass

| Пункт | Інформація |
| -------------- | --------------- |
| **версія** :package: **програми** | :nine: . :one: . :eight: . :seven: :two: :three: |
| :computer: **розробник** | Dương Diệu Pháp |
| **сайт** | [:link: посилання](https://imageglass.org/) |

## Встановлення

### :desktop_computer: Windows

- :inbox_tray: Завантажити з сайту за [:link: посиланням](https://imageglass.org/release/imageglass-9-1-8-723-55)
- :inbox_tray: Завантаження з :cloud: Хмарного сховища

