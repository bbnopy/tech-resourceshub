# INKSCAPE



## Розділ &#127991; Графіка &#128443;

>&#128161; Вільний редактор векторної графіки з можливостями, подібними до можливостей Illustrator, Freehand, CorelDraw, або Xara Xtreme.

- **версія програми &#128230;**: :one: . :three:
- **розробник &#128422;**: Inkscape team
- **офіційний сайт**: [посилання &#128279;](https://inkscape.org)

## Операційні Системи

### Windows &#128421;

- &#128189; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/store/detail/inkscape/9PD9BHGLFC7H)
- &#128229; з офіційного сайту за [посиланням &#128279;](https://inkscape.org/release/inkscape-1.2.2/windows/64-bit/msi/?redirected=1)
- &#128229; з Хмарного сховища &#9729;

### Linux &#128039;

- &#128229; з офіційного сайту за [посиланням &#128279;](https://inkscape.org/release/inkscape-1.2.2/gnulinux/appimage/dl/)
- &#128189; за допомогою **Flathub**:
  - вводимо команду у терміналі: `flatpak install flathub org.inkscape.Inkscape`
- &#128189; за допомогою **Snap Store**:
  - **версія програми &#128230;** latest stable команда для терміналу: `sudo snap install inkscape`
  - **версія програми &#128230;** latest candidate команда для терміналу: `sudo snap install inkscape --candidate`
  - **версія програми &#128230;** latest edge команда для терміналу: `sudo snap install inkscape --edge`

#### fedora WORKSTATION

- &#128189; з **Центру програмного &#128230; забезпечення**

