# LEAGUE DISPLAY


> :information_source: **Інформація:** Заставки та шпалери у форматі HD прямо на робочий стіл.

***
:open_file_folder: **Розділ** :framed_picture: *Графіка*
***

## Про League Display

| Пункт | Інформація |
| -------------- | --------------- |
| :computer: **розробник** | Riot Games |
| **сайт** | [:link: посилання](https://displays.riotgames.com/en-gb/) |

## Встановлення

### :desktop_computer: Windows

- :inbox_tray: Завнтажити з сайту за [:link: посиланням](https://displays.riotgames.com/en-gb/)
- :inbox_tray: Завантажити з :cloud: Хмарного сзовища

