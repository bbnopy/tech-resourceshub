# WINDOWS TERMINAL



## Розділ &#127991; Інструменти &#129520;

>&#128161; Windows Terminal - емулятор багатотабличного терміналу, розроблений компанією Microsoft для Windows 10 і пізніших версій як заміна Windows Console.

- **версія програми &#128230;**: :one: . :one: :nine: . :one: :zero: :five: :seven: :three: . :zero:
- **розробник &#128422;**: Microsoft Corporation
- **офіційний сайт**: [посилання &#128279;](https://github.com/Microsoft/Terminal)

## Операційні Системи

### Windows &#128421;

- &#128229; з **Microsoft Store** за [посиланням &#128279;](https://apps.microsoft.com/detail/9n0dx20hk701?rtc=1&hl=uk-ua&gl=UA)

